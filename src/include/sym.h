/*
 * sym.h --
 *
 *  Declarations for procedures exported by the symbol table
 *  module for mipsim.
 *
 *  This file is part of DISC. It came from the file "sym.h" in
 *  in the distribution of "dlxsim" available at:
 *     ftp://max.stanford.edu/pub/hennessy-patterson.software/dlx.tar.Z
 *
 *  The original source code is copyright as follows:
 *
 * Copyright 1989 Regents of the University of California
 * Permission to use, copy, modify, and distribute this
 * software and its documentation for any purpose and without
 * fee is hereby granted, provided that the above copyright
 * notice appear in all copies.  The University of California
 * makes no representations about the suitability of this
 * software for any purpose.  It is provided "as is" without
 * express or implied warranty.
 *
 */

#ifndef _SYM
#define _SYM

/*
 * Return values from Sym_GetSym:
 */

#define SYM_FOUND	0
#define SYM_REGISTER	1
#define SYM_AMBIGUOUS	2
#define SYM_NOT_FOUND	3
#define SYM_REG_NOT_OK	4
#define SYM_FREG_FOUND	5

/*
 * Flags to pass to Sym_GetSym:
 */

#define SYM_REGS_OK		0x1
#define SYM_PSEUDO_OK		0x2
#define SYM_FREGS_OK		0x4

/*
 * Flags to pass to Sym_AddSymbol:
 */

/* Leave value 1 available so SYM_REGISTER from above can be used */
#define SYM_NO_ADDR	0x2
#define SYM_GLOBAL	0x4

extern int	Sym_AddSymbol(DLX *, char *, char *, unsigned int, int);
extern void	Sym_DeleteSymbols(DLX *, char *);
extern int	Sym_GetSym(DLX *, char *, char *, int, unsigned int *);
extern int	Sym_EvalExpr(DLX *, char *, char *, int, int *, char **);
extern char *	Sym_GetString(DLX *, unsigned int);

#endif /* _SYM */

