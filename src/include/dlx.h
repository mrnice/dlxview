/*
 * dlx.h --
 *
 *  Declarations of structures used to simulate the DLX
 *  architecture.
 *
 *  This file is part of DISC.  It was modified by Yinong Zhang
 *  (yinong@ecn.purdue.edu) from the file "dlx.h" in the distribution
 *  of "dlxsim" available at:
 *     ftp://max.stanford.edu/pub/hennessy-patterson.software/dlx.tar.Z
 *
 *  The original source code is copyright as follows:
 *
 * Copyright 1989 Regents of the University of California
 * Permission to use, copy, modify, and distribute this
 * software and its documentation for any purpose and without
 * fee is hereby granted, provided that the above copyright
 * notice appear in all copies.  The University of California
 * makes no representations about the suitability of this
 * software for any purpose.  It is provided "as is" without
 * express or implied warranty.
 *
 */

#ifndef _MIPS
#define _MIPS

#define MEMSIZE 16384

#ifndef _TCL
#include <tcl.h>
#endif
#ifndef _MIPSIM_IO
#include "io.h"
#endif
#ifndef _COP0
#include "cop0.h"
#endif


/*
 * The following structure is used for each "stop" that has
 * been requested for a machine.
 */

typedef struct Stop {
	char *command;		/* Tcl command to execute, or NULL if
				 * this is a simple stop. */
	int number;			/* Number that identifies this stop for
				 * deletion purposes. */
	unsigned int address;	/* Address (in DLX memory) of memory word
				 * associated with stop. */
	struct Stop *nextPtr;	/* Next in list of stops associated with
				 * same memory location (NULL for end of
				 * list). */
	struct Stop *overallPtr;	/* Next in list of all stops set for
				 * machine (NULL for end of list). */
} Stop;

/*
 * Each memory word is represented by a structure of the following
 * format.  In order to interpret instructions efficiently, they
 * get decoded into several fields on the first execution after each
 * change to the word.
 */

typedef struct {
	int value;			/* Contents of the memory location. */
	int opCode;		/* Type of instruction.  This is NOT
				 * the same as the opcode field from the
				 * instruction:  see #defines below
				 * for details. */
	int rs1, rs2, rd;		/* Three registers from instruction. */
	int extra;			/* Immediate or target or shamt field
				 * or offset.  Immediates are sign-extended. */
	int line;                   /* Line in which the instruction is displayed
				   in the .code.t window. */
	Stop *stopList;		/* List of stops to invoke whenever
				 * this memory location is accessed. */
} MemWord;

/*
 * For each callback registered through Sim_CallBack, there is a structure
 * of the following form:
 */

typedef struct CallBack {
	int serialNum;		/* Call the procedure after executing the
				 * instruction with this serial number. */
	void (*proc)();		/* Procedure to call. */
	ClientData clientData;	/* Argument to pass to proc. */
	struct CallBack *nextPtr;	/* Next callback in list of all those
				 * associated with this machine.  NULL
				 * means end of list. */
} CallBack;



/*
 * For each pending floating point operation, there is a structure of the
 * following form:
 */

typedef struct Op {
	int rs1, rs2;                /* source register, only used in scorboarding
				  algorithm for check WAR hazards. */
	int rs1Ready, rs2Ready;      /* cycle when operands are available. */
	int source1[2], source2[2];  /* source values. */
	int rd;                      /* destination register */
	int rdReady;                 /* cycle when result will be ready */
	int result[2];               /* result[0] is result of single precision
				  operation, both elements used for double
				  precision.
				  A single precision float, and a long
				  integer must be the same size.  */
	int resultType;              /* see *_OP values */
	int unit;                    /* function unit */
	unsigned int address;	       /* mem address of  load or store for
				* load/store buffer in Tomasulo and Scoreboard Alg. */
	struct Op *nextPtr;          /* Next Op in the list of all those
				* associated with this machine.  NULL
				* means end of list. */
} Op;




/*
 * For each cycle, the structure of data for result buffers and memory
 * data register in basic pipeline configuration has the following form:
 */

typedef struct Bypass {
	int buffer;                  /* can be RESULT_BUF1,RESULT_BUF2, MEM_BUF */
	int displayCycle;            /* clock cycle to display the contents in the buffer */
	int reg;		       /* destination register for the buffer */
	struct Bypass *nextPtr;      /* Next Bypass buffer in the list of all those
				* associated with this machine.  NULL
				* means end of list. */
} Bypass;


/*
 * For each cycle, the structure of data for result buffers and memory
 * data register in basic pipeline configuration has the following form:
 */

typedef struct Update {
	int displayCycle;            /* clock cycle to update the information. */
	int readyCycle;	       /* clock cycle when the instruction finishes. */
	int actionType;	       /* type of updating action at display cycle. */
	char w[30];                  /* text widget name of the unit to be updated
				* Tomasulot and Scoreboarding, tag id of
				* items on canvas in BasicPipe.*/

	char msg[80];		       /* string to update the old information in
				  function unit display. */
	struct Update *nextPtr;      /* Next functional unit or reservation station in the
				* list of all those associated with this machine.  NULL
				* means end of list. */
} Update;






/*
 * The structure below describes the state of an DLX machine.
 */
#define TOTAL_REGS 67
#define NUM_GPRS 64
#define FP_STATUS (NUM_GPRS)
#define PC_REG (NUM_GPRS+1)
#define NEXT_PC_REG (NUM_GPRS+2)

/* default values for number of fp units and latencies */
#define MAX_FUNC_UNITS  8       /* Maximum allowable number of any 
				   type of function unit. */
#define NUM_INT_UNITS 1         /* Number of interger units. */
#define NUM_ADD_UNITS 2         /* Number of fp adder units. */
#define NUM_DIV_UNITS 2         /* Number of fp divider units. */
#define NUM_MUL_UNITS 2         /* Number of fp multiplier units. */
#define NUM_LOAD_BUFS 6		/* Number of load buffer (Tomasulo). */
#define NUM_STORE_BUFS 3	/* Number of store buffer (Tomasulo). */
#define INT_LATENCY 1           /* Latency of integer unit */
#define FP_ADD_LATENCY 2        /* Latency for an FP add. */
#define FP_DIV_LATENCY 19       /* Latency for an FP divide. */
#define FP_MUL_LATENCY 5        /* Latency for an FP multiply. */
#define LOAD_BUF_LATENCY 2     	/* Latency for a load buf (Tomasulo). */
#define STORE_BUF_LATENCY 2	/* Latency for a store buf (Tomasulo). */

#define BASICPIPE 0
#define TOMASULO 1
#define SCOREBOARD 2

#define INT 0
#define FP_ADD 1
#define FP_DIV 2
#define FP_MUL 3
#define LOAD_BUF 4
#define STORE_BUF 5
#define BRANCH 6
#define REG_FILE 7

#define NON_OP 0
#define INT_OP 1
#define SFP_OP 2
#define DFP_OP 3
#define IMM_OP 4

#define RESULT_BUF1 1
#define RESULT_BUF2 2
#define RESULT_BUF3 3
#define MEM_BUF0 4
#define MEM_BUF1 5
#define RESULT_BUF1_FP 6
#define RESULT_BUF2_FP 7

#define INSERT_HIGHLIGHT 0
#define HIGHLIGHT_CODE 1
#define DELETE_INSERT 2
#define ADD_END 3
#define HIGHLIGHT_WRITE 4
#define SHOW_CDB_LINE 5
#define SHOW_LINE 6
#define BAS_HIGHLIGHT_LINE 7
#define BAS_HIGHLIGHT_TEXT 8
#define BAS_HIGHLIGHT_RECT 9

#define CYC_CNT_RESET 16384     /* How often to reset the cycle count to
				   prevent wrap around problems. */
#define FD_SIZE 32              /* Number of simulated file descriptors */

/*
 * OpCode values for MemWord structs.  These are straight from the MIPS
 * manual except for the following special values:
 *
 * OP_NOT_COMPILED -	means the value of the memory location has changed
 *			so the instruction needs to be recompiled.
 * OP_UNIMP -		means that this instruction is legal, but hasn't
 *			been implemented in the simulator yet.
 * OP_RES -		means that this is a reserved opcode (it isn't
 *			supported by the architecture).
 */


#define OP_NOP           0
#define OP_ADD 		 1
#define OP_ADDI 	 2
#define OP_ADDU 	 3
#define OP_ADDUI 	 4
#define OP_AND 		 5
#define OP_ANDI 	 6
#define OP_BEQZ 	 7
#define OP_BFPF 	 8
#define OP_BFPT 	 9
#define OP_BNEZ 	 10
#define OP_J 		 11
#define OP_JAL 		 12
#define OP_JALR 	 13
#define OP_JR 		 14
#define OP_LB 		 15
#define OP_LBU 		 16
#define OP_LD 		 17
#define OP_LF 		 18
#define OP_LH 		 19
#define OP_LHI 		 20
#define OP_LHU 		 21
#define OP_LW 		 22
#define OP_MOVD 	 23
#define OP_MOVF 	 24
#define OP_MOVFP2I 	 25
#define OP_MOVI2FP 	 26
#define OP_MOVI2S 	 27
#define OP_MOVS2I 	 28
#define OP_OR 		 29
#define OP_ORI 		 30
#define OP_RFE 		 31
#define OP_SB 		 32
#define OP_SD 		 33
#define OP_SEQ 		 34
#define OP_SEQI 	 35
#define OP_SEQU 	 36
#define OP_SEQUI 	 37
#define OP_SF 		 38
#define OP_SGE 		 39
#define OP_SGEI 	 40
#define OP_SGEU 	 41
#define OP_SGEUI 	 42
#define OP_SGT 		 43
#define OP_SGTI 	 44
#define OP_SGTU 	 45
#define OP_SGTUI 	 46
#define OP_SH 		 47
#define OP_SLE 		 48
#define OP_SLEI 	 49
#define OP_SLEU 	 50
#define OP_SLEUI 	 51
#define OP_SLL 		 52
#define OP_SLLI 	 53
#define OP_SLT 		 54
#define OP_SLTI 	 55
#define OP_SLTU 	 56
#define OP_SLTUI 	 57
#define OP_SNE 		 58
#define OP_SNEI 	 59
#define OP_SNEU 	 60
#define OP_SNEUI 	 61
#define OP_SRA 		 62
#define OP_SRAI 	 63
#define OP_SRL 		 64
#define OP_SRLI 	 65
#define OP_SUB 		 66
#define OP_SUBI 	 67
#define OP_SUBU 	 68
#define OP_SUBUI 	 69
#define OP_SW 		 70
#define OP_TRAP 	 71
#define OP_XOR 		 72
#define OP_XORI 	 73

#define OP_ADDD 	 80
#define OP_ADDF 	 81
#define OP_CVTD2F 	 82
#define OP_CVTD2I 	 83
#define OP_CVTF2D 	 84
#define OP_CVTF2I 	 85
#define OP_CVTI2D 	 86
#define OP_CVTI2F 	 87
#define OP_DIV 		 88
#define OP_DIVD 	 89
#define OP_DIVF 	 90
#define OP_DIVU 	 91
#define OP_EQD 		 92
#define OP_EQF 		 93
#define OP_GED 		 94
#define OP_GEF 		 95
#define OP_GTD 		 96
#define OP_GTF 		 97
#define OP_LED 		 98
#define OP_LEF 		 99
#define OP_LTD 		 100
#define OP_LTF 		 101
#define OP_MULT 	 102
#define OP_MULTD 	 103
#define OP_MULTF 	 104
#define OP_MULTU 	 105
#define OP_NED 		 106
#define OP_NEF 		 107
#define OP_SUBD 	 108
#define OP_SUBF 	 109

/* special "opcodes", give these values after the above op values */

#define OP_NOT_COMPILED  111
#define OP_UNIMP 	 112
#define OP_RES 		 113
#define OP_LAST 	 114

typedef struct {
	Tcl_Interp *interp;		/* Interpreter associated with machine (used
				 * for interpreting commands, returning
				 * errors, etc.) */
	int numWords;		/* Number of words of memory simulated for
				 * this machine. */
	int numChars;               /* Number of characters of memory */
	MemWord *memPtr;		/* Array of MemWords, sufficient to provide
				 * memSize bytes of storage. */
	char *memScratch;           /* Scratchpad for use by trap handlers */
	char *endScratch;           /* Pointer to the end of the scratch pad */
	int fd_map[FD_SIZE];        /* maps simulated fd's to actual fd's.
				 * mainly protects the real standard
				 * input, output, and error from the
				 * program.  A value of -1 means that
				 * fd is not currently in use.  */
	int regs[TOTAL_REGS];	/* General-purpose registers, followed by
				 * hi and lo multiply-divide registers,
				 * followed by program counter and next
				 * program counter.  Both pc's are stored
				 * as indexes into the memPtr array. */
	unsigned int badPC;		/* If an addressing error occurs during
				 * instruction fetch, this value records
				 * the bad address.  0 means no addressing
				 * error is pending. */
	int addrErrNum;		/* If badPC is non-zero, this gives the
				 * serial number (insCount value) of the
				 * instruction after which the addressing
				 * error is to be registered. */
	int loadReg1, loadReg2;	/* For loads, register loaded by last instruction.
				 * used to watch for stalls (0 means last
				 * instruction was not a load).  loadReg2 is
				 * used when a load double is done.  */
	int nextIF, nextID;         /* the clock cycle for  IF and ID stage of next
                                   instruction in basic pipeline configuration. */
	int lastIF;                 /* the clock cycle for  IF stage of the last
                                   instruction in basic pipeline configuration. */
	int cycleHead;              /* the clock cycle for  IF stage of the first
                                   instr when instr cycle table is displayed. */
	Bypass *bypassList;         /* the buffer list used for bypassing in
                                 * basic pipeline configuration. */
	Update *updateList;         /* the list of funcional units or reservation
				 * stations which should have their contents
				 * updated after an instruction finishes. */
	int insCount;		/* Count of total # of instructions executed
				 * in this machine (i.e. serial number of
				 * current instruction). */
	int backInsCount;		/* Count number of the second most recently excuted
                                   instruction, only used in "step back" command */
	int firstIns;		/* Serial number corresponding to first
				 * instruction executed in particular run;
				 * used to ignore stops on first ins. */
	int branchSerial;		/* Serial number of most recent branch/jump
				 * instruction;  used to set BD bit during
				 * exceptions. */
	int branchPC;		/* PC of instruction given by "branchSerial":
				 * also used during exceptions. */
	int flags;			/* Used to indicate special conditions during
				 * simulation (for greatest speed, should
				 * normally be zero).  See below for
				 * definitions. */
	int stopNum;		/* Used to assign increasing reference
				 * numbers to stops. */
	Stop *stopList;		/* First in chain of all spies and stops
				 * associated with this machine (NULL means
				 * none). */
	CallBack *callBackList;	/* First in linked list of all callbacks
				 * currently registered for this machine,
				 * sorted in increasing order of serialNum. */
	Tcl_HashTable symbols;	/* Records addresses of all symbols read in
				 * by assembler for machine. */
	IoState ioState;		/* I/O-related information for machine (see
				 * io.h and io.c for details). */
	Cop0 cop0;			/* State of coprocessor 0 (see cop0.h and
				 * cop0.c for details). */

	/* statistics */

	int	stalls;                 /* Counts load stalls, structural hazard, RAW,
				   WAR, WAW stalls, etc. */

	int branchYes;              /* Count taken branches. */
	int branchNo;               /* Count not taken branches. */
	int	operationCount
	[OP_LAST+1];              /* Dynamic instruction counts */

	/* hardware configuration */
	int config;                 /* basic pipeline, tomasulo,  or scoreboarding */

	/* function unit stuff */

	int num_int_units;         /* Number integer units for this machine */
	int num_add_units;         /* Number FP add units for this machine. */
	int num_div_units;         /* Number FP divide units for this machine. */
	int num_mul_units;         /* Number FP multiply units for this machine. */
	int num_load_bufs;         /* Number load buffers for this machine. */
	int num_store_bufs;        /* Number store buffers for this machine. */
	int int_latency;           /* Integer unit latency for this machine */
	int fp_add_latency;        /* FP add/subtract latency for this machine. */
	int fp_div_latency;        /* FP divide latency for this machine. */
	int fp_mul_latency;        /* FP multiply latency for this machine. */
	int load_buf_latency;      /* Load buffer latency for this machine. */
	int store_buf_latency;     /* Store buffer latency for this machine. */
	int add_ful_pipe;          /* FP add unit is fully piped or not. */
	int mul_ful_pipe;          /* FP divide unit is fully piped or not. */
	int div_ful_pipe;          /* FP multiply unit is fully piped or not. */


	int int_units
	[MAX_FUNC_UNITS];        /* 0 means unit is available, otherwise number
				 indicates cycle count when the int unit will
				 complete its current operation. */
	int fp_add_units
	[MAX_FUNC_UNITS];        /* 0 means unit is available, otherwise number
				 indicates cycle count when the fp adder will
				 complete its current operation. */
	int fp_mul_units
	[MAX_FUNC_UNITS];        /* 0 means unit is available, otherwise number
				 indicates cycle count when the fp divider will
				 complete its current operation. */
	int fp_div_units
	[MAX_FUNC_UNITS];        /* 0 means unit is available, otherwise number
				 indicates cycle count when the fp multiplier will
				 complete its current operation. */

	int load_bufs
	[MAX_FUNC_UNITS];        /* 0 means unit is available, otherwise number
				 indicates cycle count when the load buffer will
				 complete its current operation. */
	int store_bufs
	[MAX_FUNC_UNITS];        /* 0 means unit is available, otherwise number
				 indicates cycle count when the store buffer will
				 complete its current operation. */

	int *func_units[6];        /* Top level array structure used to access
				 fp_add_units, fp_div_units, and fp_mul_units. */

	int fp_div_exist;          /* 0 means fp divider is not supported by the
               			  current configuration. Its function is
				  performed by fp multiplier. */

	int ld_st_exist;	       /* 0 means load and store buffers(units) are
				  not supported by the current configuration.
				  It's function is performed by integer unit. */

	int waiting_regs[65];     /* A non-zero value in element i means
				 register i is waiting for a result from an
				 function unit.  The value indicates the cycle
				 count when the result will be ready. */

	int Qi_regs[65];	      /* The number of the functional unit that will
				 produe a value to be stored in a FP register. */

	int sameCycle_regs[65];   /* The number of samultaneous to a single register
				 in a cycle.  Only used in scorboarding algorithm. */

	Op *opsList;              /* First in a linked list of all pending
				 operations associated with this
				 machine; NULL means none. */

	Op *finishList;           /* A List of operations in which all excution have
			         been finished except updating the screen. */

	int cycleCount;           /* Keep track of how many cycles have been
				 executed to monitor floating point units. */

	int cycleDisplayCount;    /* clock cycle when machine is in cycle simulation
                                 mode. */

	int cycleDisplayMode;     /* whether in cycle display mode or not. */

	int codeLine;             /* Line in which the loaded instruction is displayed
                                 in the .code.t window. */

	char *cycleTable[5];      /* 5 strings containing the clock cycle info
				 for each stage in BasicPipe. */

	Op *opFree;		      /* Free Op list */
	Update *updateFree;	      /* Free Update list */
	Bypass *bypassFree;       /* Free Bypass list */

	FILE *refInstrTraceFile;/* Pointer to a file into which to write dynamic
				 instruction trace as input for spam. */
	FILE *refTraceFile;       /* Pointer to a file into which to write memory
				 references for the cache simulator. */

} DLX;

/*
 * Flag values for DLX structures:
 *
 * STOP_REQUESTED:		1 means that the "stop" command has been
 *				executed and execution should stop ASAP.
 *
 */

#define STOP_REQUESTED		0x4

/*
 * Conversion between DLX addresses and indexes, which are stored
 * in pc/nextPc fields of DLX structures and also used to address
 * the memPtr values:
 */

#define INDEX_TO_ADDR(index)	((unsigned) ((index) << 2))
#define ADDR_TO_INDEX(addr)	((addr) >> 2)

/*
 * Miscellaneous definitions:
 */

#define SIGN_BIT	0x80000000
#define R31		31

/*
 * Variables and procedures exported to rest of simulator:
 */

extern char *Asm_RegNames[];


/*
 * Tcl command procedures provided by the simulator:
 */

extern int	Asm_AsmCmd(ClientData, Tcl_Interp *, int, char**);
extern int	Asm_LoadCmd(ClientData, Tcl_Interp *, int, char **);
extern int 	Gp_GetCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Gp_PutCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Gp_FGetCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Gp_FPutCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Main_QuitCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Sim_InitCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Sim_DumpStats(ClientData, Tcl_Interp *, int, char**);
extern int 	Sim_ITraceCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Sim_MTraceCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Sim_GoCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Sim_StepCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Sim_CycleCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Sim_EquationCmd(ClientData, Tcl_Interp *, int, char**);
extern int 	Stop_StopCmd(ClientData, Tcl_Interp *, int, char**);



/*
 * Other procedures that are exported from one file to another:
 */

extern int		Asm_Assemble(DLX *, char *, char *, unsigned int,
							 char *, int, int *, int *);
extern char *		Asm_Disassemble(DLX *, int, unsigned int);
extern int		Asm_GetExpr(DLX *, Tcl_Interp *, int, char **);

extern void		Cop0_Init(DLX *);
extern void		Cop0_IntPending(DLX *, int, int);

extern void		Gp_PutByte(DLX *, unsigned int, int);
/*
extern int		Gp_PutString(DLX *, char *, char, unsigned int,
				     int, char **);
*/
extern int		Gp_PutString();

extern Op *		Isu_Issue (DLX *, MemWord *, int, int, int, int);
extern void     	Isu_WriteBack(DLX *);
extern void		Isu_FreeOp(DLX *, Op *);
extern void		Isu_FreeUpdate(DLX *, Update *);
extern void		Isu_FreeBypass(DLX *, Bypass *);
extern void		Isu_CleanFinished(DLX *, char *, int);

extern void		Sim_CallBack(DLX *, int, void (*)(), ClientData);
extern DLX *		Sim_Create(Tcl_Interp *);
extern void             Sim_Initialize(DLX *);
extern unsigned int	Sim_GetPC(DLX *);
extern void		Sim_Stop(DLX *);

extern int		Stop_Execute(DLX *, Stop *);

extern void 		Trap_Init_Handle(DLX *);
extern int 		Trap_Handle(DLX *, int);

extern void             Script_Read(Tcl_Interp *);





#endif /* _MIPS */
