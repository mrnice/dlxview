/*
 * issue.c --
 *
 *  This file contains issue engines for different DLX processors.
 *
 *  This file is part of DISC.  It was written by Yinong Zhang
 *  (yinong@ecn.purdue.edu)
 *
 */

#define USE_INTERP_RESULT
#define USE_INTERP_ERRORLINE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "include/dlx.h"



#define getcolor(x)	Colors[(x) & 0x000f]



char *operationNames[] = {
	"NOP", "ADD", "ADDI", "ADDU", "ADDUI", "AND", "ANDI", "BEQZ", "BFPF",
	"BFPT", "BNEZ", "J", "JAL", "JALR", "JR", "LB", "LBU", "LD", "LF",
	"LH", "LHI", "LHU", "LW", "MOVD", "MOVF", "MOVFP2I", "MOVI2FP",
	"MOVI2S", "MOVS2I", "OR", "ORI", "RFE", "SB", "SD", "SEQ",  "SEQI",
	"SEQU", "SEQUI",
	"SF", "SGE", "SGEI", "SGEU", "SGEUI", "SGT", "SGTI", "SGTU", "SGTUI",
	"SH", "SLE", "SLEI", "SLEU", "SLEUI", "SLL",
	"SLLI", "SLT", "SLTI", "SLTU", "SLTUI", "SNE", "SNEI", "SNEU", "SNEUI",
	"SRA", "SRAI", "SRL",
	"SRLI", "SUB", "SUBI", "SUBU", "SUBUI", "SW", "TRAP", "XOR", "XORI",
	"", "", "", "","", "",

	/* There must be at least one empty string between the last integer operation
	 *	and the first floating point operation.	*/

	"ADDD", "ADDF", "CVTD2F", "CVTD2I", "CVTF2D", "CVTF2I", "CVTI2D",
	"CVTI2F", "DIV", "DIVD", "DIVF", "DIVU", "EQD", "EQF", "GED", "GEF",
	"GTD", "GTF", "LED", "LEF", "LTD", "LTF", "MULT", "MULTD", "MULTF",
	"MULTU", "NED", "NEF", "SUBD", "SUBF", ""
};


char *Units[] = {
	" Int1 ", " Int2 ", " Int3 ", " Int4 ",
	" Int5 ", " Int6 ", " Int7 ", " Int8 ",
	" Add1 ", " Add2 ", " Add3 ", " Add4 ",
	" Add5 ", " Add6 ", " Add7 ", " Add8 ",
	" Div1 ", " Div2 ", " Div3 ", " Div4 ",
	" Div5 ", " Div6 ", " Div7 ", " Div8 ",
	" Mult1", " Mult2", " Mult3", " Mult4",
	" Mult5", " Mult6", " Mult7", " Mult8",
	" Load1", " Load2", " Load3", " Load4",
	" Load5", " Load6", " Load7", " Load8"
};


char *Colors[] = {
	"Goldenrod ",	"SlateBlue ",	"Red ",        	        "Purple ",
	"Turquoise ", 	"Blue ",	"LightSeaGreen ",	"IndianRed ",
	"Magenta ",	"VioletRed ",	"YellowGreen ", 	"DeepSkyBlue ",
	"HotPink ", 	"SaddleBrown ",	"DarkSeaGreen ", 	"Maroon "
};


char *functionTypes[] = {
	"int", "add", "div", "mul", "load", "store"
};




/*
 * Forward declarations for procedures defined later in this file:
 */

static void	GenEightBit(char *);
static Op	*WhichOp(DLX *, int);
static Op	*mallocOp(DLX *);
static Update   *mallocUpdate(DLX *);
static Bypass   *mallocBypass(DLX *);
static void     BypassInsert(DLX *, int, int, int);
static void     GenUpdateStruct(DLX *, char *, int, int, int, int,
								char *, char *, char *, char *, char *);





static void
GenEightBit(str)
char *str;
{
	char temp[20];
	int i;
	int len;


	temp[0] = '\0';
	len = strlen(str);
	for (i = 0; i < (7 - len)/2.0; i++)
		strcat(temp, " ");
	strcat(temp, str);
	for (i = 0; i < (8 - len)/2.0; i++)
		strcat(temp, " ");
	temp[8] = '\0';
	strcpy(str, temp);
}


static void
GenUpdateStruct(machPtr, w, displayLater, actionType, displayCycle, readyCycle,
				part1, part2, part3, part4, part5)
DLX *machPtr;			/* Machine description. */
char *w;
int displayLater;
int actionType;
int displayCycle;
int readyCycle;
char *part1;
char *part2;
char *part3;
char *part4;
char *part5;
{
	Update *updatePtr,  *uPtr;
	if (!displayLater) {
		Tcl_SetVar(machPtr->interp, "msg", part1, TCL_GLOBAL_ONLY);
		switch (actionType) {
		case INSERT_HIGHLIGHT:
			Tcl_VarEval(machPtr->interp, "InsertAndHighLight ", w, "$msg", (char *)NULL);
			break;
		case HIGHLIGHT_CODE:
			Tcl_VarEval(machPtr->interp, "HighLightCodeLine ", "$msg", (char *)NULL);
			break;
		case DELETE_INSERT:
			Tcl_VarEval(machPtr->interp, "DeleteAndInsert ", w, "$msg", (char *)NULL);
			break;
		case ADD_END:
			Tcl_VarEval(machPtr->interp, "AddToEnd ", w, "$msg", (char *)NULL);
			break;
		case SHOW_LINE:
			goto InsertList;
		}
		return;
	}

InsertList:
	updatePtr = mallocUpdate(machPtr);
	strcpy(updatePtr->w, w);
	updatePtr->actionType = actionType;
	updatePtr->displayCycle = displayCycle;
	updatePtr->readyCycle = readyCycle;
	strcpy(updatePtr->msg, part1);
	strcat(updatePtr->msg, part2);
	strcat(updatePtr->msg, part3);
	strcat(updatePtr->msg, part4);
	strcat(updatePtr->msg, part5);
	if (machPtr->updateList == NULL ||
		updatePtr->displayCycle < machPtr->updateList->displayCycle) {
		updatePtr->nextPtr = machPtr->updateList;
		machPtr->updateList = updatePtr;
	} else {
		for (uPtr = machPtr->updateList; uPtr->nextPtr != NULL;
			 uPtr = uPtr->nextPtr)
			if (updatePtr->displayCycle < uPtr->nextPtr->displayCycle) break;
		updatePtr->nextPtr = uPtr->nextPtr;
		uPtr->nextPtr = updatePtr;
	}
}



Op *
Isu_Issue(machPtr, wordPtr, unitType, rs1Type, rs2Type, rdType)
DLX *machPtr;			/* Machine description. */
MemWord *wordPtr;
int unitType;
int rs1Type;
int rs2Type;
int rdType;
{
	Op *opPtr,  *opPtr2;
	int i,j;
	int wait;
	int soonest;
	int num_units;
	int latency;
	int load_store = 0;
	int regType, regNumber;
	Update *updatePtr;


	char msg[80], part1[40];

	switch (machPtr->config) {
	case BASICPIPE: {
		Bypass *bypassPtr;
		int result_buf1 = TOTAL_REGS;
		int result_buf2 = TOTAL_REGS;
		int result_buf3 = TOTAL_REGS;
		int mem_buf0 = TOTAL_REGS;
		int mem_buf1 = TOTAL_REGS;
		int IF, ID, EX, MEM, WB;
		int rdReady;
		int rs1;
		int ful_pipe = 0;
		int branch = 0;
		int fpbypass_to_store = 0;
		int k;
		char *instrcolor;
		char linelist[200], linecolorlist[200];
		char textlist[100], textcolorlist[200];
		char rectlist[200], rectcolorlist[200];
		char *cycleTemp;

		IF = machPtr->nextIF;
		ID = machPtr->nextID;
		instrcolor = getcolor(IF);

		switch (unitType) {
		case LOAD_BUF:
		case STORE_BUF:
			load_store = 1;
			num_units = machPtr->num_int_units;
			latency = 2;
			unitType = INT;
			break;
		case INT:
			num_units = machPtr->num_int_units;
			latency = 1;
			break;
		case BRANCH:
			branch = 1;
			num_units = machPtr->num_int_units;
			latency = 1;
			unitType = INT;
			break;
		case FP_ADD:
			num_units = machPtr->num_add_units;
			latency = machPtr->fp_add_latency;
			ful_pipe = machPtr->add_ful_pipe;
			break;
		case FP_MUL:
			num_units = machPtr->num_mul_units;
			latency = machPtr->fp_mul_latency;
			ful_pipe = machPtr->mul_ful_pipe;
			break;
		case FP_DIV:
			latency = machPtr->fp_div_latency;
			if (machPtr->fp_div_exist) {
				num_units = machPtr->num_div_units;
				ful_pipe = machPtr->div_ful_pipe;
			} else {
				/* POSSIBLE BUG. Visualization of block diagram do not know how
				   to show this.
				*/
				unitType = FP_MUL;
				num_units = machPtr->num_mul_units;
				ful_pipe = machPtr->mul_ful_pipe;
			}
			break;
		}


issueCheck0:
		wait = 0;
		/* check for WAW hazard */
		if (rdType != NON_OP && machPtr->waiting_regs[wordPtr->rd])
			wait = machPtr->waiting_regs[wordPtr->rd];

		/* check for RAW hazard */
		if (rs1Type != NON_OP && rs1Type != IMM_OP
			&& machPtr->waiting_regs[wordPtr->rs1] > wait)
			wait = machPtr->waiting_regs[wordPtr->rs1];

		/* for any store instr, the value of rs2 is required at the MEM
		stage, so the value of wait should be
		 "machPtr->waiting_regs[wordPtr->rs2]-1".  Also, note that for
		 load instr, rs2Type is NON_OP
		    */
		if (rs2Type != NON_OP && rs2Type != IMM_OP
			&& machPtr->waiting_regs[wordPtr->rs2] > wait)
			wait = machPtr->waiting_regs[wordPtr->rs2] - load_store;

		/* check for structural hazard */
		for (i = 0, soonest = 0x7fffffff; i < num_units; i++) {
			if (!(j = machPtr->func_units[unitType][i]))
				break;
			else if (j < soonest)
				soonest = j;
		}
		if (i == num_units && (soonest > wait))
			wait = soonest;

		/* check for multiple wirtes to FP reg file in the same cycle
		   rdReady is the clock cycle for the last EX stage
		*/
		if (rdType && wordPtr->rd >= 32 && wordPtr->rd < 64) {
			soonest = wait ? wait : machPtr->cycleCount;
			/* load instructions "completes" in MEM stage, therefore,
			   1 (load_sotre) must be subtracted from rdReady.
			*/
			rdReady = soonest + latency - load_store;
			opPtr2 = machPtr->opsList;
			while (opPtr2 != NULL) {
				if (rdReady == opPtr2->rdReady && opPtr2->resultType &&
					opPtr2->rd >=32 && opPtr2->rd < 64) {
					soonest++;
					rdReady++;
					wait = soonest;
					opPtr2 = opPtr2->nextPtr;
				} else if (rdReady > opPtr2->rdReady) {
					opPtr2 = opPtr2->nextPtr;
				} else {
					break;
				}
			}
		}

		if (wait > machPtr->cycleCount) {
			machPtr->cycleCount = wait;
			Isu_WriteBack(machPtr);
			goto issueCheck0;
		}


		sprintf(part1, "%d", wordPtr->line);
		Tcl_VarEval(machPtr->interp, "HighLightCodeLine ", part1, (char *)NULL);


		EX = machPtr->cycleCount + 1;
		while (machPtr->bypassList != NULL && machPtr->bypassList->displayCycle < EX) {
			bypassPtr = machPtr->bypassList;
			machPtr->bypassList = bypassPtr->nextPtr;
			Isu_FreeBypass(machPtr, bypassPtr);
		}

		while (machPtr->bypassList != NULL && machPtr->bypassList->displayCycle == EX) {
			bypassPtr = machPtr->bypassList;
			machPtr->bypassList = bypassPtr->nextPtr;
			switch (bypassPtr->buffer) {
			case RESULT_BUF1:
				result_buf1 = bypassPtr->reg;
				break;
			case RESULT_BUF2:
				result_buf2 = bypassPtr->reg;
				break;
			case RESULT_BUF3:
				result_buf3 = bypassPtr->reg;
				break;
			case MEM_BUF0:
				mem_buf0 = bypassPtr->reg;
				break;
			case MEM_BUF1:
				mem_buf1 = bypassPtr->reg;
				break;
			}
			Isu_FreeBypass(machPtr, bypassPtr);

		}

		/* when a branch depends on a previous ALU operation or previous
		   two loads, there is a data hazard.
		*/
		if (branch && rs1Type == INT_OP) {
			rs1 = wordPtr->rs1;
			if (rs1 == mem_buf1 || rs1 == result_buf1) {
				machPtr->cycleCount++;
				ID = machPtr->cycleCount;
				EX = ID + 1;
				Isu_WriteBack(machPtr);
				while (machPtr->bypassList != NULL &&
					   machPtr->bypassList->displayCycle < EX) {
					bypassPtr = machPtr->bypassList;
					machPtr->bypassList = bypassPtr->nextPtr;
					Isu_FreeBypass(machPtr, bypassPtr);
				}
			}
		}

		/* Now, machPtr->cycleCount is one less than the EX stage clock cycle.
		   (not always the same as ID stage clock cycle)
		*/

		opPtr = mallocOp(machPtr);
		opPtr->unit = unitType * MAX_FUNC_UNITS + i;
		opPtr->resultType = rdType;
		opPtr->rdReady = machPtr->cycleCount + latency;
		machPtr->nextIF = ID;
		machPtr->nextID = EX;
		if (unitType == INT) {
			MEM = EX + 1;
			WB = EX + 2;
		} else {
			MEM = opPtr->rdReady + 1;
			WB = opPtr->rdReady + 2;
		}

		if (!machPtr->cycleDisplayMode) {
			sprintf(msg, "%d;", IF);
			Tcl_VarEval(machPtr->interp,
						".bas.datapath.counter configure -text ", msg,
						".bas.block.counter configure -text ", msg, (char *)NULL);
		}


		if (rs1Type != NON_OP && rs1Type != IMM_OP) {
			switch (rs1Type) {
			case INT_OP:
				*((int *)&(opPtr->source1[0])) =
					*((int *)&(machPtr->regs[wordPtr->rs1]));
				break;
			case SFP_OP:
				*((float *)&(opPtr->source1[0])) =
					*((float *)&(machPtr->regs[wordPtr->rs1]));
				break;
			case DFP_OP:
				*((double *)&(opPtr->source1[0])) =
					*((double *)&(machPtr->regs[wordPtr->rs1]));
				break;
			}
		}

		if (rs2Type != NON_OP && rs2Type != IMM_OP) {
			switch (rs2Type) {
			case INT_OP:
				*((int *)&(opPtr->source2[0])) =
					*((int *)&(machPtr->regs[wordPtr->rs2]));
				break;
			case SFP_OP:
				*((float *)&(opPtr->source2[0])) =
					*((float *)&(machPtr->regs[wordPtr->rs2]));
				break;
			case DFP_OP:
				*((double *)&(opPtr->source2[0])) =
					*((double *)&(machPtr->regs[wordPtr->rs2]));
				break;
			}
		}

		if (load_store)
			opPtr->address = opPtr->source1[0] + wordPtr->extra;


		if (ful_pipe)
			machPtr->func_units[unitType][i] = 0;
		else
			machPtr->func_units[unitType][i] = opPtr->rdReady;

		if (machPtr->opsList == NULL ||
			opPtr->rdReady < machPtr->opsList->rdReady) {
			opPtr->nextPtr = machPtr->opsList;
			machPtr->opsList = opPtr;
		} else {
			for (opPtr2 = machPtr->opsList; opPtr2->nextPtr != NULL;
				 opPtr2 = opPtr2->nextPtr)
				if (opPtr->rdReady < opPtr2->nextPtr->rdReady) break;
			opPtr->nextPtr = opPtr2->nextPtr;
			opPtr2->nextPtr = opPtr;
		}

		opPtr->rd = wordPtr->rd;
		if (rdType && wordPtr->rd) {
			if (load_store) {
				BypassInsert(machPtr, MEM_BUF0, opPtr->rdReady, opPtr->rd);
				BypassInsert(machPtr, MEM_BUF1, opPtr->rdReady + 1, opPtr->rd);
			} else if (unitType == INT) {
				BypassInsert(machPtr, RESULT_BUF1, opPtr->rdReady + 1, opPtr->rd);
				BypassInsert(machPtr, RESULT_BUF2, opPtr->rdReady + 2, opPtr->rd);
				BypassInsert(machPtr, RESULT_BUF3, opPtr->rdReady + 3, opPtr->rd);
			} else {
				BypassInsert(machPtr, RESULT_BUF1_FP, opPtr->rdReady + 1, opPtr->rd);
				BypassInsert(machPtr, RESULT_BUF2_FP, opPtr->rdReady + 2, opPtr->rd);
			}

			machPtr->waiting_regs[wordPtr->rd] = opPtr->rdReady;
			if (rdType  == DFP_OP) {
				machPtr->waiting_regs[wordPtr->rd + 1] = opPtr->rdReady;
			}
		}

		/* The following codes deal with visulization. */

		sprintf(part1, "%s",
				Asm_Disassemble(machPtr, machPtr->memPtr[machPtr->regs[PC_REG]].value,
								Sim_GetPC(machPtr) & ~0x3));


		/* Generate clock cycle table for the 5 most recent instructions. */
		if (machPtr->insCount >= 5) {
			k = 0;
			while (!strncmp(machPtr->cycleTable[1] + k, "nonop ", 6) ||
				   !strncmp(machPtr->cycleTable[1] + k, "stall ", 6))
				k += 6;

			for (j = 0; j < 4; j++) {
				strcpy(machPtr->cycleTable[j],  machPtr->cycleTable[j + 1] + k);
			}

			machPtr->cycleHead += k/6;
			cycleTemp = machPtr->cycleTable[4];
		} else {
			cycleTemp = machPtr->cycleTable[machPtr->insCount];
		}

		k = machPtr->lastIF + 1 - machPtr->cycleHead;
		cycleTemp[0] ='\0';
		while (k > 0) {
			strcat(cycleTemp, "nonop ");
			k--;
		}

		k = IF -  machPtr->lastIF;
		while (k > 1) {
			strcat(cycleTemp, "stall ");
			k--;
		}

		strcat(cycleTemp, "IF ");
		k = ID - IF;
		while (k > 1) {
			strcat(cycleTemp, "stall ");
			k--;
		}

		strcat(cycleTemp, "ID ");
		k = EX - ID;
		while (k > 1) {
			strcat(cycleTemp, "stall ");
			k--;
		}
		strcat(cycleTemp, "EX ");

		k = MEM - EX;
		while (k > 1) {
			strcat(cycleTemp, "EX ");
			k--;
		}
		strcat(cycleTemp, "MEM WB ");
		sprintf(cycleTemp, "%s {%s %s}", cycleTemp, instrcolor, part1);

		sprintf(msg, "BasGenTable %d %d %d ", machPtr->cycleHead, WB, IF);
		Tcl_VarEval(machPtr->interp, msg, "{", machPtr->cycleTable[0], "} ",
					"{", machPtr->cycleTable[1], "} ",
					"{", machPtr->cycleTable[2], "} ",
					"{", machPtr->cycleTable[3], "} ",
					"{", machPtr->cycleTable[4], "} ", (char *)NULL);


		/* visulization in datapath */
		GenUpdateStruct(machPtr, "InstrIF ", 1, BAS_HIGHLIGHT_TEXT, IF, IF,
						instrcolor, part1, "", "", "");
		GenUpdateStruct(machPtr, "InstrID ", 1, BAS_HIGHLIGHT_TEXT, ID, ID,
						instrcolor, part1, "", "", "");
		if (unitType == INT && !branch) {
			GenUpdateStruct(machPtr, "InstrEX ", 1, BAS_HIGHLIGHT_TEXT, EX, EX,
							instrcolor, part1, "", "", "");
			GenUpdateStruct(machPtr, "InstrMEM ", 1, BAS_HIGHLIGHT_TEXT, MEM, MEM,
							instrcolor, part1, "", "", "");
			GenUpdateStruct(machPtr, "InstrWB ", 1, BAS_HIGHLIGHT_TEXT, WB, WB,
							instrcolor, part1, "", "", "");
		}

		if (rs1Type != NON_OP && rs1Type != IMM_OP) {
			if (wordPtr->rs1 < 32) {
				sprintf(part1, "R%d ", wordPtr->rs1);
				GenUpdateStruct(machPtr, "RegA ", 1, BAS_HIGHLIGHT_TEXT, ID, ID,
								instrcolor, part1, "", "", "");
			} else if (wordPtr->rs1 < 64) {
				sprintf(part1, "F%d ", wordPtr->rs1 - 32);
				GenUpdateStruct(machPtr, "RegA ", 1, BAS_HIGHLIGHT_TEXT, ID, ID,
								instrcolor, part1, "", "", "");
			}
		}

		if (rs2Type != NON_OP && rs2Type != IMM_OP && !load_store) {
			if (wordPtr->rs2 < 32) {
				sprintf(part1, "R%d ", wordPtr->rs2);
				GenUpdateStruct(machPtr, "RegB ", 1, BAS_HIGHLIGHT_TEXT, ID, ID,
								instrcolor, part1, "", "", "");
			} else if (wordPtr->rs2 < 64) {
				sprintf(part1, "F%d ", wordPtr->rs2 - 32);
				GenUpdateStruct(machPtr, "RegB ", 1, BAS_HIGHLIGHT_TEXT, ID, ID,
								instrcolor, part1, "", "", "");
			}
		}

		GenUpdateStruct(machPtr, "StageIF ", 1, BAS_HIGHLIGHT_LINE,
						IF, IF, instrcolor, "", "", "", "");
		GenUpdateStruct(machPtr, "StageID ", 1, BAS_HIGHLIGHT_LINE,
						ID, ID, instrcolor, "", "", "", "");

		if (unitType == INT) {
			if (rdType != NON_OP && wordPtr->rd) {
				if (wordPtr->rd < 32) {
					sprintf(part1, "R%d ", wordPtr->rd);
					GenUpdateStruct(machPtr, "RegDest ", 1, BAS_HIGHLIGHT_TEXT, WB, WB,
									instrcolor, part1, "", "", "");
				} else if (wordPtr->rd < 64) {
					sprintf(part1, "F%d ", wordPtr->rd - 32);
					GenUpdateStruct(machPtr, "RegDest ", 1, BAS_HIGHLIGHT_TEXT, WB, WB,
									instrcolor, part1, "", "", "");
				}
				if (load_store)      /* a load instr */
					GenUpdateStruct(machPtr, "RegMemDest ", 1, BAS_HIGHLIGHT_LINE,
									WB, WB, instrcolor, "", "", "", "");
				else
					GenUpdateStruct(machPtr, "RegAluDest ", 1, BAS_HIGHLIGHT_LINE,
									WB, WB, instrcolor, "", "", "", "");
			}

			if (branch) {
				/* when the branch condition needs to be forwarded,
				up to 2 previous ALU instructions and up to 3 previous
				load instructions need to be considered.
				   Suppose the branch is instruction (i), then
				(i-1) ALU -> (i) branch :   result_buf1 (cause a stall first)
				   (i-2) ALU -> (i) branch :   result_buf1
				   (i-3) ALU -> (i) branch :   result_buf2
				(i-1) load -> (i) branch:   mem_buf1 (cause 2 stalls first)
				   (i-2) load -> (i) branch:   mem_buf1 (cause a stall first)
				   (i-3) load -> (i) branch:   mem_buf1
				*/
				GenUpdateStruct(machPtr, "StageIDBranch ", 1, BAS_HIGHLIGHT_LINE,
								ID, ID, instrcolor, "", "", "", "");
				if (wordPtr->rs1 == result_buf1) {
					sprintf(part1, "BranchRes1 ");
				} else if (wordPtr->rs1 == result_buf2) {
					sprintf(part1, "BranchRes2 ");
				} else if (wordPtr->rs1 == result_buf3) {
					sprintf(part1, "BranchRes3 ");
				} else if (wordPtr->rs1 == mem_buf1) {
					sprintf(part1, "BranchMem1 ");
				} else {
					sprintf(part1, "BranchReg ");
				}
				GenUpdateStruct(machPtr, part1, 1, BAS_HIGHLIGHT_LINE,
								ID, ID, instrcolor, "", "", "", "");
			} else {
				GenUpdateStruct(machPtr, "StageEX ", 1, BAS_HIGHLIGHT_LINE,
								EX, EX, instrcolor, "", "", "", "");
				GenUpdateStruct(machPtr, "StageMEM ", 1, BAS_HIGHLIGHT_LINE,
								MEM, MEM, instrcolor, "", "", "", "");
				GenUpdateStruct(machPtr, "StageWB ", 1, BAS_HIGHLIGHT_LINE,
								WB, WB, instrcolor, "", "", "", "");
				if (rs1Type) {
					if (wordPtr->rs1 == result_buf1) {
						sprintf(part1, "AluARes1 ");
					} else if (wordPtr->rs1 == result_buf2) {
						sprintf(part1, "AluARes2 ");
					} else if (wordPtr->rs1 == mem_buf1) {
						sprintf(part1, "AluAMem1 ");
					} else {
						sprintf(part1, "AluAReg ");
					}
					GenUpdateStruct(machPtr, part1, 1, BAS_HIGHLIGHT_LINE,
									EX, EX, instrcolor, "", "", "", "");
				}
				if (rs2Type && !load_store) {
					if (rs2Type == IMM_OP) {
						sprintf(part1, "AluBImme ");
					} else if (wordPtr->rs2 == result_buf1) {
						sprintf(part1, "AluBRes1 ");
					} else if (wordPtr->rs2 == result_buf2) {
						sprintf(part1, "AluBRes2 ");
					} else if (wordPtr->rs2 == mem_buf1) {
						sprintf(part1, "AluBMem1 ");
					} else {
						sprintf(part1, "AluBReg ");
					}
					GenUpdateStruct(machPtr, part1, 1, BAS_HIGHLIGHT_LINE,
									EX, EX, instrcolor, "", "", "", "");
				}
				if (load_store) {
					/* when the value to be stored needs to be forwarded,
					up to 3 previous ALU instructions and up to 2 previous
					load instructions need to be considered. Since fp
					datapath is not shown here, the bypassing from an fp
					instruction to a store is ignored.
					   Suppose the store is instruction (i), then
					(i-1) ALU -> (i) store :   result_buf1 (actually from MEM/WB reg)
					   (i-2) ALU -> (i) store :   result_bif2 (should from WB/EXTRA reg)
					(i-1) load -> (i) store:   mem_buf0 (actually form MEM/WB reg)
					   (i-2) load -> (i) store:   mem_buf1 (should from WB/EXTRA reg)
					*/
					GenUpdateStruct(machPtr, "AluBImme ", 1, BAS_HIGHLIGHT_LINE,
									EX, EX, instrcolor, "", "", "", "");
					if (rs2Type) {      /* store instruction */
						if (wordPtr->rs2 == mem_buf0) {
							GenUpdateStruct(machPtr, "MemMem0 ", 1, BAS_HIGHLIGHT_LINE,
											MEM, MEM, instrcolor, "", "", "", "");
						} else if (wordPtr->rs2 == mem_buf1) {
							GenUpdateStruct(machPtr, "MemMem1 ", 1, BAS_HIGHLIGHT_LINE,
											MEM, MEM, instrcolor, "", "", "", "");
						} else if (wordPtr->rs2 == result_buf1) {
							GenUpdateStruct(machPtr, "MemRes1 ", 1, BAS_HIGHLIGHT_LINE,
											MEM, MEM, instrcolor, "", "", "", "");
						} else if (wordPtr->rs2 == result_buf2) {
							GenUpdateStruct(machPtr, "MemRes2 ", 1, BAS_HIGHLIGHT_LINE,
											MEM, MEM, instrcolor, "", "", "", "");
						} else {
							bypassPtr = machPtr->bypassList;
							while (bypassPtr != NULL && bypassPtr->displayCycle == MEM) {
								if (bypassPtr->reg == wordPtr->rs2 &&
									(bypassPtr->buffer == RESULT_BUF1_FP ||
									 bypassPtr->buffer == RESULT_BUF2_FP))
									fpbypass_to_store = 1;
								bypassPtr = bypassPtr->nextPtr;
							}
							if (!fpbypass_to_store)
								GenUpdateStruct(machPtr, "MemReg ", 1, BAS_HIGHLIGHT_LINE,
												MEM, MEM, instrcolor, "", "", "", "");
						}
					}
				}
			}
		}

		/* visualization in block diagram */
		GenUpdateStruct(machPtr, "RectIF ", 1, BAS_HIGHLIGHT_RECT,
						IF, IF, instrcolor, "", "", "", "");
		GenUpdateStruct(machPtr, "RectID ", 1, BAS_HIGHLIGHT_RECT,
						ID, ID, instrcolor, "", "", "", "");
		for (j = EX; j < MEM; j++) {
			switch (unitType) {
			case INT:
				sprintf(part1, "RectEXInt ");
				break;
			case FP_ADD:
				sprintf(part1, "RectEXAdd%d.%d ", i, j-EX+1);
				break;
			case FP_MUL:
				sprintf(part1, "RectEXMul%d.%d ", i, j-EX+1);
				break;
			case FP_DIV:
				sprintf(part1, "RectEXDiv%d.%d ", i, j-EX+1);
				break;
			}
			GenUpdateStruct(machPtr, part1, 1, BAS_HIGHLIGHT_RECT,
							j, j, instrcolor, "", "", "", "");
		}
		GenUpdateStruct(machPtr, "RectMEM ", 1, BAS_HIGHLIGHT_RECT,
						MEM, MEM, instrcolor, "", "", "", "");
		GenUpdateStruct(machPtr, "RectWB ", 1, BAS_HIGHLIGHT_RECT,
						WB, WB, instrcolor, "", "", "", "");


		while (machPtr->updateList != NULL &&
			   machPtr->updateList->displayCycle < IF) {
			updatePtr = machPtr->updateList;
			machPtr->updateList = updatePtr->nextPtr;
			Isu_FreeUpdate(machPtr, updatePtr);
		}

		machPtr->lastIF = IF;
		if (machPtr->cycleDisplayMode)
			return opPtr;

		strcpy(linelist, "{");
		strcpy(linecolorlist, "{");
		strcpy(textlist, "{");
		strcpy(textcolorlist, "{");
		strcpy(rectlist, "{");
		strcpy(rectcolorlist, "{");
		while (machPtr->updateList != NULL &&
			   machPtr->updateList->displayCycle == IF) {
			updatePtr = machPtr->updateList;
			machPtr->updateList = updatePtr->nextPtr;
			switch (updatePtr->actionType) {
			case BAS_HIGHLIGHT_LINE:
				strcat(linelist, updatePtr->w);
				strcat(linecolorlist, updatePtr->msg);
				break;
			case BAS_HIGHLIGHT_TEXT:
				strcat(textlist, updatePtr->w);
				strcat(textcolorlist, "{");
				strcat(textcolorlist, updatePtr->msg);
				strcat(textcolorlist, "} ");
				break;
			case BAS_HIGHLIGHT_RECT:
				strcat(rectlist, updatePtr->w);
				strcat(rectcolorlist, updatePtr->msg);
				break;
			}
			Isu_FreeUpdate(machPtr, updatePtr);
		}
		strcat(linelist, "} ");
		strcat(linecolorlist, "} ");
		strcat(textlist, "} ");
		strcat(textcolorlist, "} ");
		strcat(rectlist, "} ");
		strcat(rectcolorlist, "} ");
		Tcl_VarEval(machPtr->interp, "BasVisual ", linelist, linecolorlist,
					textlist, textcolorlist, rectlist, rectcolorlist, (char *)NULL);

		break;
	}

	case TOMASULO: {
		char OpCode[7],  Vj[20], Qj[20], Vk[20], Qk[20], w[20];
		char *Vblank = "         ";
		char *Qblank = "        ";
		int execBegin;
		Update *updateLinePtr = NULL;

		machPtr->cycleDisplayCount = machPtr->cycleCount;
		switch (unitType) {
		case BRANCH:
			unitType = INT;
		case INT:
			num_units = machPtr->num_int_units;
			latency = machPtr->int_latency;
			break;
		case FP_ADD:
			num_units = machPtr->num_add_units;
			latency = machPtr->fp_add_latency;
			break;
		case FP_MUL:
			num_units = machPtr->num_mul_units;
			latency = machPtr->fp_mul_latency;
			break;
		case FP_DIV:
			latency = machPtr->fp_div_latency;
			if (machPtr->fp_div_exist) {
				num_units = machPtr->num_div_units;
			} else {
				unitType = FP_MUL;
				num_units = machPtr->num_mul_units;
			}
			break;
		case LOAD_BUF:
			load_store = 1;
			if (machPtr->ld_st_exist) {
				num_units = machPtr->num_load_bufs;
				latency = machPtr->load_buf_latency;
			} else {
				unitType = INT;
				num_units = machPtr->num_int_units;
				latency = machPtr->int_latency;
			}
			break;
		case STORE_BUF:
			load_store = 1;
			if (machPtr->ld_st_exist) {
				num_units = machPtr->num_store_bufs;
				latency = machPtr->store_buf_latency;
			} else {
				unitType = INT;
				num_units = machPtr->num_int_units;
				latency = machPtr->int_latency;
			}
			break;
		}


issueCheck1:
		for (i = 0, soonest = 0x7fffffff; i < num_units; i++) {
			if (!(j = machPtr->func_units[unitType][i]))
				break;
			else if (j < soonest)
				soonest = j;
		}
		if (i == num_units) {
			machPtr->stalls = soonest - machPtr->cycleCount;
			machPtr->cycleCount = soonest;
			Isu_WriteBack(machPtr);
			machPtr->cycleCount++;
			goto issueCheck1;
		}

		Tcl_VarEval(machPtr->interp, ".tom.c delete thickLine", (char *)NULL);

		opPtr = mallocOp(machPtr);
		opPtr->rs1 = -1;
		opPtr->rs2 = -1;
		opPtr->rs1Ready = 0;
		opPtr->rs2Ready = 0;

		while (!machPtr->cycleDisplayMode && machPtr->updateList != NULL &&
			   machPtr->updateList->displayCycle <= machPtr->cycleCount) {
			updatePtr = machPtr->updateList;
			machPtr->updateList = updatePtr->nextPtr;
			if (updatePtr->readyCycle >= machPtr->cycleCount) {
				Tcl_SetVar(machPtr->interp, "msg", updatePtr->msg, TCL_GLOBAL_ONLY);
				if (updatePtr->actionType == DELETE_INSERT) {
					Tcl_VarEval(machPtr->interp, "DeleteAndInsert ", updatePtr->w, "$msg",
								(char *)NULL);
				} else if (updatePtr->actionType == SHOW_CDB_LINE) {
					updateLinePtr = updatePtr;
					continue;
				}
			}
			Isu_FreeUpdate(machPtr, updatePtr);

		}
		if (updateLinePtr != NULL) {
			updateLinePtr->nextPtr = machPtr->updateList;
			machPtr->updateList = updateLinePtr;
		}


		sprintf(w, "%d", wordPtr->line);
		GenUpdateStruct(machPtr, "", machPtr->cycleDisplayMode,
						HIGHLIGHT_CODE, machPtr->cycleCount, machPtr->cycleCount, w,
						"", "", "", "");

		sprintf(OpCode, "%-6.6s", operationNames[wordPtr->opCode]);
		strcpy(msg, OpCode);

		if (!machPtr->cycleDisplayMode)
			Tcl_VarEval(machPtr->interp,
						".tom.c.counter configure -text $cycleCount; UnHighLight", (char *)NULL);

		execBegin = machPtr->cycleCount + 1;
		opPtr->unit = unitType * MAX_FUNC_UNITS + i;
		opPtr->resultType = rdType;



		if (rs1Type != NON_OP && rs1Type != IMM_OP) {
			opPtr->rs1 = wordPtr->rs1;
			if (machPtr->waiting_regs[wordPtr->rs1]) {
				opPtr->rs1Ready = machPtr->waiting_regs[wordPtr->rs1] + 1;
				execBegin = opPtr->rs1Ready;
				opPtr2 = WhichOp(machPtr, wordPtr->rs1);
				strcat(msg, Vblank);
				sprintf(Qj, "%s ", Units[opPtr2->unit]);
				strcat(msg, Qj);
				switch (rs1Type) {
				case INT_OP:
					sprintf(Vj, "%d",
							*((int *)&(opPtr->source1[0])) =
								*((int *)&(opPtr2->result[0])));
					break;
				case SFP_OP:
					sprintf(Vj, "%f",
							*((float *)&(opPtr->source1[0])) =
								*((float *)&(opPtr2->result[0])));
					break;
				case DFP_OP:
					sprintf(Vj, "%f",
							*((double *)&(opPtr->source1[0])) =
								*((double *)&(opPtr2->result[0])));
					break;
				}
				GenEightBit(Vj);
			} else {
				sprintf(w, "1.tom.c.%s%d ", functionTypes[unitType], i);
				GenUpdateStruct(machPtr, "", machPtr->cycleDisplayMode, SHOW_LINE,
								machPtr->cycleCount, machPtr->cycleCount, w, "", "", "", "");
				switch (rs1Type) {
				case INT_OP:
					sprintf(Vj, "%d",
							*((int *)&(opPtr->source1[0])) =
								*((int *)&(machPtr->regs[wordPtr->rs1])));
					break;
				case SFP_OP:
					sprintf(Vj, "%f",
							*((float *)&(opPtr->source1[0])) =
								*((float *)&(machPtr->regs[wordPtr->rs1])));
					break;
				case DFP_OP:
					sprintf(Vj, "%f",
							*((double *)&(opPtr->source1[0])) =
								*((double *)&(machPtr->regs[wordPtr->rs1])));
					break;
				}
				GenEightBit(Vj);
				strcat(msg, Vj);
				strcat(msg, Qblank);
			}
		} else {
			strcat(msg, "                ");
			sprintf(Vj, "         ");
		}

		if (rs2Type != NON_OP && rs2Type != IMM_OP) {
			opPtr->rs2 = wordPtr->rs2;
			if (machPtr->waiting_regs[wordPtr->rs2]) {
				opPtr->rs2Ready = machPtr->waiting_regs[wordPtr->rs2] + 1;
				if (execBegin < opPtr->rs2Ready)
					execBegin = opPtr->rs2Ready;
				opPtr2 = WhichOp(machPtr, wordPtr->rs2);
				strcat(msg, Vblank);
				sprintf(Qk, "%s ", Units[opPtr2->unit]);
				strcat(msg, Qk);
				switch (rs2Type) {
				case INT_OP:
					sprintf(Vk, "%d",
							*((int *)&(opPtr->source2[0])) =
								*((int *)&(opPtr2->result[0])));
					break;
				case SFP_OP:
					sprintf(Vk, "%f",
							*((float *)&(opPtr->source2[0])) =
								*((float *)&(opPtr2->result[0])));
					break;
				case DFP_OP:
					sprintf(Vk, "%f",
							*((double *)&(opPtr->source2[0])) =
								*((double *)&(opPtr2->result[0])));
					break;
				}
				GenEightBit(Vk);
			} else {
				sprintf(w, "2.tom.c.%s%d ", functionTypes[unitType], i);
				GenUpdateStruct(machPtr, "", machPtr->cycleDisplayMode, SHOW_LINE,
								machPtr->cycleCount, machPtr->cycleCount, w, "", "", "", "");
				switch (rs2Type) {
				case INT_OP:
					sprintf(Vk, "%d",
							*((int *)&(opPtr->source2[0])) =
								*((int *)&(machPtr->regs[wordPtr->rs2])));
					break;
				case SFP_OP:
					sprintf(Vk, "%f",
							*((float *)&(opPtr->source2[0])) =
								*((float *)&(machPtr->regs[wordPtr->rs2])));
					break;
				case DFP_OP:
					sprintf(Vk, "%f",
							*((double *)&(opPtr->source2[0])) =
								*((double *)&(machPtr->regs[wordPtr->rs2])));
					break;
				}
				GenEightBit(Vk);
				strcat(msg, Vk);
				strcat(msg, Qblank);
			}
		} else {
			strcat(msg, Vblank);
			strcat(msg, Qblank);
			sprintf(Vk, "         ");
		}

		opPtr->rdReady = execBegin + latency;
		if (machPtr->opsList == NULL ||
			opPtr->rdReady < machPtr->opsList->rdReady) {
			opPtr->nextPtr = machPtr->opsList;
			machPtr->opsList = opPtr;
		} else {
			/* POSSIBLE BUG. might want to use a loop to check this condition
			   See how multiple write requests to write ports is checked in
			   BASICPIPE case.
			*/
			if (opPtr->rdReady == machPtr->opsList->rdReady && rdType != NON_OP)
				opPtr->rdReady++;

			for (opPtr2 = machPtr->opsList; opPtr2->nextPtr != NULL;
				 opPtr2 = opPtr2->nextPtr) {
				if (opPtr->rdReady < opPtr2->nextPtr->rdReady) break;
				if (opPtr->rdReady == opPtr2->nextPtr->rdReady && rdType != NON_OP)
					opPtr->rdReady++;
			}
			opPtr->nextPtr = opPtr2->nextPtr;
			opPtr2->nextPtr = opPtr;
		}
		machPtr->func_units[unitType][i] = opPtr->rdReady;

		if (load_store) {
			opPtr->address = opPtr->source1[0] + wordPtr->extra;
			if (unitType == LOAD_BUF)
				sprintf(msg, "(0x%08x)", opPtr->address);
			else if (unitType == STORE_BUF)
				sprintf(msg, "%s(0x%08x)", &msg[22], opPtr->address);
		}

		sprintf(w, ".tom.c.%s%d ", functionTypes[unitType], i);
		GenUpdateStruct(machPtr, w, machPtr->cycleDisplayMode, INSERT_HIGHLIGHT,
						machPtr->cycleCount, opPtr->rdReady, msg, "", "", "", "");

		GenUpdateStruct(machPtr, "", machPtr->cycleDisplayMode, SHOW_LINE,
						machPtr->cycleCount, machPtr->cycleCount, "i", w, "", "", "");


		sprintf(msg, "%-20.20s",
				Asm_Disassemble(machPtr, machPtr->memPtr[machPtr->regs[PC_REG]].value,
								Sim_GetPC(machPtr) & ~0x3));
		GenUpdateStruct(machPtr, ".tom.c.instr ", machPtr->cycleDisplayMode,
						DELETE_INSERT, machPtr->cycleDisplayCount, machPtr->cycleCount,
						msg, "", "", "", "");

		if (latency == 1)
			sprintf(part1,  "%8d %8d          ",
					machPtr->cycleCount, execBegin);
		else
			sprintf(part1,  "%8d %8d-%-8d ",
					machPtr->cycleCount, execBegin, opPtr->rdReady - 1);
		strcat(msg, part1);
		sprintf(part1, "%-8d", opPtr->rdReady);
		strcat(msg, part1);
		GenUpdateStruct(machPtr, ".tom.c.frame.table  ", machPtr->cycleDisplayMode,
						ADD_END, machPtr->cycleCount, machPtr->cycleCount, msg, "", "", "", "");



		if (machPtr->cycleCount < opPtr->rs1Ready ||
			machPtr->cycleCount < opPtr->rs2Ready) {
			if (!load_store) {
				if (machPtr->cycleCount < opPtr->rs1Ready &&
					opPtr->rs1Ready < opPtr->rs2Ready) {
					GenUpdateStruct(machPtr, w, 1, DELETE_INSERT,
									opPtr->rs1Ready, opPtr->rdReady, OpCode, Vj, Qblank, Vblank, Qk);
				} else if (machPtr->cycleCount < opPtr->rs2Ready &&
						   opPtr->rs2Ready < opPtr->rs1Ready) {
					GenUpdateStruct(machPtr, w, 1, DELETE_INSERT,
									opPtr->rs2Ready, opPtr->rdReady, OpCode, Vblank, Qj, Vk, Qblank);
				}
				GenUpdateStruct(machPtr, w, 1, DELETE_INSERT,
								execBegin, opPtr->rdReady, OpCode, Vj, Qblank, Vk, Qblank);
			} else if (unitType == STORE_BUF) {
				sprintf(msg, "(0x%08x)", opPtr->address);
				GenUpdateStruct(machPtr, w, 1, DELETE_INSERT,
								opPtr->rs2Ready, opPtr->rdReady, Vk, Qblank, msg, "", "");
			}
		}

		opPtr->rd = wordPtr->rd;
		if (rdType && wordPtr->rd) {
			sprintf(msg, ".tom.c.%s%d", functionTypes[unitType], i);
			GenUpdateStruct(machPtr, w, 1, SHOW_CDB_LINE,
							opPtr->rdReady, opPtr->rdReady, msg, "", "", "", "");
			machPtr->waiting_regs[wordPtr->rd] = opPtr->rdReady;
			machPtr->Qi_regs[wordPtr->rd] = opPtr->unit;
			sprintf(w, ".tom.c.reg%d ", wordPtr->rd);
			GenUpdateStruct(machPtr, w, machPtr->cycleDisplayMode, INSERT_HIGHLIGHT,
							machPtr->cycleCount, opPtr->rdReady, Units[opPtr->unit],
							"", "", "", "");
			if (rdType  == DFP_OP) {
				machPtr->waiting_regs[wordPtr->rd + 1] = opPtr->rdReady;
				machPtr->Qi_regs[wordPtr->rd + 1] = opPtr->unit;
				sprintf(w, ".tom.c.reg%d ", wordPtr->rd + 1);
				GenUpdateStruct(machPtr, w, machPtr->cycleDisplayMode, INSERT_HIGHLIGHT,
								machPtr->cycleCount, opPtr->rdReady, Units[opPtr->unit],
								"", "", "", "");

			}
		}


		break;
	}
	case SCOREBOARD: {
		char OpCode[20], Qj[10], Qk[10], Rj[10], Rk[10], w[20], temp[10];
		char *Qblank = "        ";
		int readBegin;

		machPtr->cycleDisplayCount = machPtr->cycleCount;
		switch (unitType) {
		case LOAD_BUF:
		case STORE_BUF:
			load_store = 1;
		case INT:
		case BRANCH:
			unitType = INT;
			num_units = machPtr->num_int_units;
			latency = machPtr->int_latency;
			break;
		case FP_ADD:
			num_units = machPtr->num_add_units;
			latency = machPtr->fp_add_latency;
			break;
		case FP_MUL:
			num_units = machPtr->num_mul_units;
			latency = machPtr->fp_mul_latency;
			break;
		case FP_DIV:
			latency = machPtr->fp_div_latency;
			if (machPtr->fp_div_exist) {
				num_units = machPtr->num_div_units;
			} else {
				unitType = FP_MUL;
				num_units = machPtr->num_mul_units;
			}
			break;
		}

issueCheck2:
		wait = 0;
		if (rdType != NON_OP && machPtr->waiting_regs[wordPtr->rd])
			wait = machPtr->waiting_regs[wordPtr->rd];
		for (i = 0, soonest = 0x7fffffff; i < num_units; i++) {
			if (!(j = machPtr->func_units[unitType][i]))
				break;
			else if (j < soonest)
				soonest = j;
		}
		if (i == num_units && (soonest > wait))
			wait = soonest;
		if (wait) {
			/*    machPtr->stalls = wait - machPtr->cycleCount + 1; */
			machPtr->cycleCount = wait;
			Isu_WriteBack(machPtr);
			machPtr->cycleCount++;
			goto issueCheck2;
		}

		Tcl_VarEval(machPtr->interp, ".sco.c delete thickLine", (char *)NULL);

		opPtr = mallocOp(machPtr);
		opPtr->rs1 = -1;
		opPtr->rs2 = -1;
		opPtr->rs1Ready = machPtr->cycleCount + 1;
		opPtr->rs2Ready = machPtr->cycleCount + 1;

		while (!machPtr->cycleDisplayMode && machPtr->updateList != NULL &&
			   machPtr->updateList->displayCycle <= machPtr->cycleCount) {
			updatePtr = machPtr->updateList;
			machPtr->updateList = updatePtr->nextPtr;
			if (updatePtr->readyCycle >= machPtr->cycleCount) {
				Tcl_SetVar(machPtr->interp, "msg", updatePtr->msg, TCL_GLOBAL_ONLY);
				if (updatePtr->actionType == DELETE_INSERT) {
					Tcl_VarEval(machPtr->interp, "DeleteAndInsert ", updatePtr->w, "$msg",
								(char *)NULL);
				} else if (updatePtr->actionType == SHOW_LINE) {
					Tcl_VarEval(machPtr->interp, "ShowResLine ", "$msg", (char *)NULL);
				} else if (updatePtr->actionType == HIGHLIGHT_WRITE) {
					Tcl_VarEval(machPtr->interp, "HighLightWrite ", updatePtr->w,
								(char *)NULL);
				}
			}
			Isu_FreeUpdate(machPtr, updatePtr);
		}

		sprintf(w, "%d", wordPtr->line);
		GenUpdateStruct(machPtr, "", machPtr->cycleDisplayMode,
						HIGHLIGHT_CODE, machPtr->cycleCount, machPtr->cycleCount, w,
						"", "", "", "");

		sprintf(OpCode, "Yes %-8.8s  ", operationNames[wordPtr->opCode]);

		if (!machPtr->cycleDisplayMode)
			Tcl_VarEval(machPtr->interp,
						".sco.c.counter configure -text $cycleCount; UnHighLight", (char *)NULL);

		readBegin = machPtr->cycleCount + 1;
		opPtr->unit = unitType * MAX_FUNC_UNITS + i;
		opPtr->resultType = rdType;

		for (j = 0; j < 3; j++) {
			switch (j) {
			case 0:
				regType = rdType;
				regNumber = wordPtr->rd;
				break;
			case 1:
				regType = rs1Type;
				regNumber = wordPtr->rs1;
				break;
			case 2:
				regType = rs2Type;
				regNumber = wordPtr->rs2;
				break;
			}
			switch (regType) {
			case INT_OP:
				if (wordPtr->rd != FP_STATUS)
					sprintf(temp, "R%-2d  ", regNumber);
				else
					sprintf(temp, "FPS  ");
				break;
			case SFP_OP:
			case DFP_OP:
				sprintf(temp, "F%-2d  ", regNumber - 32);
				break;
			default:
				sprintf(temp, "     ");
			}
			strcat(OpCode, temp);
		}
		strcpy(msg, OpCode);

		sprintf(Qj, Qblank);
		sprintf(Rj, "Yes ");
		if (rs1Type != NON_OP && rs1Type != IMM_OP) {
			opPtr->rs1 = wordPtr->rs1;
			if (machPtr->waiting_regs[wordPtr->rs1]) {
				opPtr->rs1Ready = machPtr->waiting_regs[wordPtr->rs1] +
								  machPtr->sameCycle_regs[wordPtr->rs1] + 1;
				readBegin = opPtr->rs1Ready;
				machPtr->sameCycle_regs[wordPtr->rs1]++;
				opPtr2 = WhichOp(machPtr, wordPtr->rs1);
				if (machPtr->cycleCount != machPtr->waiting_regs[wordPtr->rs1]) {
					sprintf(Qj, "%s  ", Units[opPtr2->unit]);
					sprintf(Rj, "No  ");
				}
				switch (rs1Type) {
				case INT_OP:
					*((int *)&(opPtr->source1[0])) =
						*((int *)&(opPtr2->result[0]));
					break;
				case SFP_OP:
					*((float *)&(opPtr->source1[0])) =
						*((float *)&(opPtr2->result[0]));
					break;
				case DFP_OP:
					*((double *)&(opPtr->source1[0])) =
						*((double *)&(opPtr2->result[0]));
					break;
				}
			} else {
				switch (rs1Type) {
				case INT_OP:
					*((int *)&(opPtr->source1[0])) =
						*((int *)&(machPtr->regs[wordPtr->rs1]));
					break;
				case SFP_OP:
					*((float *)&(opPtr->source1[0])) =
						*((float *)&(machPtr->regs[wordPtr->rs1]));
					break;
				case DFP_OP:
					*((double *)&(opPtr->source1[0])) =
						*((double *)&(machPtr->regs[wordPtr->rs1]));
					break;
				}
			}
			sprintf(w, "1.sco.c.%s%d ", functionTypes[unitType], i);
			GenUpdateStruct(machPtr, "", 1, SHOW_LINE,
							opPtr->rs1Ready, opPtr->rs1Ready, w, "", "", "", "");
		}
		strcat(msg, Qj);


		sprintf(Qk, Qblank);
		sprintf(Rk, "Yes  ");
		if (rs2Type != NON_OP && rs2Type != IMM_OP) {
			opPtr->rs2 = wordPtr->rs2;
			if (machPtr->waiting_regs[wordPtr->rs2]) {
				opPtr->rs2Ready = machPtr->waiting_regs[wordPtr->rs2] +
								  machPtr->sameCycle_regs[wordPtr->rs2] + 1;
				if (readBegin < opPtr->rs2Ready) {
					if (opPtr->rs1Ready)
						machPtr->sameCycle_regs[wordPtr->rs1]--;
					readBegin = opPtr->rs2Ready;
					machPtr->sameCycle_regs[wordPtr->rs2]++;
				}
				opPtr2 = WhichOp(machPtr, wordPtr->rs2);
				if (machPtr->cycleCount != machPtr->waiting_regs[wordPtr->rs2]) {
					sprintf(Qk, "%s  ", Units[opPtr2->unit]);
					sprintf(Rk, "No   ");
				}
				switch (rs2Type) {
				case INT_OP:
					*((int *)&(opPtr->source2[0])) =
						*((int *)&(opPtr2->result[0]));
					break;
				case SFP_OP:
					*((float *)&(opPtr->source2[0])) =
						*((float *)&(opPtr2->result[0]));
					break;
				case DFP_OP:
					*((double *)&(opPtr->source2[0])) =
						*((double *)&(opPtr2->result[0]));
					break;
				}
			} else {
				opPtr->rs2Ready = machPtr->cycleCount + 1;
				switch (rs2Type) {
				case INT_OP:
					*((int *)&(opPtr->source2[0])) =
						*((int *)&(machPtr->regs[wordPtr->rs2]));
					break;
				case SFP_OP:
					*((float *)&(opPtr->source2[0])) =
						*((float *)&(machPtr->regs[wordPtr->rs2]));
					break;
				case DFP_OP:
					*((double *)&(opPtr->source2[0])) =
						*((double *)&(machPtr->regs[wordPtr->rs2]));
					break;
				}
			}
			sprintf(w, "2.sco.c.%s%d ", functionTypes[unitType], i);
			GenUpdateStruct(machPtr, "", 1, SHOW_LINE,
							opPtr->rs2Ready, opPtr->rs2Ready, w, "", "", "", "");
		}
		strcat(msg, Qk);
		strcat(msg, Rj);
		strcat(msg, Rk);

		opPtr->rd = wordPtr->rd;
		opPtr->rdReady = readBegin + latency + 1;
		/* check for WAR hazard */
		if (rdType && wordPtr->rd) {
			int readBegin2;
			wait = opPtr->rdReady;
			for (opPtr2 = machPtr->opsList; opPtr2 != NULL; opPtr2 = opPtr2->nextPtr) {
				readBegin2 = (opPtr2->rs1Ready > opPtr2->rs2Ready) ?
							 opPtr2->rs1Ready : opPtr2->rs2Ready;
				if ((opPtr2->rs1 == opPtr->rd || opPtr2->rs2 == opPtr->rd) &&
					opPtr->rdReady  < readBegin2 + 1 && wait < readBegin2 + 1)
					wait = readBegin2 + 1;
			}
			opPtr->rdReady = wait;

		}

		if (machPtr->opsList == NULL ||
			opPtr->rdReady < machPtr->opsList->rdReady) {
			opPtr->nextPtr = machPtr->opsList;
			machPtr->opsList = opPtr;
		} else {
			for (opPtr2 = machPtr->opsList; opPtr2->nextPtr != NULL;
				 opPtr2 = opPtr2->nextPtr)
				if (opPtr->rdReady < opPtr2->nextPtr->rdReady) break;
			opPtr->nextPtr = opPtr2->nextPtr;
			opPtr2->nextPtr = opPtr;
		}
		machPtr->func_units[unitType][i] = opPtr->rdReady;

		if (load_store)  {
			opPtr->address = opPtr->source1[0] + wordPtr->extra;
		}
		sprintf(w, ".sco.c.%s%d ", functionTypes[unitType], i);
		GenUpdateStruct(machPtr, w, machPtr->cycleDisplayMode, INSERT_HIGHLIGHT,
						machPtr->cycleCount, opPtr->rdReady, msg, "", "", "", "");
		GenUpdateStruct(machPtr, w, 1, HIGHLIGHT_WRITE, opPtr->rdReady, opPtr->rdReady,
						"", "", "", "", "");
		GenUpdateStruct(machPtr, "", 1, SHOW_LINE, opPtr->rdReady, opPtr->rdReady,
						"r", w, "", "", "");

		sprintf (msg, "%-20.20s %8d %8d %8d",
				 Asm_Disassemble(machPtr, machPtr->memPtr[machPtr->regs[PC_REG]].value,
								 Sim_GetPC(machPtr) & ~0x3),
				 machPtr->cycleCount, readBegin, readBegin + 1);
		if (opPtr->rdReady == (readBegin + 2))
			sprintf(part1, "          %-8d", opPtr->rdReady);
		else
			sprintf(part1, "-%-8d %-8d", opPtr->rdReady - 1, opPtr->rdReady);
		strcat(msg, part1);
		GenUpdateStruct(machPtr, ".sco.c.frame.table  ", machPtr->cycleDisplayMode,
						ADD_END, machPtr->cycleCount, machPtr->cycleCount, msg, "", "", "", "");



		if (machPtr->cycleCount < opPtr->rs1Ready - 1 &&
			opPtr->rs1Ready < opPtr->rs2Ready) {
			GenUpdateStruct(machPtr, w, 1, DELETE_INSERT,
							machPtr->waiting_regs[wordPtr->rs1],
							opPtr->rdReady, OpCode, Qblank, Qk, "Yes No  ", "");
		} else if (machPtr->cycleCount < opPtr->rs2Ready - 1 &&
				   opPtr->rs2Ready < opPtr->rs1Ready) {
			GenUpdateStruct(machPtr, w, 1, DELETE_INSERT,
							machPtr->waiting_regs[wordPtr->rs2],
							opPtr->rdReady, OpCode, Qj, Qblank, "No  Yes ", "");
		} else if (machPtr->cycleCount < opPtr->rs1Ready - 1 &&
				   opPtr->rs1Ready == opPtr->rs2Ready) {
			GenUpdateStruct(machPtr, w, 1, DELETE_INSERT,
							machPtr->waiting_regs[wordPtr->rs1],
							opPtr->rdReady, OpCode, Qblank, Qblank, "Yes Yes ", "");
		} else if (machPtr->cycleCount < opPtr->rs1Ready - 1 &&
				   opPtr->rs2 == -1) {
			GenUpdateStruct(machPtr, w, 1, DELETE_INSERT,
							machPtr->waiting_regs[wordPtr->rs1],
							opPtr->rdReady, OpCode, Qblank, Qblank, "Yes Yes  ", "");
		} else if (machPtr->cycleCount < opPtr->rs2Ready  - 1 &&
				   opPtr->rs1 == -1) {
			GenUpdateStruct(machPtr, w, 1, DELETE_INSERT,
							machPtr->waiting_regs[wordPtr->rs2],
							opPtr->rdReady, OpCode, Qblank, Qblank, "Yes  Yes ", "");
		}
		GenUpdateStruct(machPtr, w, 1, DELETE_INSERT,
						readBegin, opPtr->rdReady, OpCode, Qblank, Qblank, "No  No  ", "");


		if (rdType && wordPtr->rd) {
			machPtr->waiting_regs[wordPtr->rd] = opPtr->rdReady;
			machPtr->Qi_regs[wordPtr->rd] = opPtr->unit;
			machPtr->sameCycle_regs[wordPtr->rd] = 0;
			sprintf(w, ".sco.c.reg%d ", wordPtr->rd);
			GenUpdateStruct(machPtr, w, machPtr->cycleDisplayMode, INSERT_HIGHLIGHT,
							machPtr->cycleCount, opPtr->rdReady, Units[opPtr->unit],
							"", "", "", "");
			if (rdType  == DFP_OP) {
				machPtr->waiting_regs[wordPtr->rd + 1] = opPtr->rdReady;
				machPtr->Qi_regs[wordPtr->rd + 1] = opPtr->unit;
				sprintf(w, ".sco.c.reg%d ", wordPtr->rd + 1);
				GenUpdateStruct(machPtr, w, machPtr->cycleDisplayMode, INSERT_HIGHLIGHT,
								machPtr->cycleCount, opPtr->rdReady, Units[opPtr->unit],
								"", "", "", "");

			}
		}
		break;

	}
	}

	return opPtr;
}


void
Isu_CleanFinished (machPtr, window, cycleCount)
DLX *machPtr;              /* Machine of interest. */
char *window;
int cycleCount;
{
	Op *opPtr;
	int unitType, unitNumber;
	char w[20];


	while (machPtr->finishList != NULL) {
		opPtr = machPtr->finishList;
		machPtr->finishList = opPtr->nextPtr;
		unitType = opPtr->unit / MAX_FUNC_UNITS;
		unitNumber = opPtr->unit % MAX_FUNC_UNITS;
		sprintf(w, "%s.c.%s%d ", window, functionTypes[unitType], unitNumber);
		Tcl_VarEval(machPtr->interp, "lindex [", w, " tag configure gray -background] 4",
					(char *)NULL);
		if (!strcmp(machPtr->interp->result, "gray")) {
			Tcl_VarEval(machPtr->interp, w, "delete 1.0 end", (char *)NULL);
		}

		if (opPtr->resultType != NON_OP &&
			machPtr->waiting_regs[opPtr->rd] == 0) {
			sprintf(w, "%s.c.reg%d ", window, opPtr->rd);
			Tcl_VarEval(machPtr->interp, w, "delete 1.0 end", (char *)NULL);
			if (opPtr->resultType == DFP_OP) {
				sprintf(w, "%s.c.reg%d ", window, opPtr->rd + 1);
				Tcl_VarEval(machPtr->interp, w, "delete 1.0 end", (char *)NULL);
			}
		}

		Isu_FreeOp (machPtr, opPtr);
	}

	while (machPtr->opsList != NULL &&
		   machPtr->opsList->rdReady <= cycleCount) {

		opPtr = machPtr->opsList;
		machPtr->opsList = opPtr->nextPtr;
		unitType = opPtr->unit / MAX_FUNC_UNITS;
		unitNumber = opPtr->unit % MAX_FUNC_UNITS;
		if (opPtr->rdReady < cycleCount) {
			sprintf(w, "%s.c.%s%d ", window, functionTypes[unitType], unitNumber);
			Tcl_VarEval(machPtr->interp, w, "delete 1.0 end", (char *)NULL);
			if (opPtr->resultType != NON_OP &&
				machPtr->waiting_regs[opPtr->rd] == 0) {
				sprintf(w, "%s.c.reg%d ", window, opPtr->rd);
				Tcl_VarEval(machPtr->interp, w, "delete 1.0 end", (char *)NULL);
				if (opPtr->resultType == DFP_OP) {
					sprintf(w, "%s.c.reg%d ", window, opPtr->rd + 1);
					Tcl_VarEval(machPtr->interp, w, "delete 1.0 end", (char *)NULL);
				}
			}
			Isu_FreeOp(machPtr, opPtr);
		} else {
			opPtr->nextPtr = machPtr->finishList;
			machPtr->finishList = opPtr;
		}

	}

}


void
Isu_WriteBack (machPtr)
DLX *machPtr;              /* Machine of interest. */
{
	Op *opPtr;
	int unitType, unitNumber;

	opPtr = machPtr->opsList;
	while (opPtr != NULL && opPtr->rdReady <= machPtr->cycleCount) {
		unitType = opPtr->unit / MAX_FUNC_UNITS;
		unitNumber = opPtr->unit % MAX_FUNC_UNITS;
		if (machPtr->config == BASICPIPE ||
			machPtr->func_units[unitType][unitNumber] == opPtr->rdReady) {
			machPtr->func_units[unitType][unitNumber] = 0;

			if (opPtr->resultType != NON_OP &&
				opPtr->rdReady == machPtr->waiting_regs[opPtr->rd]) {
				switch (opPtr->resultType) {
				case INT_OP:
					*((int *)&(machPtr->regs[opPtr->rd])) = *((int *)&(opPtr->result[0]));
					break;
				case SFP_OP:
					*((float *)&(machPtr->regs[opPtr->rd])) = *((float *)&(opPtr->result[0]));
					break;
				case DFP_OP:
					*((double *)&(machPtr->regs[opPtr->rd])) = *((double *)&(opPtr->result[0]));
					machPtr->waiting_regs[opPtr->rd + 1] = 0;
					break;
				}
				machPtr->waiting_regs[opPtr->rd] = 0;
			}
		}
		if (machPtr->config == BASICPIPE) {
			machPtr->opsList = opPtr->nextPtr;
			Isu_FreeOp(machPtr, opPtr);
			opPtr = machPtr->opsList;
		} else {
			opPtr = opPtr->nextPtr;
		}
	}



	if (machPtr->config != BASICPIPE && !machPtr->cycleDisplayMode) {
		if (machPtr->config == TOMASULO)
			Isu_CleanFinished (machPtr, ".tom", machPtr->cycleCount);
		else
			Isu_CleanFinished (machPtr, ".sco", machPtr->cycleCount);
	}

}



/*
 *----------------------------------------------------------------------
 *
 * WhichOp --
 *
 *	Return a pointer to Op for which a givn FP register is waiting.
 *
 * Results:
 *	See above
 *
 * Side effects:
 *
 *----------------------------------------------------------------------
 */

static Op *
WhichOp(machPtr, number)
DLX *machPtr;			/* Machine description. */
int number;			        /* FP register number . */
{
	Op *opPtr;
	int unit = machPtr->Qi_regs[number];

	for (opPtr = machPtr->opsList; opPtr != NULL; opPtr = opPtr->nextPtr)
		if (opPtr->unit == unit)
			return opPtr;

	/* should not reach here */
	return NULL;

}


/*
 *----------------------------------------------------------------------
 *
 * mallocOp --
 *
 *	Return a pointer to an unused Op structure.
 *
 * Results:
 *	See above
 *
 * Side effects:
 *
 *----------------------------------------------------------------------
 */

static Op *
mallocOp(machPtr)
DLX *machPtr;			/* Machine description. */
{
	Op *p;

	if (!machPtr->opFree)
		p = (Op *)malloc(sizeof(Op));
	else {
		p = machPtr->opFree;
		machPtr->opFree = machPtr->opFree->nextPtr;
	}
	return p;
}


/*
 *----------------------------------------------------------------------
 *
 * Isu_FreeOp --
 *
 *	Free what is pointed to
 *
 * Results:
 *	See above
 *
 * Side effects:
 *
 *----------------------------------------------------------------------
 */

void
Isu_FreeOp(machPtr, p)
DLX *machPtr;			/* Machine description. */
Op *p;
{
	p->nextPtr = machPtr->opFree;
	machPtr->opFree = p;
}


/*
 *----------------------------------------------------------------------
 *
 * mallocUpdate --
 *
 *	Return a pointer to an unused Update structure.
 *
 * Results:
 *	See above
 *
 * Side effects:
 *
 *----------------------------------------------------------------------
 */

static Update *
mallocUpdate(machPtr)
DLX *machPtr;			/* Machine description. */
{
	Update *p;

	if (!machPtr->updateFree)
		p = (Update *)malloc(sizeof(Update));
	else {
		p = machPtr->updateFree;
		machPtr->updateFree = machPtr->updateFree->nextPtr;
	}
	return p;
}


/*
 *----------------------------------------------------------------------
 *
 * Isu_FreeUpdate --
 *
 *	Free what is pointed to
 *
 * Results:
 *	See above
 *
 * Side effects:
 *
 *----------------------------------------------------------------------
 */

void
Isu_FreeUpdate(machPtr, p)
DLX *machPtr;			/* Machine description. */
Update *p;
{
	p->nextPtr = machPtr->updateFree;
	machPtr->updateFree = p;
}


/*
 *----------------------------------------------------------------------
 *
 * mallocBypass --
 *
 *	Return a pointer to an unused Bypass structure.
 *
 * Results:
 *	See above
 *
 * Side effects:
 *
 *----------------------------------------------------------------------
 */

static Bypass *
mallocBypass(machPtr)
DLX *machPtr;			/* Machine description. */
{
	Bypass *p;

	if (!machPtr->bypassFree)
		p = (Bypass *)malloc(sizeof(Bypass));
	else {
		p = machPtr->bypassFree;
		machPtr->bypassFree = machPtr->bypassFree->nextPtr;
	}
	return p;
}

/*
 *----------------------------------------------------------------------
 *
 * Isu_FreeBypass --
 *
 *	Free what is pointed to
 *
 * Results:
 *	See above
 *
 * Side effects:
 *
 *----------------------------------------------------------------------
 */

void
Isu_FreeBypass(machPtr, p)
Bypass *p;
DLX *machPtr;			/* Machine description. */
{
	p->nextPtr = machPtr->bypassFree;
	machPtr->bypassFree = p;
}


/*
 *----------------------------------------------------------------------
 *
 * BypassInsert --
 *
 *	Insert a bypass buffer element into the bypass list in basic pipeline.
 *
 * Results:
 *	See above
 *
 * Side effects:
 *
 *----------------------------------------------------------------------
 */

static void
BypassInsert(machPtr, buffer, displayCycle, reg)
DLX *machPtr;			/* Machine description. */
int buffer;
int displayCycle;
int reg;
{
	Bypass *bPtr;
	Bypass *bypassPtr;

	bypassPtr = mallocBypass(machPtr);
	bypassPtr->buffer = buffer;
	bypassPtr->displayCycle = displayCycle;
	bypassPtr->reg = reg;

	if (machPtr->bypassList == NULL ||
		displayCycle < machPtr->bypassList->displayCycle) {
		bypassPtr->nextPtr = machPtr->bypassList;
		machPtr->bypassList = bypassPtr;
	} else {
		for (bPtr = machPtr->bypassList; bPtr->nextPtr != NULL;
			 bPtr = bPtr->nextPtr)
			if (displayCycle < bPtr->nextPtr->displayCycle) break;
		bypassPtr->nextPtr = bPtr->nextPtr;
		bPtr->nextPtr = bypassPtr;
	}

}
