# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc mkDisc {} {
  wm title . "DLX Visual Simulator"
  wm iconname . "dlxview"
  wm geometry . +0+0
  global backInsCount errInfo config
  set backInsCount 2147483647
  option add Tk.*Background lavender
  option add Tk.*Frame.Relief raised
  option add Tk.*Frame.borderWidth 1
  option add Tk.*Frame.highlightThickness 0
  option add Tk.*Canvas.highlightThickness 0
  option add Tk.*Button.Width 12
  option add Tk.*Button.borderWidth 3
  option add Tk.*Button.foreground black
  option add Tk.*Button.Background skyblue
  option add Tk.*Button.activeForeground white
  option add Tk.*Button.activeBackground MediumOrchid
  option add Tk.*Button.padX 1
  option add Tk.*Button.padY 1
  option add Tk.*Menu.activeBackground MediumOrchid
  option add Tk.*Menubutton.activeBackground MediumOrchid
  option add Tk.*Radiobutton.Background skyblue
  option add Tk.*Radiobutton.activeBackground MediumOrchid
  option add Tk.*Radiobutton.selectColor MediumOrchid
  option add Tk.*Label.Font Helvetica?14?bold
  option add Tk.*Label.foreground black
  option add Tk.*Listbox.selectBackground MediumOrchid
  option add Tk.*Text.Relief raised
  option add Tk.*Text.borderWidth 1
  option add Tk.*Text.Font Courier?12?normal
  option add Tk.*Text.setgrid true
  option add Tk.*Text.height 10
  option add Tk.*Text.foreground black
  option add Tk.*Scrollbar.Relief raised
  option add Tk.*Scrollbar.borderWidth 1
  option add Tk.*Scrollbar.Background skyblue
  option add Tk.*Scrollbar.activeBackground MediumOrchid
  option add Tk.*Scrollbar.troughColor lavender
  option add Tk.*Scrollbar.highlightThickness 0
  option add Tk.*Scale.from 1
  option add Tk.*Scale.to 8
  option add Tk.*Scale.length 250
  option add Tk.*Scale.orient horizontal
  option add Tk.*Scale.troughColor skyblue
  option add Tk.*Scale.activeBackground MediumOrchid
  frame .button
  frame .code
  frame .menu
  pack .button .code .menu \
    -side bottom \
    -expand yes \
    -fill both
  frame .button.top \
    -relief flat
  frame .button.bot \
    -relief flat
  pack .button.top .button.bot \
    -side top \
    -expand yes \
    -fill both \
    -padx 10
  button .button.top.configure \
    -text configure  \
    -command "mkConfigure;  wm iconify .configure ; wm deiconify .configure" 
  button .button.top.load \
    -text load \
    -command mkLoad
  bind .button.top.load <Button-2> "Load [pwd]/f347.i
    Load [pwd]/f347.d
    Load [pwd]/f347.s
    .button.top.load configure -state disabled
    .button.top.stepforward configure -state normal
    .button.bot.stepbackward configure -state normal
    .button.top.nextcycle configure -state normal
    .button.bot.previouscycle configure -state normal
    .button.top.go configure -state normal
    .button.top.reset configure -state normal
    "
  button .button.top.stepforward \
    -text "step forward" \
    -command "mkGoStep Step"
  button .button.top.nextcycle \
    -text "next cycle" \
    -command "mkGoStep Cycle"
  button .button.top.go \
    -text go \
    -command "mkGoStep Go"
  button .button.top.reset  \
    -text reset \
    -command mkReset \
    -padx 1 \
    -pady 1
  button .button.top.quit \
    -text quit	\
    -command exit
  pack .button.top.configure .button.top.load .button.top.stepforward \
   .button.top.nextcycle .button.top.go .button.top.reset .button.top.quit \
    -side left \
    -expand yes \
    -padx 2 \
    -pady 5
  button .button.bot.stepbackward \
    -text "step back" \
    -command mkStepBack
  button .button.bot.previouscycle \
    -text "previous cycle" \
    -command mkPreviousCycle
  button .button.bot.stop \
    -text stop  \
    -command mkStop
  pack .button.bot.stepbackward \
    .button.bot.previouscycle .button.bot.stop \
    -side left \
    -expand yes \
    -padx 2 \
    -pady 5

  text .code.t \
    -yscrollcommand ".code.s set"

  scrollbar .code.s \
    -command ".code.t yview"

  pack .code.s \
    -side right \
    -fill y

  pack .code.t \
    -expand yes \
    -fill both

 .button.top.load configure -state disabled
 .button.top.stepforward configure -state disabled
 .button.bot.stepbackward configure -state disabled
 .button.top.nextcycle configure -state disabled
 .button.bot.previouscycle configure -state disabled
 .button.top.go configure -state disabled
 .button.top.reset configure -state disabled
 .button.bot.stop configure -state disabled

}
