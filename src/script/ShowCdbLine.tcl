# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc ShowCdbLine {w} {
  set c .tom.c
  set start [string index $w 7]
  if {$start == "l"} {
    $c create line 90 60 90 170 35 170 35 410 160 410 \
      -tag thickLine -width 10 -joinstyle miter -fill red
  }
  if {$start == "l" || $start == "i"} {
    $c create line 160 410 410 410 -tag thickLine -width 10 -joinstyle miter -fill red
  }
  if {$start == "l" || $start == "i" || $start == "a"} {
    $c create line 410 410 660 410 -tag thickLine -width 10 -joinstyle miter -fill red
  }
  if {$start == "l" || $start == "i" || $start == "a" || $start == "m"} {
    $c create line 660 410 910 410 -tag thickLine -width 10 -joinstyle miter -fill red
  }
   
  $c create line 910 410 1032 410 1032 15 620 15 620 37 \
    -tag thickLine -width 10 -joinstyle miter -fill red
    
  $c create line 1032 215 132 215 -tag thickLine -width 10 -joinstyle miter -fill red
  HighLightWrite $w
}
