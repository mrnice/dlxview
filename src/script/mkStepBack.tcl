# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc mkStepBack {} {
  global startAddress backInsCount insCount config fastmode
  if {$insCount == 1} {
    tk_dialog .warning {Warning} {You've already reached the beginning of your \
      program, no further back step can be excecuted.} warning 0 OK
    return
  }
  if {$config} {
    set backInsCount [expr $insCount-1]
    ResetMode 3
    Go $startAddress
  } else {
    if {$insCount == "2"} {
      set backInsCount 0
      ResetMode 3
      Step $startAddress
    } else {
      set backInsCount [expr $insCount-2]
      ResetMode 3
      Go $startAddress
      set fastmode 0
      step
    }
  }
}
