# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc SubConfigure {name element op} {
  global configureEnabled
  upvar $name configure
  if {[winfo exists .configure]} {
    if {$configureEnabled != "0" && $configure != "3"} {
      .configure.button.ok configure -state normal
    }
    switch $configure {
      0 {
          BasConfigure
        }
      1 {
          TomConfigure
        }
      2 {
          ScoConfigure
        }
      3 {}
    }
  }
}
