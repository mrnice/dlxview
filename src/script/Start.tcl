# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc Start {} {
  global pipeline_mode input_files start_address
  global config configureEnabled
  switch $pipeline_mode {
    BasicPipe {
      set config 0
      mkBasicPipe
    }
    Tomasulo {
      set config 1
      mkTomasulo
    }
    ScoreBoard {
      set config 2
      mkScoreBoard
    }
    default {
      puts "Unknown Pipelining Mode"
      exit
    }
  }
  foreach f $input_files {
    if {[string compare [string range $f 0 0] "/"] == "0"} {
      Load $f
    } else {
      Load [pwd]/$f
    }
  }
  
  SubConfigureOk
  set configureEnabled 0
  .button.top.load configure -state disabled
  .button.top.stepforward configure -state normal
  .button.bot.stepbackward configure -state normal
  .button.top.go configure -state normal
  .button.top.nextcycle configure -state normal
  .button.bot.previouscycle configure -state normal
  .button.top.reset configure -state normal
  if {[info exists start_address]} {
      Step $start_address
  } else {
      Step 256
  }
}
