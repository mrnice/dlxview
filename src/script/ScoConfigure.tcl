# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc ScoConfigure {{w .subconfigure}} {
  global num_int_units int_latency
  global num_add_units fp_add_latency
  global num_mul_units fp_mul_latency
  global num_div_units fp_div_latency
  global fp_div_exist
  global configureEnabled
  set num_int_unitsOrig $num_int_units
  set int_latencyOrig $int_latency
  set num_add_unitsOrig $num_add_units
  set fp_add_latencyOrig $fp_add_latency 
  set num_mul_unitsOrig $num_mul_units
  set fp_mul_latencyOrig $fp_mul_latency
  set num_div_unitsOrig $num_div_units
  set fp_div_latencyOrig $fp_div_latency
  catch {destroy $w}
  toplevel $w
  wm title $w "Scoreboarding Configuration"
  wm iconname $w "subconfigure"
  wm geometry $w +0+0
  frame $w.int
  frame $w.add
  frame $w.mul
  frame $w.div
  frame $w.button  
  set oldFocus [focus]
  if {$configureEnabled} {
    set scaletroco skyblue
    set scaleactbg MediumOrchid
  } else {
    set scaletroco lightgrey
    set scaleactbg lightgrey
  }
  pack  $w.int $w.add $w.mul $w.div $w.button \
    -side top \
    -fill x \
    -expand yes  
  scale $w.int.number \
    -tickinterval 7 \
    -label "integer unit number" \
    -command "ScaleValue num_int_units" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg 
  scale $w.int.latency \
    -to 3  \
    -tickinterval 2 \
    -label "integer unit latency (cycles)" \
    -command "ScaleValue int_latency" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg
  pack $w.int.number $w.int.latency \
    -side left \
    -fill x \
    -expand yes \
    -padx 5 \
    -pady 10
  scale $w.add.number \
    -tickinterval 7 \
    -label "FP add reservation station number" \
    -command "ScaleValue num_add_units" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg
  scale $w.add.latency \
    -to 5  \
    -tickinterval 4 \
    -label "FP add latency (cycles)" \
    -command "ScaleValue fp_add_latency" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg 
  pack $w.add.number $w.add.latency \
    -side left \
    -fill x \
    -expand yes \
    -padx 5 \
    -pady 10
  scale $w.mul.number \
    -tickinterval 7 \
    -label "FP multiply reservation station number" \
    -command "ScaleValue num_mul_units" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg
  scale $w.mul.latency \
    -to 10  \
    -tickinterval 9 \
    -label "FP multiply latency (cycles)" \
    -command "ScaleValue fp_mul_latency" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg
  pack $w.mul.number $w.mul.latency \
    -side left \
    -fill x \
    -expand yes \
    -padx 5 \
    -pady 10
  scale $w.div.number \
    -from 0 \
    -tickinterval 8 \
    -label "FP divide reservation station number" \
    -command "ScaleValue num_div_units" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg
  scale $w.div.latency \
    -to 50  \
    -tickinterval 49 \
    -label "FP divide latency (cycles)" \
    -command "ScaleValue fp_div_latency" \
    -troughcolor $scaletroco       \
    -activebackground $scaleactbg           
  pack $w.div.number $w.div.latency \
    -side left \
    -fill x \
    -expand yes \
    -padx 5 \
    -pady 10
  button $w.button.ok \
    -text Ok \
    -command  "SubConfigureOk 
       destroy $w
       focus $oldFocus"
  button $w.button.cancel \
    -text Cancel \
    -command "set num_int_units $num_int_unitsOrig
      set int_latency $int_latencyOrig
      set num_add_units $num_add_unitsOrig
      set fp_add_latency $fp_add_latencyOrig 
      set num_mul_units $num_mul_unitsOrig
      set fp_mul_latency $fp_mul_latencyOrig
      set num_div_units $num_div_unitsOrig
      set fp_div_latency $fp_div_latencyOrig
      destroy $w
      focus $oldFocus"
  pack $w.button.ok $w.button.cancel\
    -side left \
    -fill x \
    -expand yes
  $w.int.number set $num_int_units
  $w.int.latency set $int_latency
  $w.add.number set $num_add_units
  $w.add.latency set $fp_add_latency
  $w.mul.number set $num_mul_units
  $w.mul.latency set $fp_mul_latency
  $w.div.number set $num_div_units
  $w.div.latency set $fp_div_latency
  if {$configureEnabled == "0"} {
    $w.button.ok configure -state disabled
    $w.int.number configure -state disabled
    $w.int.latency configure -state disabled
    $w.add.number configure -state disabled
    $w.add.latency configure -state disabled
    $w.mul.number configure -state disabled
    $w.mul.latency configure -state disabled
    $w.div.number configure -state disabled
    $w.div.latency configure -state disabled
  }
  tkwait visibility $w
  grab $w
  focus $w
}
