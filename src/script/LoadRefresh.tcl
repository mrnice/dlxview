# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc LoadRefresh {{w .load}} {
  global curDir 
  set list [exec ls -a]
  $w.button.load configure -state disabled
  $w.entry.up.direntry delete 0 end
  $w.entry.up.direntry insert 0 $curDir
  $w.entry.up.direntry config -width [string length $curDir]
  $w.entry.down.fileentry delete 0 end
  $w.list.filelist delete 0 end
  $w.list.dirlist delete 0 end
  foreach i $list {
    if {[string match *.d $i] || [string match *.i $i] || 
        [string match *.s $i] || [file isdirectory $i]} {
      $w.list.dirlist insert end $i
    }
  }
  update
}
