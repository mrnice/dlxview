# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc mkBasicPipe {{w .bas}} {
  global num_add_units fp_add_latency add_ful_pipe
  global num_mul_units fp_mul_latency mul_ful_pipe
  global num_div_units fp_div_latency div_ful_pipe
  global fastmode
  catch {destroy $w}
  toplevel $w
  wm title $w "Basic DLX Pipeline"
  wm iconname $w "basic pipe"
  wm geometry $w +0+0
  frame $w.up \
    -bd 0
  canvas $w.block \
    -width 850 \
    -height 500 \
    -bd 0 \
    -scrollregion {0 0 850 500}
  canvas $w.datapath \
    -width 850 \
    -height 500 \
    -bd 0 \
    -scrollregion {0 0 850 500}
  pack $w.up $w.block \
    -side top \
    -fill x \
    -expand yes
  canvas $w.up.code \
    -width 150 \
    -height 180 \
    -bd 0 \
    -scrollregion {0 0 165 180}
  canvas $w.up.cycle \
    -width 650 \
    -height 180 \
    -bd 0 \
    -scrollregion {165 0 3000 180} \
    -xscrollcommand "$w.up.s set"
  for {set i 0} {$i < 58} {incr i} {
    set t strip$i
    $w.up.cycle create rectangle [expr 155+$i*50] 0 [expr 205+$i*50] 180 \
      -outline "" \
      -tags "$t strip"
  }
  scrollbar $w.up.s \
    -command "$w.up.cycle xview" \
    -relief raised \
    -orient horizontal
  pack $w.up.code \
    -side left \
    -fill y \
    -expand yes
  pack $w.up.cycle \
    -side top
  pack $w.up.s \
    -side top \
    -fill x \
    -expand yes

  font create bigfont -family Courier -size 12 -weight normal
  font create insfont -family Courier -size 12 -weight bold
  font create hugfont -family Courier -size 14 -weight bold
  font create desfont -family Courier -size 10 -weight normal
  font create subfont -family Courier -size 8 -weight normal
  font create labfont -family Helvetica -size 24 -weight normal

  set c $w.block
  $c create rectangle 50 220 110 280 \
    -tags {RectIF rect}
  $c create text 80 250 -font bigfont -text "IF"
  $c create rectangle 109 220 170 280 \
    -tags {RectID rect}
  $c create text 140 250 -font bigfont -text "ID"
  $c create rectangle 600 220 660 280
  $c create rectangle 601 221 659 250 -outline "" \
    -tags {RectMEM.1 rect}
  $c create rectangle 601 250 659 279 -outline "" \
    -tags {RectMEM.2 rect}
  $c create text 630 250 -font bigfont -text "MEM"
  $c create rectangle 659 220 720 280
  $c create rectangle 660 221 719 250 -outline "" \
    -tags {RectWB.1 rect}
  $c create rectangle 660 250 719 279 -outline "" \
    -tags {RectWB.2 rect}
  $c create text 690 250 -font bigfont -text "WB"
  set total_units [expr 1+$num_add_units+$num_mul_units+$num_div_units]

  set step_large [expr 500.0/($total_units+1)]
  set step_small [expr 60.0/($total_units+1)]
  for {set i 1} {$i <= $total_units} {incr i} {
    $c create line 170 [expr 220+$step_small*$i] 210 [expr $step_large*$i]
    $c create line 560 [expr $step_large*$i] 600 [expr 220+$step_small*$i] \
      -arrow last
  }

  set intervals 0
  if {$add_ful_pipe} {
    set intervals [expr $fp_add_latency*3-1]
  }
  if {$mul_ful_pipe} {
    if {$intervals <= [expr $fp_mul_latency*3-1]} {
      set intervals [expr $fp_mul_latency*3-1]
    }
  }
  if {$div_ful_pipe} {
    if {$intervals <= [expr $fp_div_latency*3-1]} {
      set intervals [expr $fp_div_latency*3-1]
    }
  }
  if {$intervals} {
    set width [expr 330.0/$intervals]
  } else {
    set width 165
  }
  set height [expr $step_large/3]

  $c create rectangle [expr 385-$width] [expr $step_large-$height] \
    [expr 385+$width] [expr $step_large+$height] \
    -tags {RectEXInt rect}
  $c create line 210 $step_large [expr 385-$width] $step_large \
    -arrow last
  $c create line [expr 385+$width] $step_large 560 $step_large
  $c create text 385 [expr $step_large-$height-10]  \
    -font bigfont -text "Integer unit"
  $c create text 385 $step_large  -font bigfont -text "EX"
  set i 2
  for {set j 0} {$j < $num_mul_units} {incr j} {
    set y [expr $step_large*$i]
    $c create text 385 [expr $y-$height-10]  \
      -font bigfont -text "FP/integer multiply"
    if {$mul_ful_pipe} {
      set temp_length [expr (350-($fp_mul_latency*3-1)*$width)/2]
      $c create line 210 $y [expr 210+$temp_length] $y \
        -arrow last
      $c create line [expr 560-$temp_length] $y 560 $y
      for {set k 1} {$k <= $fp_mul_latency} {incr k} {
        set x [expr 210+$temp_length+(3*$k-2)*$width]
        $c create rectangle [expr $x-$width] [expr $y-$height] \
          [expr $x+$width] [expr $y+$height] \
          -tags "RectEXMul$j.$k rect"
        $c create text $x $y -font bigfont -text "M$k"
        if {$k != $fp_mul_latency} {
          $c create line [expr $x+$width] $y [expr $x+$width*2] $y \
            -arrow last
        }
      }
    } else {
      set distance [expr 330.0/($fp_mul_latency*2)]
      $c create line 210 $y 220 $y \
        -arrow last
      $c create line 550 $y 560 $y
      for {set k 1} {$k <= $fp_mul_latency} {incr k} {
        set x [expr 220+(2*$k-1)*$distance]
        $c create rectangle [expr $x-$distance] [expr $y-$height] \
          [expr $x+$distance+1] [expr $y+$height] \
          -tags "RectEXMul$j.$k rect"
      }
      $c create text 385 $y -font bigfont -text "MUL"
    }
    incr i
  }

  for {set j 0} {$j < $num_add_units} {incr j} {
    set y [expr $step_large*$i]
    $c create text 385 [expr $y-$height-10]  \
      -font bigfont -text "FP adder"
    if {$add_ful_pipe} {
      set temp_length [expr (350-($fp_add_latency*3-1)*$width)/2]
      $c create line 210 $y [expr 210+$temp_length] $y \
        -arrow last
      $c create line [expr 560-$temp_length] $y 560 $y
      for {set k 1} {$k <= $fp_add_latency} {incr k} {
        set x [expr 210+$temp_length+(3*$k-2)*$width]
        $c create rectangle [expr $x-$width] [expr $y-$height] \
          [expr $x+$width] [expr $y+$height] \
          -tags "RectEXAdd$j.$k rect"
        $c create text $x $y -font bigfont -text "A$k"
        if {$k != $fp_add_latency} {
          $c create line [expr $x+$width] $y [expr $x+$width*2] $y \
            -arrow last
        }
      }
    } else {
      set distance [expr 330.0/($fp_add_latency*2)]
      $c create line 210 $y 220 $y \
        -arrow last
      $c create line 550 $y 560 $y
      for {set k 1} {$k <= $fp_add_latency} {incr k} {
        set x [expr 220+(2*$k-1)*$distance]
        $c create rectangle [expr $x-$distance] [expr $y-$height] \
          [expr $x+$distance+1] [expr $y+$height] \
          -tags "RectEXAdd$j.$k rect"
      }
      $c create text 385 $y -font bigfont -text "ADD"
    }
    incr i
  }

  for {set j 0} {$j < $num_div_units} {incr j} {
    set y [expr $step_large*$i]
    $c create text 385 [expr $y-$height-10]  \
      -font bigfont -text "FP/integer divider"
    if {$div_ful_pipe} {
      set temp_length [expr (350-($fp_div_latency*3-1)*$width)/2]
      $c create line 210 $y [expr 210+$temp_length] $y \
        -arrow last
      $c create line [expr 560-$temp_length] $y 560 $y
      for {set k 1} {$k <= $fp_div_latency} {incr k} {
        set x [expr 210+$temp_length+(3*$k-2)*$width]
        $c create rectangle [expr $x-$width] [expr $y-$height] \
          [expr $x+$width] [expr $y+$height] \
          -tags "RectEXDiv$j.$k rect"
        $c create text $x $y -font bigfont -text "D$k"
        if {$k != $fp_div_latency} {
          $c create line [expr $x+$width] $y [expr $x+$width*2] $y \
            -arrow last
        }
      }
    } else {
      set distance [expr 330.0/($fp_div_latency*2)]
      $c create line 210 $y 220 $y \
        -arrow last
      $c create line 550 $y 560 $y
      for {set k 1} {$k <= $fp_div_latency} {incr k} {
        set x [expr 220+(2*$k-1)*$distance]
        $c create rectangle [expr $x-$distance] [expr $y-$height] \
          [expr $x+$distance+1] [expr $y+$height] \
          -tags "RectEXDiv$j.$k rect"
      }
      $c create text 385 $y -font bigfont -text "DIV"
    }
    incr i
  }

  button $c.button \
    -text "view integer datapath" \
    -width 20 \
    -command "pack forget .bas.block
              pack .bas.datapath"
  $c create window 80 480 \
    -window $c.button
  $c create text 665 425 \
    -font desfont \
    -text "IF Cycle of Current Instruction" \
    -tag cycleTitle
  label $c.counter \
    -font labfont
  $c create window 665 445 \
    -window $c.counter


  set c $w.datapath
  $c create text 375 450 -font hugfont -text "INTEGER datapath of pipeline"
  $c create rectangle 30 225 50 275
  $c create text 40 250 -font desfont -text "PC"
  $c create line 50 250 80 250 -arrow last \
    -tags {StageIF line}
  $c create line 57 250 57 208 70 208 -arrow last \
    -tags {StageIF line}
  create_dot $c 57 250
  $c create rectangle 80 240 130 290
  $c create text 105 260 -font desfont -text "Instr"
  $c create text 105 270 -font desfont -text "memory"
  $c create line 130 265 165 265 -arrow last \
    -tags {StageIF line}

  $c create line 70 220 100 202 100 173 70 155 70 180 80 187 70 195 70 220
  $c create text 90 187 -font desfont -text "ADD"
  $c create line 130 265 165 265 -arrow last \
    -tags {StageIF line}
  $c create text 145 255 -font desfont -text "IR"
  $c create line 100 195 125 195 -arrow last \
    -tags {StageIF line}
  $c create line 57 167 70 167 -arrow last \
    -tags {StageIF line}
  $c create text 53 167 -font desfont -text "4"
  $c create rectangle 125 160 145 210
  $c create text 135 175 -font desfont -text "M"
  $c create text 135 185 -font desfont -text "U"
  $c create text 135 195 -font desfont -text "X"
  $c create line 145 185 165 185 -arrow last \
    -tags {StageIF line}
  $c create line 150 185 150 135 15 135 15 250 30 250 -arrow last \
    -tags {StageIF line}
  create_dot $c 150 185
  $c create text 90 20 -font insfont -text "" \
    -tags {InstrIF text}
  $c create rectangle 165 145 185 380 -fill gray75
  $c create text 175 135 -font desfont -text "IF/ID"
  $c create line 260 270 290 250 -fill lavender -width 3 \
    -tags {RegAluDest choise}
  $c create line 260 290 290 250 -fill lavender -width 3 \
    -tags {RegMemDest choise}
  $c create rectangle 260 190 320 310
  $c create text 290 250 -font desfont -text "Registers"
  $c create text 290 210 -font insfont -text "" \
    -tags {RegA text}
  $c create text 290 230 -font insfont -text "" \
    -tags {RegB text}
  $c create text 290 280 -font insfont -text "" \
    -tags {RegDest text}
  $c create line 320 235 380 235 -arrow last \
    -tags {StageID line}
  $c create line 320 260 380 260 -arrow last \
    -tags {StageID line}
  $c create line 335 235 335 179 345 179 -arrow last \
    -tags {StageID line}
  create_dot $c 335 235
  $c create oval 240 313 280 363
  $c create text 260 330 -font desfont -text "Sign"
  $c create text 260 340 -font desfont -text "extend"
  $c create line 280 338 380 338 -arrow last  \
    -tags {StageID line}
  $c create line 292 333 298 343
  $c create text 298 330 -font desfont -text "32"
  $c create line 327 338 327 180 260 180 260 102 280 102 -arrow last \
    -tags {StageID line}
  create_dot $c 327 338
  $c create line 345 155 375 167 -fill lavender -width 3 \
    -tags {BranchRes1 BranchRes2 choise}
  $c create line 345 163 375 167 -fill lavender -width 3 \
    -tags {BranchRes3 choise}
  $c create line 345 171 375 167 -fill lavender -width 3 \
    -tags {BranchMem1 choise}
  $c create line 345 179 375 167 -fill lavender -width 3 \
    -tags {BranchReg choise}
  $c create rectangle 345 180 375 155
  $c create text 360 167 -font desfont -text "Zero?"
  $c create line 375 167 380 167 \
    -tags {StageIDBranch line}
  $c create line 400 167 420 167 420 50 135 50 135 160 \
    -tags {StageIDBranch line}
  $c create line 280 155 310 137 310 108 280 90 280 115 290 122 280 130 280 155
  $c create text 300 122 -font desfont -text "ADD"
  $c create line 310 123 350 123 350 148 380 148 \
    -tags {StageIDBranch line}
  $c create line 400 148 412 148 412 58 110 58 110 175 125 175 -arrow last \
    -tags {StageIDBranch line}
  $c create line 185 265 196 265 196 365 380 365 -arrow last \
    -tags {StageID line}
  $c create line 196 265 196 210 260 210 -arrow last \
    -tags {StageID line}
  create_dot $c 196 265
  $c create text 216 202 -font desfont -text "IR"
  $c create text 240 205 -font desfont -text "6..10"
  $c create line 196 230 260 230 -arrow last \
    -tags {StageID line}
  create_dot $c 196 230
  $c create text 216 222 -font desfont -text "IR"
  $c create text 240 225 -font desfont -text "11..15"
  $c create line 196 338 240 338 -arrow last  \
    -tags {StageID line}
  create_dot $c 196 338
  $c create line 222 333 232 343
  $c create text 230 330 -font desfont -text "16"
  $c create line 185 185 240 185 240 140 280 140 -arrow last \
    -tags {StageID line}
  $c create text 280 20 -font insfont -text "" \
    -tags {InstrID text}
  $c create rectangle 380 145 400 380 -fill gray75
  $c create text 390 135 -font desfont -text "ID/EX"
  $c create line 490 300 530 270 530 230 490 200 490 240 500 250 490 260 490 300
  $c create text 510 250 -font desfont -text "ALU"
  $c create line 530 250 550 250 -arrow last \
    -tags {StageEX line}
  $c create line 450 205 470 220 -fill lavender -width 3 \
    -tags {AluARes1 choise}
  $c create line 450 215 470 220 -fill lavender -width 3 \
    -tags {AluARes2 choise}
  $c create line 450 225 470 220 -fill lavender -width 3 \
    -tags {AluAMem1 choise}
  $c create line 450 235 470 220 -fill lavender -width 3 \
    -tags {AluAReg choise}
  $c create rectangle 450 195 470 245
  $c create text 460 210 -font desfont -text "M"
  $c create text 460 220 -font desfont -text "U"
  $c create text 460 230 -font desfont -text "X"
  $c create line 470 220 490 220 -arrow last \
    -tags {StageEX line}
  $c create line 450 260 470 280 -fill lavender -width 3 \
    -tags {AluBReg choise}
  $c create line 450 270 470 280 -fill lavender -width 3 \
    -tags {AluBImme choise}
  $c create line 450 280 470 280 -fill lavender -width 3 \
    -tags {AluBReg1 choise}
  $c create line 450 290 470 280 -fill lavender -width 3 \
    -tags {AluBReg2 choise}
  $c create line 450 300 470 280 -fill lavender -width 3 \
    -tags {AluBMem1 choise}
  $c create rectangle 450 255 470 305
  $c create text 460 270 -font desfont -text "M"
  $c create text 460 280 -font desfont -text "U"
  $c create text 460 290 -font desfont -text "X"
  $c create line 470 280 490 280 -arrow last  \
    -tags {StageEX line}
  $c create line 400 235 450 235 -arrow last \
    -tags {StageEX line}
  $c create line 400 260 450 260 -arrow last \
    -tags {StageEX line}
  $c create line 408 260 408 310 550 310 -arrow last \
    -tags {StageEX line}
  create_dot $c 408 260
  $c create line 400 338 416 338 416 270 450 270 -arrow last \
    -tags {StageEX line}
  $c create line 400 365 550 365 -arrow last \
    -tags {StageEX line}
  $c create text 470 20 -font insfont -text "" \
    -tags {InstrEX text}
  $c create rectangle 550 145 570 380 -fill gray75
  $c create text 560 135 -font desfont -text "EX/MEM"
  $c create line 620 220 650 280 -fill lavender -width 3 \
    -tags {MemRes2 choise}
  $c create line 620 250 650 280 -fill lavender -width 3 \
    -tags {MemRes1 choise}
  $c create line 620 280 650 280 -fill lavender -width 3 \
    -tags {MemMem0 choise}
  $c create line 620 333 650 280 -fill lavender -width 3 \
    -tags {MemMem1 choise}
  $c create line 620 310 650 280 -fill lavender -width 3 \
    -tags {MemReg choise}
  $c create rectangle 620 215 680 335
  $c create text 650 275 -font desfont -text "Data"
  $c create text 650 285 -font desfont -text "memory"
  $c create line 680 250 720 250 -arrow last \
    -tags {StageMEM line}
  $c create line 570 250 590 250 \
    -tags {StageMEM line}
  $c create line 590 250 590 350 720 350 -arrow last \
    -tags {StageMEM line}
  create_dot $c 590 250
  $c create line 590 350 590 392 424 392 424 280 450 280 -arrow last \
    -tags {StageMEM line}
  create_dot $c 590 350
  $c create line 590 250 590 100 530 100 530 190 424 190 424 205 450 205 -arrow last \
    -tags {StageMEM  line}
  $c create line 530 100 335 100 335 155 345 155 -arrow last \
    -tags {StageMEM line}
  create_dot $c 530 100
  $c create line 740 365 750 365 750 400 204 400 204 250 260 250 -arrow last \
    -tags {StageWB line}
  $c create text 231 242 -font desfont -text "MEM/WB.IR"
  $c create line 740 350 758 350 758 408 212 408 212 270 260 270 -arrow last \
    -tags {StageWB line}
  $c create line 432 408 432 290 450 290 -arrow last \
    -tags {StageWB line}
  create_dot $c 432 408
  $c create line 758 350 758 92 522 92 522 182 416 182 416 215 450 215 -arrow last \
    -tags {StageWB line}
  create_dot $c 758 350
  $c create line 522 92 327 92 327 163 345 163 -arrow last \
    -tags {StageWB line}
  create_dot $c 522 92
  $c create line 598 408 598 250 620 250 -arrow last \
    -tags {StageWB line}
  create_dot $c 598 408
  $c create line 740 250 766 250 766 416 220 416 220 290 260 290 -arrow last \
    -tags {StageWB line}
  $c create line 440 416 440 300 450 300 -arrow last \
    -tags {StageWB line}
  create_dot $c 440 416
  $c create line 766 250 766 84 514 84 514 174 408 174 408 225 450 225 -arrow last \
    -tags {StageWB line}
  create_dot $c 766 250
  $c create line 514 84 319 84 319 171 345 171 -arrow last \
    -tags {StageWB line}
  create_dot $c 514 84
  $c create line 606 416 606 280 620 280 -arrow last \
    -tags {StageWB line}
  create_dot $c 606 416
  $c create line 570 310 620 310 -arrow last \
    -tags {StageMEM line}
  $c create line 570 365 720 365 -arrow last \
    -tags {StageMEM line}
  $c create text 640 20 -font insfont -text "" \
    -tags {InstrMEM text}
  $c create rectangle 720 145 740 380 -fill gray75
  $c create text 730 135 -font desfont -text "MEM/WB"
  $c create text 780 20 -font insfont -text "" \
    -tags {InstrWB text}
  button $c.button \
    -text "view block diagram" \
    -width 20 \
    -command "pack forget .bas.datapath
              pack .bas.block"
  $c create window 80 480 \
    -window $c.button
  $c create text 665 425 \
    -font desfont \
    -text "IF Cycle of Current Instruction" \
    -tag cycleTitle
  label $c.counter \
    -font labfont
  $c create window 665 475 \
    -window $c.counter
  trace variable fastmode w BasClean
}