# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc mkScoreboard {{w .sco}} {
  catch {destroy $w}
  toplevel $w
  wm title $w "Scoreboard Algorithm on DLX"
  wm iconname $w "scoreboard"
  wm geometry $w +0+0
  set c $w.c
  option add Tk$w*Text.Background gainsboro
  canvas $c \
    -width 800 \
    -height 670 \
    -relief raised
  pack $c
  global num_int_units num_add_units num_mul_units
  global num_div_units num_load_bufs num_store_bufs
  global fp_div_exist ld_st_exist
  for {set i 0} {$i < 64} {incr i} {
    text $c.reg$i \
      -relief raised \
      -borderwidth 1 \
      -setgrid true \
      -height 1 \
      -width 7 \
      -font Courier?10?normal
    DisableTextWidget $c.reg$i
    $c create window [expr 50+$i%2*42] [expr 50+$i/2*13] \
      -anchor nw \
      -window $c.reg$i
    if {$i%2 == 0} {
      if {$i < 32} {
        set reg R$i
      } else {
        set reg F[expr $i-32]
      }
      $c create text 25 [expr 57+$i/2*13] \
        -anchor w \
        -font Courier?12?normal \
        -text $reg
    }
  }
  $c create text 100 40 \
    -font Courier?12?bold \
    -text "Registers"
  set top [expr 102-$num_int_units*5]
  for {set i 0} {$i < $num_int_units} {incr i} {
    $c create rectangle 250 [expr $top+$i*10]  290 [expr $top+$i*10+9]
  }
  $c create text 330 112 \
    -font Courier?12?bold \
    -text "Integer Unit"
  $c create line 139 98 250 98 \
    -arrow last
  $c create line 139 106 250 106 \
    -arrow last
  $c create line 290 102 325 102 325 56 139 56 \
    -arrow last
  set bot [expr 102+$num_int_units*5-1]

  set top [expr 206-$num_add_units*5]
  $c create line 270 $top 270 $bot \
    -arrow last
  for {set i 0} {$i < $num_add_units} {incr i} {
    $c create rectangle 250 [expr $top+$i*10]  290 [expr $top+$i*10+9]
  }
  $c create text 330 216 \
    -font Courier?12?bold \
    -text "FP add"
  $c create text 200 150 \
    -font Courier?12?bold \
    -text "Data buses"
  $c create line 139 202 250 202 \
    -arrow last
  $c create line 139 210 250 210 \
    -arrow last
  $c create line 290 206 325 206 325 160 139 160 \
    -arrow last
  set bot [expr 206+$num_add_units*5-1]
  set top [expr 310-$num_mul_units*5]
  $c create line 270 $top 270 $bot \
    -arrow last
  for {set i 0} {$i < $num_mul_units} {incr i} {
    $c create rectangle 250 [expr $top+$i*10]  290 [expr $top+$i*10+9]
  }
  $c create text 330 320 \
    -font Courier?12?bold \
    -text "FP mul"
  $c create line 139 306 250 306 \
    -arrow last
  $c create line 139 314 250 314 \
    -arrow last
  $c create line 290 310 325 310 325 264 139 264 \
    -arrow last
  set bot [expr 310+$num_mul_units*5-1]
  if {$fp_div_exist} {
    set top [expr 414-$num_div_units*5]
    $c create line 270 $top 270 $bot \
      -arrow last
    for {set i 0} {$i < $num_div_units} {incr i} {
      $c create rectangle 250 [expr $top+$i*10]  290 [expr $top+$i*10+9]
    }
    $c create text 330 424 \
      -font Courier?12?bold \
      -text "FP divide"
    $c create line 139 410 250 410 \
      -arrow last
    $c create line 139 418 250 418 \
      -arrow last
    $c create line 290 414 325 414 325 368 139 368 \
      -arrow last
    set bot [expr 414+$num_div_units*5-1]
  }
  $c create line 270 $bot 270 480 600 480  \
    -arrow first
  $c create text 320 470 \
    -font Courier?12?bold \
    -text "Control/status"
  set top [expr 258-($num_int_units+$num_add_units+$num_mul_units+$num_div_units+1)*6.5]
  $c create line 600 $top 600 30 60 30 60 50 \
    -arrow both
  $c create text 315 20 \
    -font Courier?12?bold \
    -text "Control/status"
  text $c.title \
    -relief raised \
    -borderwidth 1 \
    -setgrid true \
    -height 1 \
    -width 53 \
    -font Courier?10?normal
  DisableTextWidget $c.title
  $c create window 440 $top \
    -anchor nw \
    -window $c.title
  $c.title insert end "Busy Op       Fi   Fj   Fk     Qj      Qk    Rj  Rk"
  set top [expr $top+13]
  for {set i 0} {$i < $num_int_units} {incr i} {
    text $c.int$i \
      -relief raised \
      -borderwidth 1 \
      -setgrid true \
      -height 1 \
      -width 53 \
      -font Courier?10?normal
    DisableTextWidget $c.int$i
    $c create window 440 [expr $top+$i*13] \
      -anchor nw \
      -window $c.int$i
    bind $c.int$i <Button-1> "mkEquation Int $i"
    $c create text 380 [expr $top+7+$i*13 ] \
      -font Courier?12?normal \
      -anchor w \
      -text Integer[expr $i+1]
  }
  set top [expr $top+$num_int_units*13]
  for {set i 0} {$i < $num_add_units} {incr i} {
    text $c.add$i \
      -relief raised \
      -borderwidth 1 \
      -setgrid true \
      -height 1 \
      -width 53 \
      -font Courier?10?normal
    DisableTextWidget $c.add$i
    $c create window 440 [expr $top+$i*13] \
      -anchor nw \
      -window $c.add$i
    bind $c.add$i <Button-1> "mkEquation Add [expr $i+8]"
    $c create text 380 [expr $top+7+$i*13 ] \
      -font Courier?12?normal \
      -anchor w \
      -text Add[expr $i+1]
  }
  set top [expr $top+$num_add_units*13]
  for {set i 0} {$i < $num_mul_units} {incr i} {
    text $c.mul$i \
      -relief raised \
      -borderwidth 1 \
      -setgrid true \
      -height 1 \
      -width 53 \
      -font Courier?10?normal
    DisableTextWidget $c.mul$i
    $c create window 440 [expr $top+$i*13] \
      -anchor nw \
      -window $c.mul$i
    bind $c.mul$i <Button-1> "mkEquation Mul [expr $i+24]"
    $c create text 380 [expr $top+7+$i*13 ] \
      -font Courier?12?normal \
      -anchor w \
      -text Mult[expr $i+1]
  }
  set top [expr $top+$num_mul_units*13]
  if {$fp_div_exist} {
    for {set i 0} {$i < $num_div_units} {incr i} {
      text $c.div$i \
        -relief raised \
        -borderwidth 1 \
        -setgrid true \
        -height 1 \
        -width 53 \
        -font Courier?10?normal
      DisableTextWidget $c.div$i
      $c create window 440 [expr $top+$i*13] \
        -anchor nw \
        -window $c.div$i
      bind $c.div$i <Button-1> "mkEquation Div [expr $i+16]"
      $c create text 380 [expr $top+7+$i*13 ] \
        -font Courier?12?normal \
        -anchor w \
        -text Divide[expr $i+1]
    }
    set top [expr $top+$num_div_units*13]
  }
  $c create line 600 $top 600 480 \
    -arrow first
  frame $c.frame
  text $c.frame.table \
    -relief raised \
    -borderwidth 1 \
    -height 9 \
    -width 70 \
    -background lavender \
    -yscrollcommand "$c.frame.s set" \
    -font Courier?12?normal
  DisableTextWidget $c.frame.table
  label $c.frame.l1 \
    -relief raised \
    -borderwidth 1 \
    -font Courier?12?bold \
    -text "                           Instruction status                        "
  label $c.frame.l2 \
    -relief raised \
    -borderwidth 1 \
    -font Courier?12?bold \
    -text "  Instruction           Issue  Read operands  Excute  Write result   "
  scrollbar $c.frame.s \
    -relief raised \
    -borderwidth 1 \
    -command "$c.frame.table yview"
  pack $c.frame.s \
    -side right \
    -fill y
  pack $c.frame.l1 $c.frame.l2 \
    -side top \
    -expand yes \
    -fill x
  pack $c.frame.table \
    -side bottom
  $c create window 25 500 \
    -anchor nw \
    -window $c.frame
  label $c.counter \
    -font Times?40?bold
  $c create window 670 560 \
    -window $c.counter
  $c create text 670 600 \
    -font Courier?10?normal \
    -text "Issue Cycle of Current Instruction" \
    -tag cycleTitle
}
