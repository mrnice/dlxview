# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc mkEquation {res unit {w .equation}} {
  global config
  set name $res[expr $unit%8+1]
  switch $config {
    1 {
        set title "reservation station"
        set widget .tom.c.[string tolower $res][expr $unit%8]
        set cycle [lindex [.tom.c.counter configure -text] 4]
      }
    2 {
        set title "functional unit"
        set widget .sco.c.[string tolower $res][expr $unit%8]
        set cycle [lindex [.sco.c.counter configure -text] 4]
      }
  }
  catch {destroy $w}
  toplevel $w
  wm title $w "Action or bookeeping in $title $name"
  wm iconname $w "equation"
  wm geometry $w +0+0
  text $w.t \
    -relief raised \
    -setgrid true \
    -width 75 \
    -height 11
  DisableTextWidget $w.t
  button $w.ok \
    -text OK \
    -command "destroy $w"
  pack $w.t \
    -side top
  pack $w.ok \
    -side top \
    -fill x
  $w.t tag configure bold -font Courier?12?bold
  $w.t tag configure normal -font Courier?12?bold
  $w.t tag configure red -foreground red
  $w.t tag configure blue -foreground blue
  $w.t tag configure underline -underline on
  if {[$widget get 1.0 end] == ""} {
    InsertWithTags $w.t "No action for $title " normal
    InsertWithTags $w.t "$name " red
    InsertWithTags $w.t "at cycle " normal
    InsertWithTags $w.t "$cycle" red
    InsertWithTags $w.t "." normal
  } elseif {$config == "1"} {
    if {[lindex [$widget tag configure toggle -background] 4] == "black"} {
      set regs [equation $unit $cycle issue]
      set rs1 [lindex $regs 0]
      if {$rs1 != ""} {
        InsertWithTags $w.t "if (Register\[" normal
        InsertWithTags $w.t "$rs1" red
        InsertWithTags $w.t "\].Qi != 0) \n   " normal
        set Qj [lindex $regs 1]
        if {$Qj} {
          InsertWithTags $w.t "\{RS\[" normal underline
          InsertWithTags $w.t "$name" red underline
          InsertWithTags $w.t "\].Qj<-Register\[" normal underline
          InsertWithTags $w.t "$rs1" red underline
          InsertWithTags $w.t "\].Qi\}" normal underline
          if {$res != "Load"} {
            InsertWithTags $w.t "         (RS\[$name\].Qj = " blue bold
            if {$res != "Store"} {
              InsertWithTags $w.t "[lindex [$widget get 1.0 end] 1])" blue bold
            } else {
              InsertWithTags $w.t "[lindex [$widget get 1.0 end] 0])" blue bold
            }
          }
          InsertWithTags $w.t ""
          InsertWithTags $w.t "else " normal
          InsertWithTags $w.t "\{RS\[" normal
          InsertWithTags $w.t "$name" red
          InsertWithTags $w.t "\].Vj<-" normal
          InsertWithTags $w.t "$rs1" red
          InsertWithTags $w.t "; RS\[" normal
          InsertWithTags $w.t "$name" red
          InsertWithTags $w.t "\].Qj<-0\}" normal
        } else {
          InsertWithTags $w.t "\{RS\[" normal 
          InsertWithTags $w.t "$name" red 
          InsertWithTags $w.t "\].Qj<-Register\[" normal
          InsertWithTags $w.t "$rs1" red underline
          InsertWithTags $w.t "\].Qi\}\nelse" normal 
          InsertWithTags $w.t "\{RS\[" normal underline
          InsertWithTags $w.t "$name" red underline
          InsertWithTags $w.t "\].Vj<-" normal underline
          InsertWithTags $w.t "$rs1" red underline
          InsertWithTags $w.t "; RS\[" normal underline
          InsertWithTags $w.t "$name" red underline
          InsertWithTags $w.t "\].Qj<-0\}" normal underline
        }
      }
      set rs2 [lindex $regs 2]
      if {$rs2 != ""} {
        InsertWithTags $w.t "if (Register\[" normal
        InsertWithTags $w.t "$rs2" red
        InsertWithTags $w.t "\].Qi != 0) \n   " normal
        set Qk [lindex $regs 3]
        if {$Qk} {
          InsertWithTags $w.t "\{RS\[" normal underline
          InsertWithTags $w.t "$name" red underline
          InsertWithTags $w.t "\].Qk<-Register\[" normal underline
          InsertWithTags $w.t "$rs2" red underline
          InsertWithTags $w.t "\].Qi\}" normal underline
          if {$res != "Load"} {
            InsertWithTags $w.t "         (RS\[$name\].Qk = " blue bold
            if {$res != "Store"} {
              InsertWithTags $w.t "[lindex [$widget get 1.0 end] 1])" blue bold
            } else {
              InsertWithTags $w.t "[lindex [$widget get 1.0 end] 0])" blue bold
            }
          }
          InsertWithTags $w.t ""
          InsertWithTags $w.t "else " normal
          InsertWithTags $w.t "\{RS\[" normal
          InsertWithTags $w.t "$name" red
          InsertWithTags $w.t "\].Vj<-" normal
          InsertWithTags $w.t "$rs2" red
          InsertWithTags $w.t "; RS\[" normal
          InsertWithTags $w.t "$name" red
          InsertWithTags $w.t "\].Qk<-0\}" normal
        } else {
          InsertWithTags $w.t "\{RS\[" normal
          InsertWithTags $w.t "$name" red
          InsertWithTags $w.t "\].Qk<-Register\[" normal
          InsertWithTags $w.t "$rs2" red underline
          InsertWithTags $w.t "\].Qi\}\nelse" normal
          InsertWithTags $w.t "\{RS\[" normal underline
          InsertWithTags $w.t "$name" red underline
          InsertWithTags $w.t "\].Vj<-" normal underline
          InsertWithTags $w.t "$rs2" red underline
          InsertWithTags $w.t "; RS\[" normal underline
          InsertWithTags $w.t "$name" red underline
          InsertWithTags $w.t "\].Qk<-0\}" normal underline
        }
      }
      set rd [lindex $regs 4]
      if {$rd != ""} {
        InsertWithTags $w.t "RS\[" normal underline
        InsertWithTags $w.t "$name" red underline
        InsertWithTags $w.t "\].Busy<-yes;\nRegister\[" normal underline
        InsertWithTags $w.t "$rd" red underline
        InsertWithTags $w.t "\].Qi=" normal underline
        InsertWithTags $w.t "$name" red underline
        InsertWithTags $w.t ";" normal underline
      }
    } elseif {[lindex [$widget tag configure gray -background] 4] == "gray"} {
      set info [equation $unit $cycle write]
      set r [lindex $info 0]
      if {$r != ""} {
        InsertWithTags $w.t "for any x (if (Register\[x\].Qi=" normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t ")     " normal
        InsertWithTags $w.t "\{x<-result;" normal underline
        InsertWithTags $w.t "Register\[x].Qi<-0\}" normal underline
        InsertWithTags $w.t ");\}" normal
        InsertWithTags $w.t "x = $r" blue bold
      }
      set i 1
      if {[lindex $info $i] == "1"} {
        InsertWithTags $w.t "for any x (if (RS\[x\].Qj=" normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t ")     " normal
        InsertWithTags $w.t "\{RS\[x\].Vj<-result;" normal underline
        InsertWithTags $w.t "RS\[x].Qj<-0\}" normal underline
        InsertWithTags $w.t ");\}" normal
        InsertWithTags $w.t "x = \{" blue bold
        for {} {[lindex $info $i] == "1"} {incr i} {
          InsertWithTags $w.t "[lindex $info [incr i]] " blue bold
        }
        InsertWithTags $w.t "\}" blue bold
      }
      if {[lindex $info $i] == "2"} {
        InsertWithTags $w.t "for any x (if (RS\[x\].Qk=" normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t ")     " normal
        InsertWithTags $w.t "\{RS\[x\].Vk<-result;" normal underline
        InsertWithTags $w.t "RS\[x].Qk<-0\}" normal underline
        InsertWithTags $w.t ");\}" normal
        InsertWithTags $w.t "x = \{" blue bold
        for {} {[lindex $info $i] == "2"} {incr i} {
          InsertWithTags $w.t "[lindex $info [incr i]] " blue bold
        }
        InsertWithTags $w.t "\}" blue bold
      }

      InsertWithTags $w.t "RS\[" normal underline
      InsertWithTags $w.t "$name" red
      InsertWithTags $w.t "\].Busy<-No" normal
    } else {
      InsertWithTags $w.t "Instruction at reservation station " normal
      InsertWithTags $w.t "$name" red
      InsertWithTags $w.t " is now excuting." normal
    }
  }  elseif {$config == "2"} {
    if {[lindex [$widget tag configure toggle -background] 4] == "black"} {
      InsertWithTags $w.t "Busy(" normal
      InsertWithTags $w.t "$name" red
      InsertWithTags $w.t ")<-yes" normal
      set regs [equation $unit $cycle issue]
      set rs1 [lindex $regs 0]
      set Qj [lindex $regs 1]
      set rs2 [lindex $regs 2]
      set Qk [lindex $regs 3]
      set rd [lindex $regs 4]
      if {$rd != ""} {
        InsertWithTags $w.t ";  Result(" normal
        InsertWithTags $w.t "$rd" red
        InsertWithTags $w.t ")<-" normal
        InsertWithTags $w.t "$name" red
      }

      set op [lindex [$widget get 1.0 end] 1]
      InsertWithTags $w.t ";  Op(" normal
      InsertWithTags $w.t "$name" red
      InsertWithTags $w.t ")<-" normal
      InsertWithTags $w.t "$op" red
      InsertWithTags $w.t ";"
      if {$rd != ""} {
        InsertWithTags $w.t "Fi(" normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t ")<-" normal
        InsertWithTags $w.t "$rd" red
        InsertWithTags $w.t ";  " normal
      }
      if {$rs1 != ""} {
        InsertWithTags $w.t "Fj(" normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t ")<-" normal
        InsertWithTags $w.t "$rs1" red
        InsertWithTags $w.t ";  " normal
      }
      if {$rs2 != ""} {
        InsertWithTags $w.t "Fk(" normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t ")<-" normal
        InsertWithTags $w.t "$rs2" red
        InsertWithTags $w.t ";  " normal
      }
      if {$rs1 != ""} {
        InsertWithTags $w.t "\nQj<-Result(" normal
        InsertWithTags $w.t "$rs1" red
        InsertWithTags $w.t "); " normal
        InsertWithTags $w.t "[$widget get 1.30 1.35]     " blue
      }
      if {$rs2 != ""} {
        if {$rs1 == ""} {
          InsertWithTags $w.t "" normal
        }
        InsertWithTags $w.t "Qk<-Result(" normal
        InsertWithTags $w.t "$rs2" red
        InsertWithTags $w.t ");" normal
        InsertWithTags $w.t "[$widget get 1.38 1.43]" blue
      }
      if {$rs1 != ""} {
        InsertWithTags $w.t "\nRj<-not Qj; " normal
        InsertWithTags $w.t "[$widget get 1.45 1.47]         " blue
      }
      if {$rs2 != ""} {
        if {$rs1 == ""} {
          InsertWithTags $w.t "" normal
        }
        InsertWithTags $w.t "Rk<-not Qk;" normal
        InsertWithTags $w.t "[$widget get 1.49 1.52]" blue
      }
    } elseif {[lindex [$widget tag configure gray -background] 4] == "gray"} {
      set info [equation $unit $cycle write]

      set i 1
      if {[lindex $info $i] == "1"} {
        InsertWithTags $w.t "for any f (if (Qj(f)=" normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t " then Rj(f)<-Yes)" normal
        InsertWithTags $w.t "f = \{" blue bold
        for {} {[lindex $info $i] == "1"} {incr i} {
          InsertWithTags $w.t "[lindex $info [incr i]] " blue bold
        }
        InsertWithTags $w.t "\}" blue bold
      }

      if {[lindex $info $i] == "2"} {
        InsertWithTags $w.t "for any f (if (Qk(f)=" normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t " then Rk(f)<-Yes)" normal
        InsertWithTags $w.t "f = \{" blue bold
        for {} {[lindex $info $i] == "2"} {incr i} {
          InsertWithTags $w.t "[lindex $info [incr i]] " blue bold
        }
        InsertWithTags $w.t "\}" blue bold
      }
      set r [lindex $info 0]
      if {$r != ""} {
        InsertWithTags $w.t "Result(" normal
        InsertWithTags $w.t "$r" red
        InsertWithTags $w.t ")<-Clear;  " normal
      }
      InsertWithTags $w.t "Busy(" normal
      InsertWithTags $w.t "$name" red
      InsertWithTags $w.t ")<-No" normal
    } else {
      if {[$widget get 1.4 1.7] == "DIV"} {
        set info [equation $unit $cycle inused]
      } else {
        set info [equation $unit $cycle inuse]
      }
      if {$info == "read"} {
        InsertWithTags $w.t "Rj<-No;  Rk<-No" normal
      } elseif {$info == "exec"} {
        InsertWithTags $w.t "Instruction at functional unit " normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t " is now excuting." normal
      } elseif {$info == "RAW"} {
        InsertWithTags $w.t "Instruction at functional unit " normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t " is waiting for operand(s)." normal
      } elseif {$info == "WAR"} {
        InsertWithTags $w.t "Instruction at functional unit " normal
        InsertWithTags $w.t "$name" red
        InsertWithTags $w.t " is waiting because of WAR hazard." normal
      }
    }
  }
}
