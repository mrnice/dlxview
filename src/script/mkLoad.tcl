# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc mkLoad {{w .load}} {
  catch {destroy $w}
  toplevel $w
  wm title $w "Load DLX Program, Data, and Initialize Registers"
  wm iconname $w "load"
  wm geometry $w +0+0
  set oldFocus [focus]
  option add Tk$w*Relief raised
  option add Tk$w*Listbox.Setgrid yes
  option add Tk$w*Label.Relief flat
  option add Tk$w*Label.Width 16
  option add Tk$w*Label.Anchor w
  option add Tk$w*Entry.Width 40
  
  frame $w.list 
  frame $w.entry  
  label $w.label
  frame $w.button  
  pack $w.list \
    -side top \
    -fill x \
    -expand yes
    
  pack $w.entry \
    -side top \
    -pady 5 \
    -fill x \
    -expand yes
  pack $w.label $w.button \
    -side top \
    -fill x \
    -expand yes
  listbox $w.list.dirlist \
    -yscrollcommand "$w.list.dirscroll set"       
  bind $w.list.dirlist <Button-1> {
    %W selection clear 0 end
    .load.button.load configure -state normal
    set num [%W nearest %y]
    %W selection set $num
    LoadOpen [%W get $num]
  }
  bind $w.list.dirlist <Button1-Motion> {%W selection clear 0 end
    set num [%W nearest %y]
    %W selection set $num
    set fHeaderExt [%W get $num]
    .load.button.load configure -state normal
    set length [string length $fHeaderExt]
    set fHeader [string range $fHeaderExt 0 [expr $length-3]]
    if {[string match *.d $fHeaderExt]} {
      if {[string compare $fHeader.i [%W get [expr $num+1]]] == "0"} {
        %W selection set [expr $num+1]
        if {[string compare $fHeader.s [%W get [expr $num+2]]] == "0"} {
          %W selection set [expr $num+2]
        }
      } elseif {[string compare $fHeader.s [%W get [expr $num+1]]] == "0"} {
        %W selection set [expr $num+1]
      }     
    }
    if {[string match *.i $fHeaderExt]} {
      if {[string compare $fHeader.d [%W get [expr $num-1]]] == "0"} {
        %W selection set [expr $num-1]
      }    
      if {[string compare $fHeader.s [%W get [expr $num+1]]] == "0"} {
        %W selection set [expr $num+1]
      }       
    }
    if {[string match *.s $fHeaderExt]} {
      if {[string compare $fHeader.i [%W get [expr $num-1]]] == "0"} {
        %W selection set [expr $num-1]
        if {[string compare $fHeader.d [%W get [expr $num-2]]] == "0"} {
         %W selection set [expr $num-2]
        }           
      } elseif {[string compare $fHeader.d [%W get [expr $num-1]]] == "0"} {
        %W selection set [expr $num-1]
      }           
    }
    set fileList {}
    foreach fileNum [%W curselection] {
      set fileTail [%W get $fileNum]
      lappend fileList $fileTail
    }
    LoadOpen $fileList
  }
  scrollbar $w.list.dirscroll \
    -command "$w.list.dirlist yview"
  listbox $w.list.filelist \
    -yscrollcommand "$w.list.filescroll set"
  scrollbar $w.list.filescroll \
    -command "$w.list.filelist yview"   
 
  pack $w.list.dirlist $w.list.dirscroll \
    -side left \
    -fill y 
  pack $w.list.filelist \
    -side left \
    -fill both \
    -expand yes
  pack $w.list.filescroll \
    -side right \
    -fill y 
  frame $w.entry.up 
  frame $w.entry.down
  pack $w.entry.up $w.entry.down \
    -side top \
    -fill x \
    -expand yes
    
  label $w.entry.up.dirlabel \
    -text "directory name:" 
  entry $w.entry.up.direntry 
  bind $w.entry.up.direntry <Return> "focus $w.entry.down.fileentry
    LoadOpen \[$w.entry.up.direntry get \]"
  
  pack $w.entry.up.dirlabel \
    -side left
  pack $w.entry.up.direntry \
    -side left \
    -fill x \
    -expand yes
  label $w.entry.down.filelabel \
    -text "file name:"
  entry $w.entry.down.fileentry
  bind $w.entry.down.fileentry <Return> FileEntryReturn  
  pack $w.entry.down.filelabel \
    -side left
  pack $w.entry.down.fileentry \
    -side left \
    -fill x \
    -expand yes
  button $w.button.load \
    -text Load \
    -command LoadFile
  button $w.button.done \
    -text Done \
    -command LoadDone
  button $w.button.cancel \
    -text Cancel \
    -command "LoadCancel $oldFocus"
  pack $w.button.load $w.button.done $w.button.cancel\
    -side left \
    -fill x \
    -expand yes   
  global curDir env
  if {[info exists env(DISC_INPUT)] && [file isdirectory $env(DISC_INPUT)]} {
    set curDir $env(DISC_INPUT)
  } else {
    set curDir [pwd]
  }
  LoadRefresh
  focus $w.entry.down.fileentry
  grab $w
}
