# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc BasVisual {linelist linecolorlist textlist textcolorlist
    rectlist rectcolorlist} {
  global fastmode
  if {$fastmode} {
    return
  }
  set c .bas.datapath
  $c itemconfigure line -fill black -width 1
  $c itemconfigure choise -fill lavender
  $c itemconfigure text -text ""
  set i 0
  foreach line $linelist {
    $c itemconfigure $line -fill  [lindex $linecolorlist $i] -width 3
    incr i
  }
  set i 0
  foreach text $textlist {
    set color_content [lindex $textcolorlist $i]
    $c itemconfigure $text \
      -fill [lindex $color_content 0] \
      -text [lrange $color_content 1 end] 
    incr i
  }  
  set c .bas.block
  $c itemconfigure rect -fill lavender  
  set i 0
  foreach rect $rectlist {
    set target $rect
    if {$rect == "RectMEM" || $rect == "RectWB" } { 
      set target $rect.1
      set origColor [lindex [$c itemconfigure $target -fill] 4]
      if {$origColor == "lavender"} {
        $c itemconfigure $target -fill  [lindex $rectcolorlist $i] 
      }
      set target $rect.2
    } 
    $c itemconfigure $target -fill  [lindex $rectcolorlist $i] 
    incr i
  }
}
