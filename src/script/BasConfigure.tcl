# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc BasConfigure {{w .subconfigure}} {
  global num_int_units 
  global num_add_units fp_add_latency add_ful_pipe
  global num_mul_units fp_mul_latency mul_ful_pipe
  global num_div_units fp_div_latency div_ful_pipe
  global configureEnabled
  set num_add_unitsOrig $num_add_units
  set fp_add_latencyOrig $fp_add_latency 
  set num_mul_unitsOrig $num_mul_units
  set fp_mul_latencyOrig $fp_mul_latency
  set num_div_unitsOrig $num_div_units
  set fp_div_latencyOrig $fp_div_latency
  catch {destroy $w}
  toplevel $w
  wm title $w "Basic Pipeline Configuration"
  wm iconname $w "subconfigure"
  wm geometry $w +0+0
  frame $w.add
  frame $w.mul
  frame $w.div
  frame $w.button  
  set oldFocus [focus]
  if {$configureEnabled} {
    set scaletroco skyblue
    set scaleactbg MediumOrchid
  } else {
    set scaletroco lightgrey
    set scaleactbg lightgrey
  }
  pack $w.add $w.mul $w.div $w.button \
    -side top \
    -fill x \
    -expand yes  
  scale $w.add.number \
    -tickinterval 7 \
    -label "FP adder number" \
    -command "ScaleValue num_add_units" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg
  scale $w.add.latency \
    -to 5  \
    -tickinterval 4 \
    -label "FP adder latency (cycles)" \
    -command "ScaleValue fp_add_latency" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg 
  radiobutton $w.add.fulpipe \
    -relief flat \
    -text "fully piped" \
    -variable add_ful_pipe \
    -value 1 \
    -command "BasRadioCommand add inactive"
  radiobutton $w.add.notpipe \
    -relief flat \
    -text "not piped" \
    -variable add_ful_pipe \
    -value 0 \
    -command "BasRadioCommand add active"
  pack $w.add.number $w.add.latency $w.add.fulpipe $w.add.notpipe \
    -side left \
    -fill x \
    -expand yes \
    -padx 5 \
    -pady 10
  scale $w.mul.number \
    -tickinterval 7 \
    -label "FP multiplier number" \
    -command "ScaleValue num_mul_units" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg
  scale $w.mul.latency \
    -to 10  \
    -tickinterval 9 \
    -label "FP multiplier latency (cycles)" \
    -command "ScaleValue fp_mul_latency" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg
  radiobutton $w.mul.fulpipe \
    -relief flat \
    -text "fully piped" \
    -variable mul_ful_pipe \
    -value 1 \
    -command "BasRadioCommand mul inactive"
  radiobutton $w.mul.notpipe \
    -relief flat \
    -text "not piped" \
    -variable mul_ful_pipe \
    -value 0 \
    -command "BasRadioCommand mul active"
  pack $w.mul.number $w.mul.latency $w.mul.fulpipe $w.mul.notpipe\
    -side left \
    -fill x \
    -expand yes \
    -padx 5 \
    -pady 10
  scale $w.div.number \
    -from 0 \
    -tickinterval 8 \
    -label "FP divider number" \
    -command "ScaleValue num_div_units" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg
  scale $w.div.latency \
    -to 50  \
    -tickinterval 49 \
    -label "FP divider latency (cycles)" \
    -command "ScaleValue fp_div_latency" \
    -troughcolor $scaletroco \
    -activebackground $scaleactbg
  radiobutton $w.div.fulpipe \
    -relief flat \
    -text "fully piped" \
    -variable div_ful_pipe \
    -value 1 \
    -command "BasRadioCommand div inactive"
  radiobutton $w.div.notpipe \
    -relief flat \
    -text "not piped" \
    -variable div_ful_pipe \
    -value 0 \
    -command "BasRadioCommand div active"
  pack $w.div.number $w.div.latency $w.div.fulpipe $w.div.notpipe \
    -side left \
    -fill x \
    -expand yes \
    -padx 5 \
    -pady 10
  button $w.button.ok \
    -text Ok \
    -command "SubConfigureOk 
       destroy $w
       focus $oldFocus"
  button $w.button.cancel \
    -text Cancel \
    -command " set num_add_units $num_add_unitsOrig
      set fp_add_latency $fp_add_latencyOrig 
      set num_mul_units $num_mul_unitsOrig
      set fp_mul_latency $fp_mul_latencyOrig
      set num_div_units $num_div_unitsOrig
      set fp_div_latency $fp_div_latencyOrig
      destroy $w
      focus $oldFocus"
  pack $w.button.ok $w.button.cancel\
    -side left \
    -fill x \
    -expand yes
  $w.add.number set $num_add_units
  $w.add.latency set $fp_add_latency
  $w.mul.number set $num_mul_units
  $w.mul.latency set $fp_mul_latency
  $w.div.number set $num_div_units
  $w.div.latency set $fp_div_latency
  if {$configureEnabled == "0"} {
    $w.button.ok configure -state disabled
    $w.add.number configure -state disabled
    $w.add.latency configure -state disabled
    $w.add.fulpipe configure -state disabled
    $w.add.notpipe configure -state disabled
    $w.mul.number configure -state disabled
    $w.mul.latency configure -state disabled
    $w.mul.fulpipe configure -state disabled
    $w.mul.notpipe configure -state disabled
    $w.div.number configure -state disabled
    $w.div.latency configure -state disabled
    $w.div.fulpipe configure -state disabled
    $w.div.notpipe configure -state disabled
  }
  if {$add_ful_pipe} {
    $w.add.fulpipe invoke
  } else {
    $w.add.notpipe invoke
  }
  if {$mul_ful_pipe} {
    $w.mul.fulpipe invoke
  } else {
    $w.mul.notpipe invoke
  }
  if {$div_ful_pipe} {
    $w.div.fulpipe invoke
  } else {
    $w.div.notpipe invoke
  }
  tkwait visibility $w
  grab $w
  focus $w
}
