# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc ResetMode {mode} {
  global config highLightList 
  global fullNameList 
  global num_int_units num_add_units num_mul_units 
  global num_div_units num_load_bufs num_store_bufs
  global fp_div_exist ld_st_exist
  global configureEnabled 
  set highLightList {}
  
  if {$mode == "4"} {
    return
  }
  init
  .code.t delete 1.0 end
  .button.top.go configure -command "mkGoStep Go"
  .button.top.stepforward configure -command "mkGoStep Step"
  .button.top.nextcycle configure -command "mkGoStep Cycle"
  switch $mode {
    0 {
      switch $config {
        0 {
          .bas.up.code delete Stage
          .bas.up.cycle delete Stage
          .bas.up.cycle itemconfigure strip -fill lavender 
          .bas.datapath itemconfigure line -fill black -width 1
	  .bas.datapath itemconfigure choise -fill lavender
	  .bas.datapath itemconfigure text -text ""
          .bas.datapath.counter configure -text {}
          .bas.block itemconfigure rect -fill lavender
          .bas.block.counter configure -text {}
        }
        1 {
          set c .tom.c
          $c delete thickLine
          for {set i 0} {$i < 64} {incr i} {
            $c.reg$i delete 1.0 end
          }
          for {set i 0} {$i < $num_int_units} {incr i} {
            $c.int$i delete 1.0 end
          }
          for {set i 0} {$i < $num_add_units} {incr i} {
            $c.add$i delete 1.0 end
          }
          for {set i 0} {$i < $num_mul_units} {incr i} {
            $c.mul$i delete 1.0 end
          }  
          $c.instr delete 1.0 end  
          if {$num_div_units > "0"} {
            for {set i 0} {$i < $num_div_units} {incr i} {
              $c.div$i delete 1.0 end
            } 
            set fp_div_exist 1
          } else {
            set fp_div_exist 0
          }
          if {$num_load_bufs > "0"} {
            for {set i 0} {$i < $num_load_bufs} {incr i} {
              $c.load$i delete 1.0 end
            } 
            for {set i 0} {$i < $num_store_bufs} {incr i} {
              $c.store$i delete 1.0 end
            } 
            set ld_st_exist 1
          } else {
            set ld_st_exist 0
          }
          $c.frame.table delete 1.0 end    
          $c.counter configure -text {}    
        }
        2 {
          set c .sco.c
          $c delete thickLine
          for {set i 0} {$i < 64} {incr i} {
            $c.reg$i delete 1.0 end
          }
          for {set i 0} {$i < $num_int_units} {incr i} {
            $c.int$i delete 1.0 end
          }
          for {set i 0} {$i < $num_add_units} {incr i} {
            $c.add$i delete 1.0 end
          }
          for {set i 0} {$i < $num_mul_units} {incr i} {
            $c.mul$i delete 1.0 end
          }    
          if {$num_div_units > "0"} {
            for {set i 0} {$i < $num_div_units} {incr i} {
              $c.div$i delete 1.0 end
            } 
            set fp_div_exist 1
          } else {
            set fp_div_exist 0
          }
          $c.frame.table delete 1.0 end 
          $c.counter configure -text {}       
        }
      } 
    }
    1 {
      if {[winfo exists .bas]} {
        destroy .bas
      }
      if {[winfo exists .tom]} {
        destroy .tom
      }
      if {[winfo exists .sco]} {
        destroy .sco
      }
      set backFullNameList $fullNameList
      set fullNameList {}
      foreach file $backFullNameList {
        Load $file
      }
      set configureEnabled 1
      while {![winfo exists .bas] && ![winfo exists .tom] && ![winfo exists .sco]} {
     	.button.top.load configure -state disabled
        .button.top.stepforward configure -state normal
        .button.bot.stepbackward configure -state normal
        .button.top.nextcycle configure -state normal
        .button.bot.previouscycle configure -state normal
        .button.top.go configure -state normal
        mkConfigure
        tkwait window .configure
      } 
    }
    2 {
      if {[winfo exists .bas]} {
        destroy .bas
      }
      if {[winfo exists .tom]} {
        destroy .tom
      }
      if {[winfo exists .sco]} {
        destroy .sco
      }
      set fullNameList {}
      set configureEnabled 1
      .button.top.load configure -state disabled
      .button.top.stepforward configure -state disabled
      .button.bot.stepbackward configure -state disabled
      .button.top.nextcycle configure -state disabled
      .button.bot.previouscycle configure -state disabled
      .button.top.go configure -state disabled
      .button.bot.stop configure -state disabled
      .button.top.reset configure -state disabled
    }
    3 {
      switch $config {
        0 {
          .bas.up.code delete Stage
          .bas.up.cycle delete Stage
          .bas.up.cycle itemconfigure strip -fill lavender 
          .bas.datapath itemconfigure line -fill black -width 1
	  .bas.datapath itemconfigure choise -fill lavender
	  .bas.datapath itemconfigure text -text ""
          .bas.datapath.counter configure -text {}
          .bas.block itemconfigure rect -fill lavender
          .bas.block.counter configure -text {}
        }
        1 {
          set c .tom.c
          $c delete thickLine
          for {set i 0} {$i < 64} {incr i} {
            $c.reg$i delete 1.0 end
          }
          for {set i 0} {$i < $num_int_units} {incr i} {
            $c.int$i delete 1.0 end
          }
          for {set i 0} {$i < $num_add_units} {incr i} {
            $c.add$i delete 1.0 end
          }
          for {set i 0} {$i < $num_mul_units} {incr i} {
            $c.mul$i delete 1.0 end
          }  
          $c.instr delete 1.0 end  
          if {$num_div_units > "0"} {
            for {set i 0} {$i < $num_div_units} {incr i} {
              $c.div$i delete 1.0 end
            } 
            set fp_div_exist 1
          } else {
            set fp_div_exist 0
          }
          if {$num_load_bufs > "0"} {
            for {set i 0} {$i < $num_load_bufs} {incr i} {
              $c.load$i delete 1.0 end
            } 
            for {set i 0} {$i < $num_store_bufs} {incr i} {
              $c.store$i delete 1.0 end
            } 
            set ld_st_exist 1
          } else {
            set ld_st_exist 0
          }
          $c.frame.table delete 1.0 end    
          $c.counter configure -text {}    
        }
        2 {
          set c .sco.c
          $c delete thickLine
          for {set i 0} {$i < 64} {incr i} {
            $c.reg$i delete 1.0 end
          }
          for {set i 0} {$i < $num_int_units} {incr i} {
            $c.int$i delete 1.0 end
          }
          for {set i 0} {$i < $num_add_units} {incr i} {
            $c.add$i delete 1.0 end
          }
          for {set i 0} {$i < $num_mul_units} {incr i} {
            $c.mul$i delete 1.0 end
          }    
          if {$num_div_units > "0"} {
            for {set i 0} {$i < $num_div_units} {incr i} {
              $c.div$i delete 1.0 end
            } 
            set fp_div_exist 1
          } else {
            set fp_div_exist 0
          }
          $c.frame.table delete 1.0 end 
          $c.counter configure -text {}       
        }
      } 
      set backFullNameList $fullNameList
      set fullNameList {}
      foreach file $backFullNameList {
        Load $file
      }
      .button.top.stepforward configure -state normal
      .button.top.go configure -state normal
      .button.top.nextcycle configure -state normal
    }
  }
}
