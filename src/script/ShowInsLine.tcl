# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc ShowInsLine {w} {
  set c .tom.c
  set end [string index $w 7]
  $c create line 280 50 280 204 -tag thickLine -width 8 -fill ForestGreen
  if {$end == "i"} {
    $c create line 280 200 65 200 65 236 -tag thickLine -width 8 -fill ForestGreen \
       -joinstyle miter
  }
  if {$end == "a"} {
    $c create line 280 200 315 200 315 236 -tag thickLine -width 8 -fill ForestGreen \
       -joinstyle miter
  }
  if {$end == "m"} {
    $c create line 280 200 565 200 565 236 -tag thickLine -width 8 -fill ForestGreen \
       -joinstyle miter
  }
  if {$end == "d"} {
    $c create line 280 200 815 200 815 236 -tag thickLine -width 6 -fill ForestGreen \
       -joinstyle miter
  }
}
