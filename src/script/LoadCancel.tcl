# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc LoadCancel {oldFocus {w .load}} {
  global ld_st_exist fp_div_exist fullNameList
  if {$fullNameList == {}} {
    destroy $w
    focus $oldFocus
    return
  }
  set mode [tk_dialog .loadcancel {Cancel} {This will force all the files \
    (instructions and data) loaded previously being moved out.} \
    warning 0 Ok Cancel]
 
  switch $mode {
    0 {
      set backLd_st_exist $ld_st_exist
      set backFp_div_exist $fp_div_exist
      init
      .code.t delete 1.0 end
      set ld_st_exist $backLd_st_exist
      set fp_div_exist $backFp_div_exist
      set fullNameList {}
      destroy $w
      focus $oldFocus
    }
  }
}
