# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc mkTomasulo {{w .tom}} {
  catch {destroy $w}
  toplevel $w
  wm title $w "Tomasulo Algorithm on DLX"
  wm iconname $w "tomasulo"
  wm geometry $w +0+0
  set c $w.c
  option add Tk$w*Text.Background gainsboro
  canvas $c \
    -width 1030 \
    -height 600 \
    -relief raised \
    -scrollregion {30 0 1060 600}
  pack $c
  global num_int_units num_add_units num_mul_units
  global num_div_units num_load_bufs num_store_bufs
  global fp_div_exist ld_st_exist cycleCount
  if {$ld_st_exist} {
    text $c.load \
      -relief raised \
      -borderwidth 1 \
      -height 1 \
      -width 12 \
      -font Courier?10?normal
    DisableTextWidget $c.load
    $c create window 50 51 \
      -anchor sw \
      -window $c.load
    $c.load insert end "  address"
    for {set i [expr $num_load_bufs-1]} {$i >=0} {incr i -1} {
      text $c.load$i \
        -relief raised \
        -borderwidth 1 \
        -setgrid true \
        -height 1 \
        -width 12 \
        -font Courier?10?normal
      DisableTextWidget $c.load$i
      $c create window 50 [expr 50+($num_load_bufs-1-$i)*13] \
        -anchor nw \
        -window $c.load$i
      bind $c.load$i <Button-1> "mkEquation Load [expr $i+32]"
      $c create text 42 [expr 57+($num_load_bufs-1-$i)*13] \
        -font Courier?12?normal\
        -text [expr $i+1]
    }
    $c create line 90 [expr 50+$num_load_bufs*13] 90 170 35 170 35 410 160 410 \
      -width 4
    $c create line 80 15 80 36 \
      -arrow last
    $c create text 92 25 \
      -font Courier?12?normal\
      -text "Load Buffers"
  }
  text $c.instr \
    -relief raised \
    -borderwidth 1 \
    -setgrid true \
    -background ivory \
    -height 1 \
    -width 40 \
    -font Courier?10?normal
  DisableTextWidget $c.instr
  $c create window 160 50 \
    -anchor sw \
    -window $c.instr
 
  text $c.reg \
    -relief raised \
    -borderwidth 1 \
    -height 1 \
    -width 56 \
    -font Courier?10?normal
  DisableTextWidget $c.reg
  $c create window 430 51 \
    -anchor sw \
    -window $c.reg
  $c.reg insert end "  Qi     Qi     Qi     Qi     Qi     Qi     Qi     Qi"
  for {set i 0} {$i < 64} {incr i} {
    text $c.reg$i \
      -relief raised \
      -borderwidth 1 \
      -setgrid true \
      -height 1 \
      -width 7 \
      -font Courier?10?normal
    DisableTextWidget $c.reg$i
    $c create window [expr 430+$i%8*42] [expr 50+$i/8*13] \
      -anchor nw \
      -window $c.reg$i
    if {$i%8 == 0} {
      if {$i < 32} {
        set reg R$i
      } else {
        set reg F[expr $i-32]
      }
      $c create text 405 [expr 57+$i/8*13] \
        -anchor w \
        -font Courier?12?normal\
        -text $reg
    }
  }
  if {$ld_st_exist} {
    text $c.store \
      -relief raised \
      -borderwidth 1 \
      -height 1 \
      -width 28 \
      -font Courier?10?normal
    DisableTextWidget $c.store
    $c create window 845 51 \
      -anchor sw \
      -window $c.store
    $c.store insert end "   Vi      Qi    address"
    for {set i [expr $num_store_bufs-1]} {$i >=0} {incr i -1} {
      text $c.store$i \
        -relief raised \
        -borderwidth 1 \
        -setgrid true \
        -height 1 \
        -width 28 \
        -font Courier?10?normal
      DisableTextWidget $c.store$i
      $c create window 845 [expr 50+($num_store_bufs-1-$i)*13] \
        -anchor nw \
        -window $c.store$i
      bind $c.store$i <Button-1> "mkEquation Store [expr $i+40]"
      $c create text 837 [expr 57+($num_store_bufs-1-$i)*13] \
        -font Courier?12?normal\
        -text [expr $i+1]
    }
    $c create line 935 15 935 37 \
      -arrow last
    $c create line 1000 [expr 50+$num_store_bufs*13] 1000 [expr 70+$num_store_bufs*13] \
      -arrow last
    $c create line 885 180 885 [expr 50+$num_store_bufs*13] \
      -arrow last
    $c create text 980 [expr 75+$num_store_bufs*13] \
      -font Courier?12?normal\
      -text "To Memory"
    $c create text 885 25 \
      -font Courier?12?normal\
      -text "Store buffers"
  }
  text $c.int \
    -relief raised \
    -borderwidth 1 \
    -height 1 \
    -width 37 \
    -font Courier?10?normal
  DisableTextWidget $c.int
  $c create window 50 251 \
    -anchor sw \
    -window $c.int
  $c.int insert end " op      Vj      Qj      Vk      Qk"
  for {set i [expr $num_int_units-1]} {$i >= 0} {incr i -1} {
    text $c.int$i \
      -relief raised \
      -borderwidth 1 \
      -setgrid true \
      -height 1 \
      -width 37 \
      -font Courier?10?normal
    DisableTextWidget $c.int$i
    $c create window 50 [expr 250+($num_int_units-1-$i)*13] \
      -anchor nw \
      -window $c.int$i
    bind $c.int$i <Button-1> "mkEquation Int $i"
    $c create text 42 [expr 257+($num_int_units-1-$i)*13] \
      -font Courier?12?normal\
      -text [expr $i+1]
  }
  set yshift [expr $num_int_units*13]
  $c create line 135 [expr 250+$yshift] 135 371 \
    -arrow last
  $c create line 185 [expr 250+$yshift] 185 371 \
    -arrow last
  $c create line 160 389 160 408 \
    -arrow last
  label $c.intl \
    -relief raised \
    -borderwidth 1 \
    -text "Integer units"
  $c create window 160 380 \
    -window $c.intl
  text $c.add \
    -relief raised \
    -borderwidth 1 \
    -height 1 \
    -width 37 \
    -font Courier?10?normal
  DisableTextWidget $c.add
  $c create window 300 251 \
    -anchor sw \
    -window $c.add
  $c.add insert end " op      Vj      Qj      Vk      Qk"
  for {set i [expr $num_add_units-1]} {$i >= 0} {incr i -1} {
    text $c.add$i \
      -relief raised \
      -borderwidth 1 \
      -setgrid true \
      -height 1 \
      -width 37 \
      -font Courier?10?normal
    DisableTextWidget $c.add$i
    $c create window 300 [expr 250+($num_add_units-1-$i)*13] \
      -anchor nw \
      -window $c.add$i
    bind $c.add$i <Button-1> "mkEquation Add [expr $i+8]"
    $c create text 292 [expr 257+($num_add_units-1-$i)*13] \
      -font Courier?12?normal\
      -text [expr $i+1]
  }
  set yshift [expr $num_add_units*13]
  $c create line 385 [expr 250+$yshift] 385 371 \
    -arrow last
  $c create line 435 [expr 250+$yshift] 435 371 \
    -arrow last
  $c create line 410 389 410 408 \
    -arrow last
  label $c.addl \
    -relief raised \
    -borderwidth 1 \
    -text "FP adders"
  $c create window 410 380 \
    -window $c.addl
  text $c.mul \
    -relief raised \
    -borderwidth 1 \
    -height 1 \
    -width 37 \
    -font Courier?10?normal
  DisableTextWidget $c.mul
  $c create window 550 251 \
    -anchor sw \
    -window $c.mul
  $c.mul insert end " op      Vj      Qj      Vk      Qk"
  for {set i [expr $num_mul_units-1]} {$i >= 0} {incr i -1} {
    text $c.mul$i \
      -relief raised \
      -borderwidth 1 \
      -setgrid true \
      -height 1 \
      -width 37 \
      -font Courier?10?normal
    DisableTextWidget $c.mul$i
    $c create window 550 [expr 250+($num_mul_units-1-$i)*13] \
      -anchor nw \
      -window $c.mul$i
    bind $c.mul$i <Button-1> "mkEquation Mul [expr $i+24]"
    $c create text 542 [expr 257+($num_mul_units-1-$i)*13] \
      -font Courier?12?normal\
      -text [expr $i+1]
  }
  set yshift [expr $num_mul_units*13]
  $c create line 635 [expr 250+$yshift] 635 371 \
    -arrow last
  $c create line 685 [expr 250+$yshift] 685 371 \
    -arrow last
  $c create line 660 389 660 408 \
    -arrow last
  label $c.mull \
    -relief raised \
    -borderwidth 1 \
    -text "FP multipliers"
  $c create window 660 380 \
    -window $c.mull
  if {$fp_div_exist} {
    text $c.div \
      -relief raised \
      -borderwidth 1 \
      -height 1 \
      -width 37 \
      -font Courier?10?normal
    DisableTextWidget $c.div
    $c create window 800 251 \
      -anchor sw \
      -window $c.div
    $c.div insert end " op      Vj      Qj      Vk      Qk"
    for {set i [expr $num_div_units-1]} {$i >= 0} {incr i -1} {
      text $c.div$i \
        -relief raised \
        -borderwidth 1 \
        -setgrid true \
        -height 1 \
        -width 37 \
        -font Courier?10?normal
      DisableTextWidget $c.div$i
      $c create window 800 [expr 250+($num_div_units-1-$i)*13] \
        -anchor nw \
        -window $c.div$i
      bind $c.div$i <Button-1> "mkEquation Div [expr $i+16]"
      $c create text 792 [expr 257+($num_div_units-1-$i)*13] \
        -font Courier?12?normal\
        -text [expr $i+1]
    }
    set yshift [expr $num_div_units*13]
    $c create line 885 [expr 250+$yshift] 885 371 \
      -arrow last
    $c create line 935 [expr 250+$yshift] 935 371 \
      -arrow last
    $c create line 910 389 910 408 \
      -arrow last
    label $c.divl \
      -relief raised \
      -borderwidth 1 \
      -text "FP dividers"
    $c create window 910 380 \
      -window $c.divl
    $c create line 565 200 815 200 815 236 \
      -arrow last
    $c create line 635 180 885 180 885 236 \
      -arrow last
    $c create line 735 190 985 190 985 236 \
      -arrow last
    $c create oval 563 198 569 204 \
      -fill black
    $c create oval 633 178 639 184 \
      -fill black
    $c create oval 733 188 739 194 \
      -fill black
    $c create oval 883 212 889 218 \
      -fill black
    $c create oval 983 212 989 218 \
      -fill black
 }
  frame $c.frame
  text $c.frame.table \
    -relief raised \
    -borderwidth 1 \
    -height 9 \
    -width 60 \
    -background lavender \
    -yscrollcommand "$c.frame.s set" \
    -font Courier?12?normal
  DisableTextWidget $c.frame.table
  label $c.frame.l1 \
    -relief raised \
    -borderwidth 1 \
    -font Courier?12?bold \
    -text "                      Instruction status                    "
  label $c.frame.l2 \
    -relief raised \
    -borderwidth 1 \
    -font Courier?12?bold \
    -text "  Instruction             Issue   Excute  Write result      "
  scrollbar $c.frame.s \
    -relief raised \
    -borderwidth 1 \
    -command "$c.frame.table yview"
  pack $c.frame.s \
    -side right \
    -fill y
  pack $c.frame.l1 $c.frame.l2 \
    -side top \
    -expand yes \
    -fill x
  pack $c.frame.table \
    -side bottom
  $c create window 30 430 \
    -anchor nw \
    -window $c.frame
  label $c.counter \
    -font Times?40?normal
  $c create window 750 500 \
    -window $c.counter
  $c create text 750 550 \
    -font Courier?10?normal \
    -text "Issue Cycle of Current Instruction" \
    -tag cycleTitle
  $c create line 320 15 320 36 \
    -arrow last
  $c create line 280 50 280 200 65 200 65 236 \
    -arrow last
  $c create line 280 200 315 200 315 236 \
    -arrow last
  $c create line 315 200 565 200 565 236 \
    -arrow last
  $c create line 520 154 520 180 135 180 135 236 \
    -arrow last
  $c create line 385 180 385 236 \
    -arrow last
  $c create line 520 180 635 180 635 236 \
    -arrow last
  $c create line 550 154 550 190 485 190 485 236 \
    -arrow last 
  $c create line 485 190 235 190 235 236 \
    -arrow last
  $c create line 550 190 735 190 735 236 \
    -arrow last
  $c create line 160 410 1032 410 1032 15 620 15 620 37 \
    -width 4 \
    -arrow last 
  $c create line 1032 215 135 215 \
    -width 4
  $c create oval 278 198 284 204 \
    -fill black
  $c create oval 313 198 319 204 \
    -fill black
  $c create oval 383 178 389 184 \
    -fill black
  $c create oval 518 178 524 184 \
    -fill black
  $c create oval 548 188 554 194 \
    -fill black
  $c create oval 483 188 489 194 \
    -fill black
  $c create oval 133 212 139 218 \
    -fill black
  $c create oval 233 212 239 218 \
    -fill black
  $c create oval 383 212 389 218 \
    -fill black
  $c create oval 483 212 489 218 \
    -fill black
  $c create oval 633 212 639 218 \
    -fill black
  $c create oval 733 212 739 218 \
    -fill black
  $c create text 320 10 \
    -font Courier?12?normal\
    -text "From instruction unit"
  $c create text 220 25 \
    -font Courier?12?normal\
    -text "Current instruction"
  $c create text 540 25 \
    -font Courier?12?normal\
    -text "Register file"
  $c create text 230 140 \
    -font Courier?12?normal\
    -text "Operation bus"
  $c create text 478 165 \
    -font Courier?12?normal\
    -text "Operand bus"
  $c create text 305 228 \
    -font Courier?12?normal\
    -text "Reservation stations"
  $c create text 285 400 \
    -font Courier?12?normal\
    -text "Common data bus (CDB)"
}
