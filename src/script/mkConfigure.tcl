# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc mkConfigure {{w .configure}} {
  global config
  global numWords
  global configureEnabled
  set configOrig $config
  set numWordsOrig $numWords
  catch {destroy $w}
  toplevel $w
  wm title $w "Configure"
  wm iconname $w "configure"
  wm geometry $w +0+0
  set oldFocus [focus]
  frame $w.config  
  frame $w.mem
  frame $w.button
  pack $w.config $w.mem $w.button \
    -side top \
    -fill x \
    -expand yes
  radiobutton $w.config.basicpipe \
    -relief flat \
    -text "Basic Pipeline" \
    -variable config \
    -value 0
  radiobutton $w.config.tomasulo \
    -relief flat \
    -text "Tomasulo" \
    -variable config \
    -value 1
  radiobutton $w.config.scoreboarding \
    -relief flat \
    -text "Scoreboarding" \
    -variable config \
    -value 2
  pack $w.config.basicpipe $w.config.tomasulo $w.config.scoreboarding \
    -side left \
    -fill x \
    -expand yes \
    -padx 5 \
    -pady 2
  scale $w.mem.size \
    -label "memory size (words)" \
    -from 2048 \
    -to 16384 \
    -tickinterval 14336 \
    -command ShowMemSize
  pack $w.mem.size \
    -side left \
    -fill x \
    -expand yes \
    -padx 5 \
    -pady 10
  button $w.button.ok \
    -text Ok \
    -state disabled \
    -command "ConfigureOk $oldFocus"
  button $w.button.cancel \
    -text Cancel \
    -command "destroy $w
      focus $oldFocus
      set config $configOrig
      set numWords $numWordsOrig"
  pack $w.button.ok $w.button.cancel \
    -side left \
    -fill x \
    -expand yes
  trace variable config w SubConfigure
  $w.mem.size set $numWords
 
  if {$configureEnabled == "0"} {
    $w.mem.size configure -state disabled
    $w.mem.size configure -troughcolor lightgrey
    $w.mem.size configure -activebackground lightgrey
    $w.button.ok configure -state disabled
    switch $config {
      0 {
          $w.config.tomasulo configure -state disabled
          $w.config.scoreboarding configure -state disabled
        }
      1 {
          $w.config.basicpipe configure -state disabled
          $w.config.scoreboarding configure -state disabled
        }
      2 {
          $w.config.basicpipe configure -state disabled
          $w.config.tomasulo configure -state disabled
        }
    }
  }
   
  grab $w
  focus $w
}
