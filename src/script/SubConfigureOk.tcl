# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc SubConfigureOk {} {
  global num_int_units ld_st_exist fp_div_exist 
  global num_load_bufs num_div_units
  global config configureEnabled 
  if {$config == "0"} {
    set num_int_units 2
    set cycleDisplayCount 0
  }
  if {$config == "1" && $num_load_bufs > "0"} {
    set ld_st_exist 1
  } else {
    set ld_st_exist 0
  }
  if {$num_div_units > "0"} {
    set fp_div_exist 1
  } else {
    set fp_div_exist 0
  }
}
