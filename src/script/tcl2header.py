#!/usr/bin/env python3

# Copyright 2018 Bernhard Guillon <Bernhard.Guillon@begu.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the "Software"), to deal in the Software
# without restriction, including without limitation the rights to use, copy, modify,
# merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.

import argparse
import os

class Tcl2Header:

    def __init__(self):
        self.setup_parser()

    def setup_parser(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('-s','--source', nargs='+', help='<Required> tcl source file', required=True)
        parser.add_argument('-o','--output', help='<Required> output header file', required=True)
        self.args = parser.parse_args()

    def generate_header(self):
        guard = self.args.output.upper()
        guard = guard.replace('.', '_')
        header = "/* Generated header file, please don't modify directly */\n\n"
        header = header + "#ifndef " + guard + "\n"
        header = header + "#define " + guard + "\n\n"
        return header

    def generate_footer(self):
        return "\n#endif\n"

    def escape_line(self, line):
        escaped_line=""
        escaped_line=line.replace('\\', '\\\\')
        escaped_line=escaped_line.replace('"', '\\"')
        escaped_line=escaped_line +'\\n\\' + '\n'
        return escaped_line

    def parse_source(self, source):
        basename = os.path.basename(source)
        res = "char " + basename.replace(".tcl", "") + "_tcl[] = \"\\" + "\n"
        with open(source, 'r') as f:
            for line in f:
                current_proc=""
                end_token=""
                if line.startswith('#') or line == "\n":
                    continue

                res = res + self.escape_line(line.strip('\n'))

        res = res + "\";" + "\n\n"

        return res

    def generate_header_file(self):
        with open(self.args.output, 'w') as f:
            f.write(self.generate_header())
            for source in self.args.source:
                f.write(self.parse_source(source))
            f.write(self.generate_footer())

if __name__ == "__main__":
    tcl2header = Tcl2Header()
    tcl2header.generate_header_file()