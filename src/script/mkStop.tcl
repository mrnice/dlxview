# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc mkStop {{w .stop}} {
  catch {destroy $w}
  toplevel $w
  wm title $w "Stop"
  wm iconname $w "stop"
  wm geometry $w +200+200
  set oldFocus [focus]
  global msg stops
  option add Tk$w*Relief raised
  option add Tk$w*Label.Anchor w
  option add Tk$w*Listbox.Setgrid yes
  option add Tk$w*Label.Relief flat
  option add Tk$w*Entry.Width 10
  frame $w.entry
  frame $w.list
  frame $w.button
  pack $w.entry \
    -side top \
    -fill x \
    -expand yes
  pack $w.list \
    -side top \
    -fill x \
    -expand yes
  pack $w.button \
    -side bottom \
    -fill x \
    -expand yes
  label $w.entry.l \
    -text "Stop at"
  entry $w.entry.e
  bind $w.entry.e <Return> "stop at \[$w.entry.e get \]
    $w.list.list delete 0 end
    stop info"
  pack $w.entry.l \
    -side left
  pack $w.entry.e \
    -side left \
    -fill x \
    -expand yes
  label $w.list.label \
    -text "Current Stop Information"
  listbox $w.list.list \
    -yscrollcommand "$w.list.scroll set"
  
  scrollbar $w.list.scroll \
    -command "$w.list.list yview"   
  pack $w.list.label \
    -side top
 
  pack $w.list.list $w.list.scroll \
    -side left \
    -fill both \
    -expand yes
  button $w.button.delete \
    -text "Delete Stop" \
  button $w.button.ok \
    -text Ok \
    -command "destroy $w;  focus $oldFocus"
  pack $w.button.delete $w.button.ok \
    -side left \
    -fill x \
    -expand yes   
  stop info
  focus $w.entry.e
  grab $w
}
