# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

source mkConfigure.tcl
source ShowMemSize.tcl
source SubConfigure.tcl
source BasConfigure.tcl
source TomConfigure.tcl
source ScoConfigure.tcl
source ConfigureOk.tcl
source SubConfigureOk.tcl
source ScaleValue.tcl
source BasRadioCommand.tcl
source mkDisc.tcl
source DISC.tcl
source mkBasicPipe.tcl
source create_dot.tcl
source BasVisual.tcl
source BasGenTable.tcl
source BasClean.tcl
source BasChangeStrip.tcl
source mkStepBack.tcl
source mkPreviousCycle.tcl
source DeleteAndInsert.tcl
source InsertAndHighLight.tcl
source UnHighLight.tcl
source AddToEnd.tcl
source HighLightCodeLine.tcl
source Trap0.tcl
source Err.tcl
source ChangeCycleTitle.tcl
source ChangeCycleIcon.tcl
source Start.tcl
source DisableTextWidget.tcl
source mkGoStep.tcl
source Go.tcl
source Step.tcl
source Cycle.tcl
source demo.tcl
source demoactbody.tcl
source mkLoad.tcl
source LoadOpen.tcl
source LoadRefresh.tcl
source LoadFile.tcl
source Load.tcl
source LoadDone.tcl
source LoadCancel.tcl
source FileEntryReturn.tcl
source mkReset.tcl
source ResetMode.tcl
source mkScoreboard.tcl
source mkStop.tcl
source mkTomasulo.tcl
source ShowCdbLineBAK.tcl
source ShowCdbLine.tcl
source HighLightWrite.tcl
source ShowInsLine.tcl
source ShowResLine.tcl
source mkEquation.tcl
source InsertWithTags.tcl