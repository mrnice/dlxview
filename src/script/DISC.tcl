# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

set auto_path "[pwd] $auto_path"
set config 3
set numWords 16384
set num_int_units 1
set int_latency 1
set num_add_units 2
set fp_add_latency 2
set add_ful_pipe 1
set num_mul_units 2
set fp_mul_latency 5
set mul_ful_pipe 1
set num_div_units 0
set fp_div_latency 19
set div_ful_pipe 0
set num_load_bufs 0
set load_buf_latency 1
set num_store_bufs 0
set store_buf_latency 1
set ld_st_exist 0
set fp_div_exist 1
set fullNameList {}
set startAddress {}
set configureEnabled 1
set fastmode 0
global msg
mkDisc
