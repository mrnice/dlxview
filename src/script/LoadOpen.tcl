# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc LoadOpen {fname {w .load}} {
  global curDir
  if {[llength $fname] > "1"} {
    $w.entry.down.fileentry delete 0 end
    $w.entry.down.fileentry insert 0 "Multiple files are selected"
    $w.list.filelist delete 0 end
  } elseif {[file isdirectory $fname]} {
    cd $fname
    set curDir [pwd]
    LoadRefresh
  } else {
    set thisfile [open $fname r]
    $w.entry.down.fileentry delete 0 end
    $w.entry.down.fileentry insert 0 $fname
    $w.list.filelist delete 0 end
    for {set i 1} {$i < 50} {incr i} {
      $w.list.filelist insert end [gets $thisfile]
    }
    update
    close $thisfile
  }
}
