# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc ChangeCycleTitle {cycleMode} {
  global config
  grab set .code.t
  switch $config {
    0 {
        if {$cycleMode} {
          .bas.datapath itemconfigure cycleTitle -text "Current Cycle"
          .bas.block itemconfigure cycleTitle -text "Current Cycle"
        } else {
          .bas.datapath itemconfigure cycleTitle -text "IF Cycle of Current Instruction"
          .bas.block itemconfigure cycleTitle -text "IF Cycle of Current Instruction"
          .bas.datapath.counter configure -bitmap hourglass
          .bas.block.counter configure -bitmap hourglass
        }
      }
    1 {
        if {$cycleMode} {
          .tom.c itemconfigure cycleTitle -text "Current Cycle"
        } else {
          .tom.c itemconfigure cycleTitle -text "Issue Cycle of Current Instruction"
          .tom.c.counter configure -bitmap hourglass
        }
        catch {destroy .equation}
      }
    2 {
        if {$cycleMode} {
          .bas.c itemconfigure cycleTitle -text "Current Cycle"
        } else {
          .bas.c itemconfigure cycleTitle -text "Issue Cycle of Current Instruction"
          .bas.c.counter configure -bitmap hourglass
        }
        catch {destroy .equation}
      }
  }
  update
} 
