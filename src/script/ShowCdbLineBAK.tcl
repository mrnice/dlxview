# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc ShowCdbLineBAK {w} {
  set c .tom.c
  set start [string index $w 7]
  if {$start == "l"} {
    for {set y 60} {$y < 170} {incr y 10} {
      $c create line 90 $y 90 [expr $y+10] -tag thickLine -width 10 -fill red
      
    }
    for {set x 95} {$x > 25} {incr x -10} {
      $c create line $x 170 [expr $x-10] 170 -tag thickLine -width 10 -fill red
      
    }
    for {set y 170} {$y < 410} {incr y 10} {
      $c create line 30 $y 30 [expr $y+10] -tag thickLine -width 10 -fill red
      
    }
    for {set x 25} {$x < 166} {incr x +10} {
      $c create line $x 410 [expr $x+10] 410 -tag thickLine -width 10 -fill red
      
    }
  }
  if {$start == "l" || $start == "i"} {
    for {set x 160} {$x < 410} {incr x +10} {
      $c create line $x 410 [expr $x+10] 410 -tag thickLine -width 10 -fill red
      
    }
  }
  if {$start == "l" || $start == "i" || $start == "a"} {
    for {set x 410} {$x < 660} {incr x +10} {
      $c create line $x 410 [expr $x+10] 410 -tag thickLine -width 10 -fill red
      
    }
  }
  if {$start == "l" || $start == "i" || $start == "a" || $start == "m"} {
    for {set x 660} {$x < 910} {incr x +10} {
      $c create line $x 410 [expr $x+10] 410 -tag thickLine -width 10 -fill red
      
    }
  }
    
  for {set x 910} {$x < 1040} {incr x +10} {
    $c create line $x 410 [expr $x+10] 410 -tag thickLine -width 10 -fill red
    
  }
  for {set y 415} {$y > 215} {incr y -10} {
    $c create line 1040 $y 1040 [expr $y-10] -tag thickLine -width 10 -fill red
    
  }
  for {set i 0} {$i < 21} {incr i} {
    $c create line 1040 [expr 220-$i*10] 1040 [expr 210-$i*10] -tag thickLine \
      -width 10 -fill red
    $c create line [expr 1040-$i*10] 215 [expr 1030-$i*10] 215 -tag thickLine \
      -width 10 -fill red 
    
  }
  for {set i 0} {$i < 42} {incr i} {
    $c create line [expr 1040-$i*10] 15 [expr 1030-$i*10] 15 -tag thickLine \
      -width 10 -fill red
    $c create line [expr 840-$i*10] 215 [expr 830-$i*10] 215 -tag thickLine \
      -width 10 -fill red
    
  }
  for {set i 0} {$i < 4} {incr i} {
    $c create line 620 [expr 10+$i*10] 620 [expr 20+$i*10] -tag thickLine \
      -width 10 -fill red
    $c create line [expr 460-$i*10] 215 [expr 460-$i*10] 215 -tag thickLine \
      -width 10 -fill red 
    
  }
  for {set x 420} {$x > 140} {incr x -10} {
    $c create line $x 215 [expr $x-10] 215 -tag thickLine -width 10 -fill red
    
  }
  $c create line 140 215 135 215 -tag thickLine -width 10 -fill red
  HighLightWrite $w
}
