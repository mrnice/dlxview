# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc ShowResLine {w} {
  global num_int_units num_add_units num_mul_units num_div_units
  set c [string range $w 1 6]
  set start [string range $w 0 2]
  set end [string index $w 8]
  if {$start == "1.t" && $end == "i"} {
    $c create line 520 154 520 180 135 180 135 236 -tag thickLine -width 8 \
       -fill blue  -joinstyle miter
  } elseif {$start == "1.t" && $end == "a"} {
    $c create line 520 154 520 180 385 180 385 236 -tag thickLine -width 8 \
       -fill blue  -joinstyle miter
  } elseif {$start == "1.t" && $end == "m"} {
    $c create line 520 154 520 180 635 180 635 236 -tag thickLine -width 8 \
       -fill blue  -joinstyle miter
  } elseif {$start == "1.t" && $end == "d"} {
    $c create line 520 154 520 180 885 180 885 236 -tag thickLine -width 8 \ 
       -fill blue  -joinstyle miter
  } elseif {$start == "2.t" && $end == "i"} {
    $c create line 550 154 550 190 235 190 235 236 -tag thickLine -width 8 \
       -fill blue  -joinstyle miter
  } elseif {$start == "2.t" && $end == "a"} {
    $c create line 550 154 550 190 485 190 485 236 -tag thickLine -width 8 \
       -fill blue  -joinstyle miter
  } elseif {$start == "2.t" && $end == "m"} {
    $c create line 550 154 550 190 735 190 735 236 -tag thickLine -width 8 \
       -fill blue  -joinstyle miter
  } elseif {$start == "2.t" && $end == "d"} {
    $c create line 550 154 550 190 985 190 985 236 -tag thickLine -width 8 \
       -fill blue  -joinstyle miter
  } elseif {$start == "2.t" && $end == "s"} {
    $c create line 550 154 550 190 885 190 885 50 -tag thickLine -width 8 \
       -fill blue  -joinstyle miter
  } elseif {$start == "i.t" && $end == "i"} {
    $c create line 280 50 280 200 65 200 65 236 -tag thickLine -width 8 \
       -fill ForestGreen -joinstyle miter
  } elseif {$start == "i.t" && $end == "a"} {
    $c create line 280 50 280 200 315 200 315 236 -tag thickLine -width 8 \
       -fill ForestGreen -joinstyle miter
  } elseif {$start == "i.t" && $end == "m"} {
    $c create line 280 50 280 200 565 200 565 236 -tag thickLine -width 8 \
       -fill ForestGreen -joinstyle miter
  } elseif {$start == "i.t" && $end == "d"} {
    $c create line 280 50 280 200 815 200 815 236 -tag thickLine -width 8 \
       -fill ForestGreen -joinstyle miter
  } elseif {$start == "i.t"} {
    $c create line 280 50 280 204 -tag thickLine -width 8 \
       -fill ForestGreen -joinstyle miter
  } elseif {$start == "1.s" && $end == "i"} {
    $c create line 139 98 250 98 -tag thickLine -width 7 -fill blue 
    set y [expr 107-$num_int_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill blue
  } elseif {$start == "1.s" && $end == "a"} {
    $c create line 139 202 250 202 -tag thickLine -width 7 -fill blue
    set y [expr 211-$num_add_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill blue
  } elseif {$start == "1.s" && $end == "m"} {
    $c create line 139 306 250 306 -tag thickLine -width 7 -fill blue 
    set y [expr 315-$num_mul_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill blue
  } elseif {$start == "1.s" && $end == "d"} {
    $c create line 139 410 250 410 -tag thickLine -width 7 -fill blue
    set y [expr 419-$num_div_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill blue
  } elseif {$start == "2.s" && $end == "i"} {
    $c create line 139 106 250 106 -tag thickLine -width 7 -fill blue 
    set y [expr 107-$num_int_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill blue
  } elseif {$start == "2.s" && $end == "a"} {
    $c create line 139 210 250 210 -tag thickLine -width 7 -fill blue
    set y [expr 211-$num_add_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill blue
  } elseif {$start == "2.s" && $end == "m"} {
    $c create line 139 314 250 314 -tag thickLine -width 7 -fill blue 
    set y [expr 315-$num_mul_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill blue
  } elseif {$start == "2.s" && $end == "d"} {
    $c create line 139 418 250 418 -tag thickLine -width 7 -fill blue
    set y [expr 419-$num_div_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill blue
  } elseif {$start == "r.s" && $end == "i"} {
    $c create line 290 102 325 102 325 56 139 56 -tag thickLine -width 7 \
      -fill red -joinstyle miter
    set y [expr 107-$num_int_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill red
  } elseif {$start == "r.s" && $end == "a"} {
    $c create line 290 206 325 206 325 160 139 160 -tag thickLine -width 7 \
      -fill red -joinstyle miter
    set y [expr 211-$num_add_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill red
  } elseif {$start == "r.s" && $end == "m"} {
    $c create line 290 310 325 310 325 264 139 264 -tag thickLine -width 7 \
      -fill red -joinstyle miter
    set y [expr 315-$num_mul_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill red
  } elseif {$start == "r.s" && $end == "d"} {
    $c create line 290 414 325 414 325 368 139 368 -tag thickLine -width 7 \
      -fill red -joinstyle miter
    set y [expr 419-$num_div_units*5+[string index $w 11]*10]
    $c create line 250 $y 290 $y -tag thickLine -width 8 -fill red
  }
   
}
