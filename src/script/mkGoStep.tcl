# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc mkGoStep {choice {w .gostep}} {
  catch {destroy $w}
  toplevel $w
  wm title $w $choice
  wm iconname $w $choice
  wm geometry $w +0+0
  set oldFocus [focus]
  option add Tk$w*Relief raised
  option add Tk$w*Button.borderWidth 3
  label $w.label \
    -relief flat \
    -font Courier?14?normal \
    -text "$choice from:"
  frame $w.entryok
  frame $w.button
  pack $w.label $w.entryok \
    -side top \
    -padx 10 \
    -pady 4

  pack $w.button \
    -side bottom \
    -expand yes \
    -fill x \
    -padx 10 \
    -pady 4
  entry $w.entryok.entry \
    -width 20
  bind $w.entryok.entry <Return> "$choice \[$w.entryok.entry get \]
    destroy $w
    focus $oldFocus"

  frame $w.entryok.frame \
    -width 10
  button $w.entryok.ok \
    -text Ok \
    -command "$choice \[$w.entryok.entry get \]
      destroy $w
      focus $oldFocus"
  pack $w.entryok.entry $w.entryok.frame $w.entryok.ok \
    -side left
  button $w.button.default \
    -text Default \
    -command "destroy $w
      focus $oldFocus
      $choice 256"

  button $w.button.main \
    -text Main \
    -command "destroy $w
      focus $oldFocus
      $choice _main"
  button $w.button.cancel \
    -text Cancel \
    -command "destroy $w
      focus $oldFocus"

  pack $w.button.default $w.button.main $w.button.cancel \
    -side left \
    -expand yes \
    -fill x
  focus $w.entryok.entry
}
