# NOTE: This copyright notice was recreated as good as possible
#
# The script.h file had the following Copyright information.
#
#  This file is part of DISC.  It was written by Yinong Zhang
#  (yinong@ecn.purdue.edu)
#
# As the rest of the DISC distribution is copyrighted with
# the following BSD License, I believe it was originally intended
# for this code to be BSD Licensed as well.
#
# Copyright 1989 Regents of the University of California
# Permission to use, copy, modify, and distribute this
# software and its documentation for any purpose and without
# fee is hereby granted, provided that the above copyright
# notice appear in all copies.  The University of California
# makes no representations about the suitability of this
# software for any purpose.  It is provided "as is" without
# express or implied warranty.

proc BasGenTable {head tail IF args} {
  global insCount fastmode
  if {$fastmode} {
    return
  }
  set xstart 180
  set ystart 60
  set c .bas.up

  $c.code delete Stage
  $c.cycle delete Stage
  $c.cycle itemconfigure strip -fill lavender
  $c.cycle itemconfigure strip[expr $IF-$head] -fill gray75
  for {set j $head} {$j <= $tail} {incr j} {
    $c.cycle create text [expr $xstart+50*($j-$head)] [expr $ystart-25] \
      -font Courier?12?normal \
      -text $j \
      -tags Stage
  }
  for {set j 0} {$j < 5} {incr j} {
    set instr [lindex $args $j]
    set len [llength  $instr]
    set instrStage [lrange $instr 0 [expr $len - 2]]
    set instrColor [lindex [lindex $instr [expr $len - 1]] 0]
    set instrContent [lrange [lindex $instr [expr $len - 1]] 1 end]
    set i 0
      $c.code create text 10 [expr $ystart+$j*25] \
        -font Courier?12?normal \
        -text $instrContent \
        -anchor w \
        -fill $instrColor \
        -tags Stage
      foreach stage $instrStage {
        if {$stage == "nonop"} {
	  set stage ""
	}
        $c.cycle create text [expr $xstart+$i*50] [expr $ystart+$j*25] \
          -font Courier?14?bold \
          -text $stage \
          -tags Stage
        incr i
      }
  }
}
