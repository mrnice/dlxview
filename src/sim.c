/*
 * sim.c --
 *
 *  This file contains a simple Tcl-based simulator for an abridged
 *  version of the MIPS DLX architecture.
 *
 *  This file is part of DISC.  It was modified by Yinong Zhang
 *  (yinong@ecn.purdue.edu) from the file "sim.c" in the distribution
 *  of "dlxsim" available at:
 *     ftp://max.stanford.edu/pub/hennessy-patterson.software/dlx.tar.Z
 *
 *  The original source code is copyright as follows:
 *
 * Copyright 1989 Regents of the University of California
 * Permission to use, copy, modify, and distribute this
 * software and its documentation for any purpose and without
 * fee is hereby granted, provided that the above copyright
 * notice appear in all copies.  The University of California
 * makes no representations about the suitability of this
 * software for any purpose.  It is provided "as is" without
 * express or implied warranty.
 *
 */

#define USE_INTERP_RESULT
#define USE_INTERP_ERRORLINE

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <tcl.h>
#include "include/dlx.h"
#include "include/sym.h"


/*
 * The table below is used to translate bits 31:26 of the instruction
 * into a value suitable for the "opCode" field of a MemWord structure,
 * or into a special value for further decoding.
 */

#define SPECIAL 120
#define FPARITH	121

#define IFMT 1
#define JFMT 2
#define RFMT 3




typedef struct {
	int opCode;		/* Translated op code. */
	int format;		/* Format type (IFMT or JFMT or RFMT) */
} OpInfo;

OpInfo opTable[] = {
	{SPECIAL, RFMT}, {FPARITH, RFMT}, {OP_J, JFMT}, {OP_JAL, JFMT},
	{OP_BEQZ, IFMT}, {OP_BNEZ, IFMT}, {OP_BFPT, IFMT}, {OP_BFPF, IFMT},
	{OP_ADDI, IFMT}, {OP_ADDUI, IFMT}, {OP_SUBI, IFMT}, {OP_SUBUI, IFMT},
	{OP_ANDI, IFMT}, {OP_ORI, IFMT}, {OP_XORI, IFMT}, {OP_LHI, IFMT},
	{OP_RFE, IFMT}, {OP_TRAP, IFMT}, {OP_JR, IFMT}, {OP_JALR, IFMT},
	{OP_RES, IFMT}, {OP_RES, IFMT}, {OP_RES, IFMT}, {OP_RES, IFMT},
	{OP_SEQI, IFMT}, {OP_SNEI, IFMT}, {OP_SLTI, IFMT}, {OP_SGTI, IFMT},
	{OP_SLEI, IFMT}, {OP_SGEI, IFMT}, {OP_RES, IFMT}, {OP_RES, IFMT},
	{OP_LB, IFMT}, {OP_LH, IFMT}, {OP_RES, IFMT}, {OP_LW, IFMT},
	{OP_LBU, IFMT}, {OP_LHU, IFMT}, {OP_LF, IFMT}, {OP_LD, IFMT},
	{OP_SB, IFMT}, {OP_SH, IFMT}, {OP_RES, IFMT}, {OP_SW, IFMT},
	{OP_RES, IFMT}, {OP_RES, IFMT}, {OP_SF, IFMT}, {OP_SD, IFMT},
	{OP_SEQUI, IFMT}, {OP_SNEUI, IFMT}, {OP_SLTUI, IFMT}, {OP_SGTUI, IFMT},
	{OP_SLEUI, IFMT}, {OP_SGEUI, IFMT}, {OP_RES, IFMT}, {OP_RES, IFMT},
	{OP_RES, IFMT}, {OP_RES, IFMT}, {OP_RES, IFMT}, {OP_RES, IFMT},
	{OP_RES, IFMT}, {OP_RES, IFMT}, {OP_RES, IFMT}, {OP_RES, IFMT}
};

/*
 * the table below is used to convert the "funct" field of SPECIAL
 * instructions into the "opCode" field of a MemWord.
 */

int specialTable[] = {
	OP_SLLI, OP_RES, OP_SRLI, OP_SRAI, OP_SLL, OP_RES, OP_SRL, OP_SRA,
	OP_RES, OP_RES, OP_RES, OP_RES, OP_TRAP, OP_RES, OP_RES, OP_RES,
	OP_SEQU, OP_SNEU, OP_SLTU, OP_SGTU, OP_SLEU, OP_SGEU, OP_RES, OP_RES,
	OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES,
	OP_ADD, OP_ADDU, OP_SUB, OP_SUBU, OP_AND, OP_OR, OP_XOR, OP_RES,
	OP_SEQ, OP_SNE, OP_SLT, OP_SGT, OP_SLE, OP_SGE, OP_RES, OP_RES,
	OP_MOVI2S, OP_MOVS2I, OP_MOVF, OP_MOVD, OP_MOVFP2I, OP_MOVI2FP,
	OP_RES, OP_RES,
	OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES
};

/*
 * the table below is used to convert the "funct" field of FPARITH
 * instructions into the "opCode" field of a MemWord.
 */
int FParithTable[] = {
	OP_ADDF, OP_SUBF, OP_MULTF, OP_DIVF, OP_ADDD, OP_SUBD, OP_MULTD, OP_DIVD,
	OP_CVTF2D, OP_CVTF2I, OP_CVTD2F, OP_CVTD2I, OP_CVTI2F, OP_CVTI2D,
	OP_MULT, OP_DIV,
	OP_EQF, OP_NEF, OP_LTF, OP_GTF, OP_LEF, OP_GEF, OP_MULTU, OP_DIVU,
	OP_EQD, OP_NED, OP_LTD, OP_GTD, OP_LED, OP_GED, OP_RES, OP_RES,
	OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES,
	OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES,
	OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES,
	OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES, OP_RES
};

extern char *Units[];



/*
 * The following value is used to handle virtually all special cases
 * while simulating.  The simulator normally executes in a fast path
 * where it ignores all special cases.  However, after executing each
 * instruction it checks the current serial number (total # of instructions
 * executed) agains the value below.  If that serial number has been
 * executed, then the simulator pauses to check for all possible special
 * conditions (stops, callbacks, errors, etc.).  Thus anyone that wants
 * to get a special condition handled must be sure to set checkNum below
 * so that the special-check code will be executed.  This facility means
 * that the simulator only has to check a single condition in its fast
 * path.
 */

static int checkNum;

/*
 * Forward declarations for procedures defined later in this file:
 */
static void	statsReset(DLX *);
static int	AddressError(DLX *, unsigned int, int);
static char     *errstring(void);
static int	BusError(DLX *, unsigned int, int);
static void	Compile(MemWord *);
static int      Mult(int, int, int, int *);
static int	Overflow(DLX *);
static int	ReadMem(DLX *, unsigned int, int *);
static int	WriteMem(DLX *, unsigned int, int, int);
static int	Simulate(DLX *, Tcl_Interp *, int);
static int      NextCycle(DLX *, Tcl_Interp *);


/*
 *----------------------------------------------------------------------
 *
 * Sim_Create --
 *
 *	Create a description of an DLX machine.
 *
 * Results:
 *	The return value is a pointer to the description of the DLX
 *	machine.
 *
 * Side effects:
 *	The DLX structure gets allocated and initialized.  Several
 *	Tcl commands get registered for interp.
 *
 *----------------------------------------------------------------------
 */

DLX *
Sim_Create(interp)
Tcl_Interp *interp;		/* Interpreter to associate with machine. */
{
	register DLX *machPtr;
	extern int Main_QuitCmd();

	machPtr = (DLX *) calloc(1, sizeof(DLX));
	machPtr->interp = interp;
	machPtr->memPtr = NULL;
	machPtr->memScratch = NULL;
	machPtr->stopList = NULL;
	machPtr->callBackList = NULL;
	machPtr->bypassList = NULL;
	machPtr->updateList = NULL;
	machPtr->opsList = NULL;
	machPtr->finishList = NULL;
	machPtr->opFree = NULL;
	machPtr->updateFree = NULL;
	machPtr->bypassFree = NULL;

	Tcl_LinkVar(interp, "config", (char *)&(machPtr->config), TCL_LINK_INT);
	Tcl_LinkVar(interp, "insCount", (char *)&(machPtr->insCount), TCL_LINK_INT);
	Tcl_LinkVar(interp, "backInsCount", (char *)&(machPtr->backInsCount), TCL_LINK_INT);
	Tcl_LinkVar(interp, "cycleCount", (char *)&(machPtr->cycleCount), TCL_LINK_INT);
	Tcl_LinkVar(interp, "cycleDisplayCount", (char *)&(machPtr->cycleDisplayCount), TCL_LINK_INT);
	Tcl_LinkVar(interp, "numWords", (char *)&(machPtr->numWords), TCL_LINK_INT);
	Tcl_LinkVar(interp, "num_int_units", (char *)&(machPtr->num_int_units), TCL_LINK_INT);
	Tcl_LinkVar(interp, "num_add_units", (char *)&(machPtr->num_add_units), TCL_LINK_INT);
	Tcl_LinkVar(interp, "num_mul_units", (char *)&(machPtr->num_mul_units), TCL_LINK_INT);
	Tcl_LinkVar(interp, "num_div_units", (char *)&(machPtr->num_div_units), TCL_LINK_INT);
	Tcl_LinkVar(interp, "num_load_bufs", (char *)&(machPtr->num_load_bufs), TCL_LINK_INT);
	Tcl_LinkVar(interp, "num_store_bufs", (char *)&(machPtr->num_store_bufs), TCL_LINK_INT);
	Tcl_LinkVar(interp, "int_latency", (char *)&(machPtr->int_latency), TCL_LINK_INT);
	Tcl_LinkVar(interp, "fp_add_latency", (char *)&(machPtr->fp_add_latency), TCL_LINK_INT);
	Tcl_LinkVar(interp, "fp_mul_latency", (char *)&(machPtr->fp_mul_latency), TCL_LINK_INT);
	Tcl_LinkVar(interp, "fp_div_latency", (char *)&(machPtr->fp_div_latency), TCL_LINK_INT);
	Tcl_LinkVar(interp, "load_buf_latency", (char *)&(machPtr->load_buf_latency), TCL_LINK_INT);
	Tcl_LinkVar(interp, "store_buf_latency", (char *)&(machPtr->store_buf_latency), TCL_LINK_INT);
	Tcl_LinkVar(interp, "add_ful_pipe", (char *)&(machPtr->add_ful_pipe), TCL_LINK_INT);
	Tcl_LinkVar(interp, "mul_ful_pipe", (char *)&(machPtr->mul_ful_pipe), TCL_LINK_INT);
	Tcl_LinkVar(interp, "div_ful_pipe", (char *)&(machPtr->div_ful_pipe), TCL_LINK_INT);
	Tcl_LinkVar(interp, "fp_div_exist", (char *)&(machPtr->fp_div_exist), TCL_LINK_INT);
	Tcl_LinkVar(interp, "ld_st_exist", (char *)&(machPtr->ld_st_exist), TCL_LINK_INT);


	Tcl_CreateCommand(interp, "asm", Asm_AsmCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "load", Asm_LoadCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "get", Gp_GetCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "put", Gp_PutCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "fget", Gp_FGetCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "fput", Gp_FPutCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "quit", Main_QuitCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "init", Sim_InitCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "stats", Sim_DumpStats, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "itrace", Sim_ITraceCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "mtrace", Sim_MTraceCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "go", Sim_GoCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "step", Sim_StepCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "cycle", Sim_CycleCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "equation", Sim_EquationCmd, (ClientData) machPtr,
					  (void (*)()) NULL);
	Tcl_CreateCommand(interp, "stop", Stop_StopCmd, (ClientData) machPtr,
					  (void (*)()) NULL);


	return machPtr;
}


void Sim_Initialize(machPtr)
DLX *machPtr;                   /* machine description */
{
	MemWord *wordPtr;
	int i;
	Stop *stopPtr;
	CallBack *cbPtr;
	Bypass *bypassPtr;
	Update *updatePtr;
	Op *opPtr;

	while (machPtr->stopList != NULL) {
		stopPtr = machPtr->stopList;
		machPtr->stopList = stopPtr->overallPtr;
		free(stopPtr->command);
		free(stopPtr);
	}

	while (machPtr->callBackList != NULL) {
		cbPtr = machPtr->callBackList;
		machPtr->callBackList = cbPtr->nextPtr;
		free(cbPtr);
	}

	while (machPtr->bypassList != NULL) {
		bypassPtr = machPtr->bypassList;
		machPtr->bypassList = bypassPtr->nextPtr;
		Isu_FreeBypass(machPtr, bypassPtr);
	}

	while (machPtr->updateList != NULL) {
		updatePtr = machPtr->updateList;
		machPtr->updateList = updatePtr->nextPtr;
		Isu_FreeUpdate(machPtr, updatePtr);
	}

	while (machPtr->opsList != NULL) {
		opPtr = machPtr->opsList;
		machPtr->opsList = opPtr->nextPtr;
		Isu_FreeOp(machPtr, opPtr);
	}

	while (machPtr->finishList != NULL) {
		opPtr = machPtr->finishList;
		machPtr->finishList = opPtr->nextPtr;
		Isu_FreeOp(machPtr, opPtr);
	}

	machPtr->numChars = machPtr->numWords * 4;
	if (machPtr->memPtr != NULL)
		free(machPtr->memPtr);
	machPtr->memPtr = (MemWord *)
					  calloc(1, (unsigned) (sizeof(MemWord) * machPtr->numWords));
	for (i = machPtr->numWords, wordPtr = machPtr->memPtr; i > 0;
		 i--, wordPtr++) {
		wordPtr->value = 0;
		wordPtr->opCode = OP_NOT_COMPILED;
		wordPtr->stopList = NULL;
	}
	if (machPtr->memScratch != NULL)
		free(machPtr->memScratch);
	machPtr->memScratch = (char *) malloc (machPtr->numChars);
	machPtr->endScratch = machPtr->memScratch + machPtr->numChars;
	for (i = 0; i < NUM_GPRS; i++) {
		machPtr->regs[i] = 0;
	}
	machPtr->regs[FP_STATUS] = 0;
	machPtr->regs[PC_REG] = 0;
	machPtr->regs[NEXT_PC_REG] = 1;
	machPtr->badPC = 0;
	machPtr->addrErrNum = 0;
	machPtr->nextIF = 0;
	machPtr->nextID = 1;
	machPtr->lastIF = -1;
	machPtr->cycleHead = 0;
	machPtr->insCount = 0;
	machPtr->firstIns = 0;
	machPtr->branchSerial = -1;
	machPtr->branchPC = 0;
	machPtr->flags = 0;
	machPtr->stopNum = 1;

	/* set up floating point stuff */
	machPtr->regs[FP_STATUS] = 0;
	for (i = 0; i < machPtr->num_int_units; i++) machPtr->int_units[i] = 0;
	for (i = 0; i < machPtr->num_add_units; i++) machPtr->fp_add_units[i] = 0;
	for (i = 0; i < machPtr->num_div_units; i++) machPtr->fp_div_units[i] = 0;
	for (i = 0; i < machPtr->num_mul_units; i++) machPtr->fp_mul_units[i] = 0;
	for (i = 0; i < 6; i++) machPtr->load_bufs[i] = 0;
	for (i = 0; i < 3; i++) machPtr->store_bufs[i] = 0;
	machPtr->func_units[INT] = machPtr->int_units;
	machPtr->func_units[FP_ADD] = machPtr->fp_add_units;
	machPtr->func_units[FP_DIV] = machPtr->fp_div_units;
	machPtr->func_units[FP_MUL] = machPtr->fp_mul_units;
	machPtr->func_units[LOAD_BUF] = machPtr->load_bufs;
	machPtr->func_units[STORE_BUF] = machPtr->store_bufs;
	machPtr->fp_div_exist = 1;
	machPtr->ld_st_exist = 0;

	for (i = 0; i < 65; i++) {
		machPtr->waiting_regs[i] = 0;
		machPtr->sameCycle_regs[i] = 0;
	}
	machPtr->cycleCount = 1;
	machPtr->cycleDisplayCount = 1;
	machPtr->cycleDisplayMode = 0;
	machPtr->codeLine = 1;

	for (i = 0; i < 5; i++) {
		if (machPtr->cycleTable[i] != NULL)
			free(machPtr->cycleTable[i]);
		machPtr->cycleTable[i] = (char *)calloc(1, sizeof(char) * 500);
	}

	if (machPtr->refInstrTraceFile != NULL)
		fclose(machPtr->refInstrTraceFile);
	machPtr->refInstrTraceFile = NULL;

	if (machPtr->refTraceFile != NULL)
		fclose(machPtr->refTraceFile);
	machPtr->refTraceFile = NULL;

	/* initialize counters */
	statsReset(machPtr);

	Tcl_DeleteHashTable(&machPtr->symbols);
	Tcl_InitHashTable(&machPtr->symbols, TCL_STRING_KEYS);
	Io_Init(machPtr);
	Cop0_Init(machPtr);
	Trap_Init_Handle(machPtr);

	Sym_AddSymbol(machPtr, "", "memSize", machPtr->numChars, SYM_GLOBAL);
}


/*
 *----------------------------------------------------------------------
 * statsReset --
 *
 *	This procedure clears the various statistics which are kept on
 *	dynamic execution of the code.
 *
 * Results:
 *	None
 *
 * Side effects:
 *	Changes the related global variables.
 *
 *----------------------------------------------------------------------
 */
void statsReset(machPtr)
DLX *machPtr;                    /* machine description */
{
	int i;

	machPtr->stalls = 0;
	machPtr->branchYes = 0;
	machPtr->branchNo = 0;
	for (i = 0; i <= OP_LAST; i++)
		machPtr->operationCount[i] = 0;
}


int
Sim_InitCmd(clientData, interp, argc, argv)
ClientData clientData;
Tcl_Interp *interp;			/* Current interpreter. */
int argc;				/* Number of arguments. */
char **argv;			/* Argument strings. */
{
	DLX *machPtr = (DLX *) clientData;
	Sim_Initialize(machPtr);
	return TCL_OK;			/* Never gets executed. */
}


/*
 *----------------------------------------------------------------------
 *
 * Sim_DumpStats --
 *
 *	This procedure is invoked to process the "stats" Tcl command.
 *	See the user documentation for details on what it does.
 *
 * Results:
 *	A standard Tcl result.
 *
 * Side effects:
 *	See the user documentation.
 *
 *----------------------------------------------------------------------
 */

int
Sim_DumpStats(clientData, interp, argc, argv)
ClientData clientData;
Tcl_Interp *interp;			/* Current interpreter. */
int argc;				/* Number of arguments. */
char **argv;			/* Argument strings. */
{
	return TCL_OK;
}


/*
 *----------------------------------------------------------------------
 *
 * Sim_GoCmd --
 *
 *	This procedure is invoked to process the "go" Tcl command.
 *	See the user documentation for details on what it does.
 *
 * Results:
 *	A standard Tcl result.
 *
 * Side effects:
 *	See the user documentation.
 *
 *----------------------------------------------------------------------
 */

int
Sim_GoCmd(clientData, interp, argc, argv)
ClientData clientData;
Tcl_Interp *interp;			/* Current interpreter. */
int argc;				/* Number of arguments. */
char **argv;			/* Argument strings. */
{
	DLX *machPtr = (DLX *) clientData;
	int effectiveCycleCount;

	if (argc > 2) {
		sprintf(interp->result,
				"too many args:  should be \"%.50s\" [address]", argv[0]);
		return TCL_ERROR;
	}

	if (argc == 2) {
		char *end;
		int newPc;

		if (Sym_EvalExpr(machPtr, (char *) NULL, argv[1], 0, &newPc, &end)
			!= TCL_OK) {
			return TCL_ERROR;

		}
		if ((*end != 0) || (newPc & 0x3)) {
			sprintf(interp->result,
					"\"%.50s\" isn't a valid starting address", argv[1]);
			return TCL_ERROR;
		}
		machPtr->regs[PC_REG] = ADDR_TO_INDEX(newPc);
		machPtr->regs[NEXT_PC_REG] = machPtr->regs[PC_REG] + 1;
		machPtr->flags = 0;
		machPtr->badPC = 0;
	}
	Tcl_VarEval(machPtr->interp, "ChangeCycleTitle 0; set fastmode 1",
				(char *)NULL);

	if (machPtr->config == BASICPIPE) {
		effectiveCycleCount = machPtr->nextIF;
	} else {
		effectiveCycleCount = machPtr->cycleCount;
	}

	if (machPtr->cycleDisplayMode &&
		machPtr->cycleDisplayCount < effectiveCycleCount) {
		while (machPtr->cycleDisplayCount < effectiveCycleCount) {
			NextCycle (machPtr, interp);
		}
	}

	machPtr->cycleDisplayMode = 0;
	return Simulate(machPtr, interp, 0);
}


/*
 *----------------------------------------------------------------------
 *
 * Sim_StepCmd --
 *
 *	This procedure is invoked to process the "step" Tcl command.
 *	See the user documentation for details on what it does.
 *
 * Results:
 *	A standard Tcl result.
 *
 * Side effects:
 *	See the user documentation.
 *
 *----------------------------------------------------------------------
 */

int
Sim_StepCmd(clientData, interp, argc, argv)
ClientData clientData;
Tcl_Interp *interp;			/* Current interpreter. */
int argc;				/* Number of arguments. */
char **argv;			/* Argument strings. */
{
	DLX *machPtr = (DLX *) clientData;
	int effectiveCycleCount;

	if (argc > 2) {
		sprintf(interp->result,
				"too many args:  should be \"%.50s\" [address]", argv[0]);
		return TCL_ERROR;
	}

	if (argc == 2) {
		char *end;
		int newPc;

		if (Sym_EvalExpr(machPtr, (char *) NULL, argv[1], 0, &newPc, &end)
			!= TCL_OK) {
			return TCL_ERROR;
		}
		if ((*end != 0) || (newPc & 0x3)) {
			sprintf(interp->result,
					"\"%.50s\" isn't a valid address", argv[1]);
			return TCL_ERROR;
		}
		machPtr->regs[PC_REG] = ADDR_TO_INDEX(newPc);
		machPtr->regs[NEXT_PC_REG] = machPtr->regs[PC_REG] + 1;
		machPtr->flags = 0;
		machPtr->badPC = 0;
	}

	Tcl_VarEval(machPtr->interp, "ChangeCycleTitle 0; set fastmode 0",
				(char *)NULL);

	if (machPtr->config == BASICPIPE) {
		effectiveCycleCount = machPtr->nextIF + 1;
	} else {
		effectiveCycleCount = machPtr->cycleCount;
	}

	if (machPtr->cycleDisplayMode &&
		machPtr->cycleDisplayCount < effectiveCycleCount) {
		while (machPtr->cycleDisplayCount < effectiveCycleCount) {
			NextCycle (machPtr, interp);
		}
		machPtr->cycleDisplayMode = 0;
		return TCL_OK;
	} else {
		machPtr->cycleDisplayMode = 0;
		return Simulate(machPtr, interp, 1);
	}
}


/*
 *----------------------------------------------------------------------
 *
 * Sim_CycleCmd --
 *
 *      This procedure is invoked to process the "cycle" Tcl command.
 *      See the user documentation for details on what it does.
 *
 * Results:
 *      A standard Tcl result.
 *
 * Side effects:
 *      See the user documentation.
 *
 *----------------------------------------------------------------------
 */

int
Sim_CycleCmd(clientData, interp, argc, argv)
ClientData clientData;
Tcl_Interp *interp;                 /* Current interpreter. */
int argc;                           /* Number of arguments. */
char **argv;                        /* Argument strings. */
{
	DLX *machPtr = (DLX *) clientData;

	if (argc > 2) {
		sprintf(interp->result,
				"too many args:  should be \"%.50s\" [address]", argv[0]);
		return TCL_ERROR;
	}

	if (argc == 2) {
		char *end;
		int newPc;

		if (Sym_EvalExpr(machPtr, (char *) NULL, argv[1], 0, &newPc, &end)
			!= TCL_OK) {
			return TCL_ERROR;

		}
		if ((*end != 0) || (newPc & 0x3)) {
			sprintf(interp->result,
					"\"%.50s\" isn't a valid starting address", argv[1]);
			return TCL_ERROR;
		}
		machPtr->regs[PC_REG] = ADDR_TO_INDEX(newPc);
		machPtr->regs[NEXT_PC_REG] = machPtr->regs[PC_REG] + 1;
		machPtr->flags = 0;
		machPtr->badPC = 0;
	}

	if (!machPtr->cycleDisplayMode) {
		if (machPtr->config == BASICPIPE)
			machPtr->cycleDisplayCount = machPtr->lastIF + 1;
		else
			machPtr->cycleDisplayCount = machPtr->cycleCount;
		machPtr->cycleDisplayMode = 1;
	}

	Tcl_VarEval(machPtr->interp, "ChangeCycleTitle 1; set fastmode 0",
				(char *)NULL);
	return NextCycle(machPtr, interp);

}


/*
 *----------------------------------------------------------------------
 *
 * Sim_CallBack --
 *
 *	Arrange for a particular procedure to be invoked after a given
 *	number of instructions have been simulated.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	After numIns instructions have been executed, proc will be
 *	invoked in the following way:
 *
 *	void
 *	proc(clientData, machPtr)
 *	    ClientData clientData;
 *	    DLX *machPtr;
 *	{
 *	}
 *
 *	The clientData and machPtr arguments will be the same as those
 *	passed to this procedure.
 *----------------------------------------------------------------------
 */

void
Sim_CallBack(machPtr, numIns, proc, clientData)
DLX *machPtr;		/* Machine of interest. */
int numIns;			/* Call proc after this many instructions
				 * have been executed in machPtr. */
void (*proc)();		/* Procedure to call. */
ClientData clientData;	/* Arbitrary one-word value to pass to proc. */
{
	register CallBack *cbPtr;

	cbPtr = (CallBack *) calloc(1, sizeof(CallBack));
	cbPtr->serialNum = machPtr->insCount + numIns;
	cbPtr->proc = proc;
	cbPtr->clientData = clientData;
	if ((machPtr->callBackList == NULL) ||
		(cbPtr->serialNum < machPtr->callBackList->serialNum)) {
		cbPtr->nextPtr = machPtr->callBackList;
		machPtr->callBackList = cbPtr;
	} else {
		register CallBack *cbPtr2;

		for (cbPtr2 = machPtr->callBackList; cbPtr2->nextPtr != NULL;
			 cbPtr2 = cbPtr2->nextPtr) {
			if (cbPtr->serialNum < cbPtr2->nextPtr->serialNum) {
				break;
			}
		}
		cbPtr->nextPtr = cbPtr2->nextPtr;
		cbPtr2->nextPtr = cbPtr;
	}
	if (cbPtr->serialNum < checkNum) {
		checkNum = cbPtr->serialNum;
	}
}


/*
 *----------------------------------------------------------------------
 *
 * Sim_Stop --
 *
 *	Arrange for the execution of the machine to stop after the
 *	current instruction.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The machine will stop executing (if it was executing in the
 *	first place).
 *
 *----------------------------------------------------------------------
 */

void
Sim_Stop(machPtr)
DLX *machPtr;			/* Machine to stop. */
{
	machPtr->flags |= STOP_REQUESTED;
	checkNum = machPtr->insCount + 1;
}


/*
 *----------------------------------------------------------------------
 *
 * Sim_GetPC --
 *
 *	This procedure computes the current program counter for
 *	machPtr.
 *
 * Results:
 *	The return value is the current program counter for the
 *	machine.  This is a bit tricky to compute because the PC
 *	is stored as an index, and there may have been an unaligned
 *	value put in the PC.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

unsigned int
Sim_GetPC(machPtr)
register DLX *machPtr;		/* Machine whose PC is wanted. */
{
	if ((machPtr->badPC != 0) && (machPtr->insCount >= machPtr->addrErrNum)) {
		return machPtr->badPC;
	}
	return INDEX_TO_ADDR(machPtr->regs[PC_REG]);
}


int
Sim_EquationCmd(clientData, interp, argc, argv)
ClientData clientData;
Tcl_Interp *interp;			/* Current interpreter. */
int argc;				/* Number of arguments. */
char **argv;			/* Argument strings. */
{
	DLX *machPtr = (DLX *) clientData;
	int unit;
	int cycle;
	Op *opPtr1, *opPtr2;
	Tcl_DString list;
	char part[20];

	if (argc != 4) {
		interp->result = "wrong # args";
		return TCL_ERROR;
	}

	unit = atoi(argv[1]);
	cycle = atoi(argv[2]);
	Tcl_DStringInit(&list);

	if (!(strcmp(argv[3], "issue"))) {
		for (opPtr1 = machPtr->opsList; opPtr1 != NULL; opPtr1 = opPtr1->nextPtr) {
			if (opPtr1->unit == unit) {
				if (opPtr1->rs1 != -1 && opPtr1->rs1 < 64) {
					if (opPtr1->rs1 < 32)
						sprintf(part, "R%d", opPtr1->rs1);
					else
						sprintf(part, "F%d", opPtr1->rs1 - 32);
					Tcl_DStringAppendElement(&list, part);
					if (opPtr1->rs1Ready)
						Tcl_DStringAppendElement(&list, "1");
					else
						Tcl_DStringAppendElement(&list, "0");
				} else {
					Tcl_DStringAppendElement(&list, "");
					Tcl_DStringAppendElement(&list, "0");
				}
				if (opPtr1->rs2 != -1 && opPtr1->rs2 < 64) {
					if (opPtr1->rs2 < 32)
						sprintf(part, "R%d", opPtr1->rs2);
					else
						sprintf(part, "F%d", opPtr1->rs2 - 32);
					Tcl_DStringAppendElement(&list, part);
					if (opPtr1->rs2Ready)
						Tcl_DStringAppendElement(&list, "1");
					else
						Tcl_DStringAppendElement(&list, "0");
				} else {
					Tcl_DStringAppendElement(&list, "");
					Tcl_DStringAppendElement(&list, "0");
				}
				if (opPtr1->resultType != NON_OP && opPtr1->rd < 64) {
					if (opPtr1->rd < 32)
						sprintf(part, "R%d", opPtr1->rd);
					else
						sprintf(part, "F%d", opPtr1->rd - 32);
					Tcl_DStringAppendElement(&list, part);
				} else {
					Tcl_DStringAppendElement(&list, "");
				}
				Tcl_DStringResult(interp, &list);
				break;
			}
		}
	} else if (!(strcmp(argv[3], "write"))) {
		for (opPtr1 = machPtr->finishList; opPtr1 != NULL; opPtr1 = opPtr1->nextPtr) {
			if (opPtr1->resultType != NON_OP && opPtr1->unit == unit &&
				opPtr1->rdReady == cycle) {
				if (machPtr->waiting_regs[opPtr1->rd] == 0) {
					if (opPtr1->rd < 32)
						sprintf(part, "R%d", opPtr1->rd);
					else
						sprintf(part, "F%d", opPtr1->rd - 32);
					Tcl_DStringAppendElement(&list, part);
				} else {
					Tcl_DStringAppendElement(&list, "");
				}

				for (opPtr2 = machPtr->opsList; opPtr2 != NULL; opPtr2 = opPtr2->nextPtr) {
					if (opPtr2->rs1 == opPtr1->rd && opPtr2->rs1Ready > cycle) {
						if (machPtr->config == TOMASULO && opPtr2->rs1Ready != cycle + 1)
							continue;
						Tcl_DStringAppendElement(&list, "1");
						sprintf(part, "%s", Units[opPtr2->unit]);
						Tcl_DStringAppendElement(&list, part);
					}
				}

				for (opPtr2 = machPtr->opsList; opPtr2 != NULL; opPtr2 = opPtr2->nextPtr) {
					if (opPtr2->rs2 == opPtr1->rd && opPtr2->rs2Ready > cycle) {
						if (machPtr->config == TOMASULO && opPtr2->rs2Ready != cycle + 1)
							continue;
						Tcl_DStringAppendElement(&list, "2");
						sprintf(part, "%s", Units[opPtr2->unit]);
						Tcl_DStringAppendElement(&list, part);
					}
				}
				Tcl_DStringResult(interp, &list);
			}
		}
	} else if (!(strncmp(argv[3], "inuse", 5))) {
		for (opPtr1 = machPtr->opsList; opPtr1 != NULL; opPtr1 = opPtr1->nextPtr) {
			if (opPtr1->unit == unit) {
				int readBegin, execComplete;
				readBegin = (opPtr1->rs1Ready > opPtr1->rs2Ready) ?
							opPtr1->rs1Ready : opPtr1->rs2Ready;
				if (cycle < readBegin) {
					Tcl_DStringAppendElement(&list, "RAW");
				} else if (cycle == readBegin) {
					Tcl_DStringAppendElement(&list, "read");
				} else {
					switch (opPtr1->unit / MAX_FUNC_UNITS) {
					case INT:
						execComplete = machPtr->int_latency + readBegin;
						break;
					case FP_ADD:
						execComplete = machPtr->fp_add_latency + readBegin;
						break;
					case FP_MUL:
						if (!(strncmp(argv[3], "inuse", 5)))
							execComplete = machPtr->fp_mul_latency + readBegin;
						else
							execComplete = machPtr->fp_div_latency + readBegin;
						break;
					case FP_DIV:
						execComplete = machPtr->fp_div_latency + readBegin;
						break;
					}
					if (cycle <= execComplete)
						Tcl_DStringAppendElement(&list, "exec");
					else
						Tcl_DStringAppendElement(&list, "WAR");

				}
				Tcl_DStringResult(interp, &list);
				break;
			}
		}
	}

	return TCL_OK;
}


/*
 *----------------------------------------------------------------------
 *
 * Sim_MTraceCmd --
 *
 *	This command turns on or off tracing in the simulator.
 *
 * Results:
 *	A standard Tcl result.
 *
 * Side effects:
 *	See the user documentation.
 *
 *----------------------------------------------------------------------
 */
int
Sim_MTraceCmd(clientData, interp, argc, argv)
ClientData clientData;
Tcl_Interp *interp;			/* Current interpreter. */
int argc;				/* Number of arguments. */
char **argv;			/* Argument strings. */
{
	DLX *machPtr = (DLX *) clientData;

	if( argc == 2 && !strcmp( argv[1], "off" ) ) {
		if( !machPtr->refTraceFile ) {
			sprintf( interp->result, "tracing wasn't on" );
			return TCL_ERROR;
		}
		fclose( machPtr->refTraceFile );
		machPtr->refTraceFile = NULL;
		return TCL_OK;
	}
	if( argc == 3 && !strcmp( argv[1], "on" ) ) {
		if( machPtr->refTraceFile ) {
			sprintf( interp->result,
					 "tracing already on -- turn it off first" );
			return TCL_ERROR;
		}
		if( !(machPtr->refTraceFile = fopen( argv[2], "a" )) ) {
			sprintf( interp->result, "couldn't open \"%s\": %s",
					 argv[2], errstring() );
			return TCL_ERROR;
		}
		return TCL_OK;
	}
	sprintf( interp->result, "bad option to trace: should be 'on' <filename> or 'off'." );
	return TCL_ERROR;
}


/*
 *----------------------------------------------------------------------
 *
 * Sim_ITraceCmd --
 *
 *	This command prints a dynamic instruction trace.
 *
 * Results:
 *	A standard Tcl result.
 *
 *----------------------------------------------------------------------
 */
int
Sim_ITraceCmd(clientData, interp, argc, argv)
ClientData clientData;
Tcl_Interp *interp;			/* Current interpreter. */
int argc;				/* Number of arguments. */
char **argv;			/* Argument strings. */
{
	DLX *machPtr = (DLX *) clientData;

	if( argc == 2 && !strcmp( argv[1], "off" ) ) {
		if( !machPtr->refInstrTraceFile ) {
			sprintf( interp->result, "tracing wasn't on" );
			return TCL_ERROR;
		}
		fclose( machPtr->refInstrTraceFile );
		machPtr->refTraceFile = NULL;
		return TCL_OK;
	}
	if( argc == 3 && !strcmp( argv[1], "on" ) ) {
		if( machPtr->refInstrTraceFile ) {
			sprintf( interp->result,
					 "instruction tracing already on -- turn it off first" );
			return TCL_ERROR;
		}
		if( !(machPtr->refInstrTraceFile = fopen( argv[2], "a" )) ) {
			sprintf( interp->result, "couldn't open \"%s\": %s",
					 argv[2], errstring() );
			return TCL_ERROR;
		}
		return TCL_OK;
	}
	sprintf( interp->result, "bad option to trace: should be 'on' <filename> or 'off'." );
	return TCL_ERROR;


}


static char *
errstring(void)
{
	static char msgbuf[64];

	if( !errno )
		return "no error";
	if( errno >= sys_nerr ) {
		sprintf( msgbuf, "unknown error %d", errno );
		return msgbuf;
	}
	return sys_errlist[ errno ];
}


/*
 *----------------------------------------------------------------------
 *
 * ReadMem --
 *
 *	Read a word from DLX memory.
 *
 * Results:
 *	Under normal circumstances, the result is 1 and the word at
 *	*valuePtr is modified to contain the DLX word at the given
 *	address.  If no such memory address exists, or if a stop is
 *	set on the memory location, then 0 is returned to signify that
 *	simulation should stop.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

static int
ReadMem(machPtr, address, valuePtr)
register DLX *machPtr;	/* Machine whose memory is being read. */
unsigned int address;	/* Desired word address. */
int *valuePtr;		/* Store contents of given word here. */
{
	unsigned int index;
	int result;
	register MemWord *wordPtr;

	index = ADDR_TO_INDEX(address);
	if (index < machPtr->numWords) {
		wordPtr = &machPtr->memPtr[index];
		if ((wordPtr->stopList != NULL)
			&& (machPtr->insCount != machPtr->firstIns)) {
			result = Stop_Execute(machPtr, wordPtr->stopList);
			if ((result != TCL_OK) || (machPtr->flags & STOP_REQUESTED)) {
				return 0;
			}
		}
		if (machPtr->refTraceFile)
			fprintf( machPtr->refTraceFile, "0 %x\n", address );
		*valuePtr = wordPtr->value;
		return 1;
	}

	/*
	 * The word isn't in the main memory.  See if it is an I/O
	 * register.
	 */

	if (Io_Read(machPtr, (address & ~0x3), valuePtr) == 1) {
		return 1;
	}

	/*
	 * The word doesn't exist.  Register a bus error.  If interrupts
	 * ever get implemented for bus errors, this code will have to
	 * change a bit.
	 */

	(void) BusError(machPtr, address, 0);
	return 0;
}


/*
 *----------------------------------------------------------------------
 *
 * WriteMem --
 *
 *	Write a value into the DLX's memory.
 *
 * Results:
 *	If the write completed successfully then 1 is returned.  If
 *	any sort of problem occurred (such as an addressing error or
 *	a stop) then 0 is returned;  the caller should stop simulating.
 *
 * Side effects:
 *	The DLX memory gets updated with a new byte, halfword, or word
 *	value.
 *
 *----------------------------------------------------------------------
 */

static int
WriteMem(machPtr, address, size, value)
register DLX *machPtr;	/* Machine whose memory is being read. */
unsigned int address;	/* Desired word address. */
int size;			/* Size to be written (1, 2, or 4 bytes). */
int value;			/* New value to write into memory. */
{
	unsigned int index;
	int result;
	register MemWord *wordPtr;

	if (((size == 4) && (address & 0x3)) || ((size == 2) && (address & 0x1))) {
		(void) AddressError(machPtr, address, 0);
		return 0;
	}
	index = ADDR_TO_INDEX(address);
	if (index < machPtr->numWords) {
		wordPtr = &machPtr->memPtr[index];
		if ((wordPtr->stopList != NULL)
			&& (machPtr->insCount != machPtr->firstIns)) {
			result = Stop_Execute(machPtr, wordPtr->stopList);
			if ((result != TCL_OK) || (machPtr->flags & STOP_REQUESTED)) {
				return 0;
			}
		}
		if (machPtr->refTraceFile)
			fprintf(machPtr->refTraceFile, "1 %x\n", address);
		if (size == 4) {
			wordPtr->value = value;
		} else if (size == 2) {
			if (address & 0x2) {
				wordPtr->value = (wordPtr->value & 0xffff0000)
								 | (value & 0xffff);
			} else {
				wordPtr->value = (wordPtr->value & 0xffff)
								 | (value << 16);
			}
		} else {
			switch (address & 0x3) {
			case 0:
				wordPtr->value = (wordPtr->value & 0x00ffffff)
								 | (value << 24);
				break;
			case 1:
				wordPtr->value = (wordPtr->value & 0xff00ffff)
								 | ((value & 0xff) << 16);
				break;
			case 2:
				wordPtr->value = (wordPtr->value & 0xffff00ff)
								 | ((value & 0xff) << 8);
				break;
			case 3:
				wordPtr->value = (wordPtr->value & 0xffffff00)
								 | (value & 0xff);
				break;
			}
		}
		wordPtr->opCode = OP_NOT_COMPILED;
		return 1;
	}

	/*
	 * Not in main memory.  See if it's an I/O device register.
	 */

	if (Io_Write(machPtr, address, value, size) == 1) {
		return 1;
	}

	(void) BusError(machPtr, address, 0);
	return 0;
}


/*
 *----------------------------------------------------------------------
 *
 * Simulate --
 *
 *	This procedure forms the core of the simulator.  It executes
 *	instructions until either a break occurs or an error occurs
 *	(or until a single instruction has been executed, if single-
 *	stepping is requested).
 *
 * Results:
 *	A standard Tcl result.
 *
 * Side effects:
 *	The state of *machPtr changes in response to the simulation.
 *	Return information may be left in *interp.
 *
 *----------------------------------------------------------------------
 */

static int
Simulate(machPtr, interp, singleStep)
register DLX *machPtr;		/* Machine description. */
Tcl_Interp *interp;			/* Tcl interpreter, for results and
					 * break commands. */
int singleStep;			/* Non-zero means execute exactly
					 * one instruction, regardless of
					 * breaks found. */
{
	register MemWord *wordPtr;		/* Memory word for instruction. */
	register unsigned int pc;		/* Current ins. address, then new
					 * nextPc value. */
	Op *opPtr;
	unsigned int tmp;
	int i, result;
	char *errMsg, msg[20];
	Update *updatePtr;

	/*
	 * Can't continue from an addressing error on the program counter.
	 */

	if ((machPtr->badPC != 0) && (machPtr->addrErrNum == machPtr->insCount)) {
		sprintf(interp->result,
				"address error on instruction fetch, pc = 0x%x",
				machPtr->badPC);
		return TCL_ERROR;
	}

	machPtr->flags &= ~STOP_REQUESTED;
	machPtr->firstIns = machPtr->insCount;
	Io_BeginSim(machPtr);

setCheckNum:
	if (machPtr->callBackList != NULL) {
		checkNum = machPtr->callBackList->serialNum;
	} else {
		checkNum = machPtr->insCount+100000;
	}
	if ((machPtr->badPC != 0) && (machPtr->addrErrNum > machPtr->insCount)) {
		if (checkNum > machPtr->addrErrNum) {
			checkNum = machPtr->addrErrNum;
		}
	} else {
		machPtr->badPC = 0;
	}
	if (singleStep) {
		checkNum = machPtr->insCount+1;
	}
	while (1) {
		int trapCaught = 0;

		/*
		 * Fetch an instruction, and compute the new next pc (but don't
		 * store it yet, in case the instruction doesn't complete).
		 */
		pc = machPtr->regs[PC_REG];
		if (pc >= machPtr->numWords) {
			result = BusError(machPtr, INDEX_TO_ADDR(pc), 1);
			if (result != TCL_OK) {
				goto stopSimulation;
			} else {
				goto endOfIns;
			}
		}
		if (machPtr->refTraceFile)
			fprintf(machPtr->refTraceFile, "2 %x\n", INDEX_TO_ADDR(pc));

		if (machPtr->refInstrTraceFile)
			fprintf(machPtr->refInstrTraceFile, "%x  %.50s\n",
					INDEX_TO_ADDR(pc),
					Asm_Disassemble(machPtr, machPtr->memPtr[pc].value, tmp & ~0x3));

		wordPtr = &machPtr->memPtr[pc];
		pc = machPtr->regs[NEXT_PC_REG]+1;
		/*
		 * Handle breaks on the instruction, if this isn't the first
		 * instruction executed.
		 */
		if ((wordPtr->stopList != NULL)
			&& (machPtr->insCount != machPtr->firstIns)) {
			result = Stop_Execute(machPtr, wordPtr->stopList);
			if ((result != TCL_OK) || (machPtr->flags & STOP_REQUESTED)) {
				goto stopSimulation;
			}
		}

		/*
		 * Execute the instruction.
		 */

execute:
		machPtr->operationCount[wordPtr->opCode]++;
		switch (wordPtr->opCode) {
		case OP_NOP:
			Isu_Issue(machPtr, wordPtr, INT, NON_OP, NON_OP, NON_OP);
			break;
		case OP_ADD: {
			int sum;
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			sum = opPtr->source1[0] + opPtr->source2[0];
			if (!((opPtr->source1[0] ^ opPtr->source2[0])
				  & SIGN_BIT) && ((opPtr->source1[0]
								   ^ sum) & SIGN_BIT)) {
				result = Overflow(machPtr);
				if (result != TCL_OK) {
					goto stopSimulation;
				} else {
					goto endOfIns;
				}
			}
			opPtr->result[0] = sum;
			break;
		}

		case OP_ADDI: {
			int sum;
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			sum = opPtr->source1[0] + wordPtr->extra;
			if (!((opPtr->source1[0] ^ wordPtr->extra)
				  & SIGN_BIT) && ((opPtr->source1[0]
								   ^ sum) & SIGN_BIT)) {
				result = Overflow(machPtr);
				if (result != TCL_OK) {
					goto stopSimulation;
				} else {
					goto endOfIns;
				}
			}
			opPtr->result[0] = sum;
			break;
		}

		case OP_ADDU:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0] + opPtr->source2[0];
			break;

		case OP_ADDUI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0] + (wordPtr->extra & 0xffff);
			break;

		case OP_AND:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0] & opPtr->source2[0];
			break;

		case OP_ANDI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0] & (wordPtr->extra & 0xffff);
			break;

		case OP_BEQZ:
		case OP_BFPF:
			opPtr = Isu_Issue(machPtr, wordPtr, BRANCH, INT_OP, NON_OP, NON_OP);
			if (opPtr->source1[0] == 0) {
				pc = machPtr->regs[NEXT_PC_REG] + ADDR_TO_INDEX(wordPtr->extra);
				machPtr->branchYes++;
			} else
				machPtr->branchNo++;
			machPtr->branchSerial = machPtr->insCount;
			machPtr->branchPC = INDEX_TO_ADDR(machPtr->regs[PC_REG]);
			break;

		case OP_BNEZ:
		case OP_BFPT:
			opPtr = Isu_Issue(machPtr, wordPtr, BRANCH, INT_OP, NON_OP, NON_OP);
			if (opPtr->source1[0] != 0) {
				pc = machPtr->regs[NEXT_PC_REG] + ADDR_TO_INDEX(wordPtr->extra);
				machPtr->branchYes++;
			} else
				machPtr->branchNo++;
			machPtr->branchSerial = machPtr->insCount;
			machPtr->branchPC = INDEX_TO_ADDR(machPtr->regs[PC_REG]);
			break;

		case OP_JAL:
			opPtr = Isu_Issue(machPtr, wordPtr, BRANCH, NON_OP, NON_OP, INT_OP);
			opPtr->result[0] = INDEX_TO_ADDR(machPtr->regs[NEXT_PC_REG] + 1);
			pc = machPtr->regs[NEXT_PC_REG] + ADDR_TO_INDEX(wordPtr->extra);
			machPtr->branchSerial = machPtr->insCount;
			machPtr->branchPC = INDEX_TO_ADDR(machPtr->regs[PC_REG]);
			break;

		case OP_J:
			Isu_Issue(machPtr, wordPtr, BRANCH, NON_OP, NON_OP, NON_OP);
			pc = machPtr->regs[NEXT_PC_REG] + ADDR_TO_INDEX(wordPtr->extra);
			machPtr->branchSerial = machPtr->insCount;
			machPtr->branchPC = INDEX_TO_ADDR(machPtr->regs[PC_REG]);
			break;

		case OP_JALR:
			opPtr = Isu_Issue(machPtr, wordPtr, BRANCH, INT_OP, NON_OP, INT_OP);
			opPtr->result[0] = INDEX_TO_ADDR(machPtr->regs[NEXT_PC_REG] + 1);
			tmp = opPtr->source1[0];
			pc = ADDR_TO_INDEX(tmp);
			if ((tmp & 0x3) && (machPtr->badPC == 0)) {
				machPtr->badPC = tmp;
				machPtr->addrErrNum = machPtr->insCount + 2;
				if (checkNum > machPtr->addrErrNum) {
					checkNum = machPtr->addrErrNum;
				}
			}
			machPtr->branchSerial = machPtr->insCount;
			machPtr->branchPC = INDEX_TO_ADDR(machPtr->regs[PC_REG]);
			break;

		case OP_JR:
			opPtr = Isu_Issue(machPtr, wordPtr, BRANCH, INT_OP, NON_OP, NON_OP);
			tmp = opPtr->source1[0];
			pc = ADDR_TO_INDEX(tmp);
			if ((tmp & 0x3) && (machPtr->badPC == 0)) {
				machPtr->badPC = tmp;
				machPtr->addrErrNum = machPtr->insCount + 2;
				if (checkNum > machPtr->addrErrNum) {
					checkNum = machPtr->addrErrNum;
				}
			}
			machPtr->branchSerial = machPtr->insCount;
			machPtr->branchPC = INDEX_TO_ADDR(machPtr->regs[PC_REG]);
			break;

		case OP_LB:
		case OP_LBU: {
			int value;
			opPtr = Isu_Issue(machPtr, wordPtr, LOAD_BUF, INT_OP, NON_OP, INT_OP);
			tmp = opPtr->address;
			if (ReadMem(machPtr, tmp, &value) == 0) {
				goto stopSimulation;
			}
			switch (tmp & 0x3) {
			case 0:
				value >>= 24;
				break;
			case 1:
				value >>= 16;
				break;
			case 2:
				value >>= 8;
			}
			if ((value & 0x80) && (wordPtr->opCode == OP_LB)) {
				value |= 0xffffff00;
			} else {
				value &= 0xff;
			}
			opPtr->result[0] = value;
			break;
		}

		case OP_LD:
			opPtr = Isu_Issue(machPtr, wordPtr, LOAD_BUF, INT_OP, NON_OP, DFP_OP);
			tmp = opPtr->address;
			if (tmp & 0x3) {
				result = AddressError(machPtr, tmp, 1);
				if (result != TCL_OK) {
					goto stopSimulation;
				} else {
					goto endOfIns;
				}
			}
			if (ReadMem(machPtr, tmp, &(opPtr->result[0])) == 0) {
				goto stopSimulation;
			}
			if (ReadMem(machPtr, tmp+4, &(opPtr->result[1])) == 0) {
				goto stopSimulation;
			}
			break;

		case OP_LF:
			opPtr = Isu_Issue(machPtr, wordPtr, LOAD_BUF, INT_OP, NON_OP, SFP_OP);
			tmp = opPtr->address;
			if (tmp & 0x3) {
				result = AddressError(machPtr, tmp, 1);
				if (result != TCL_OK) {
					goto stopSimulation;
				} else {
					goto endOfIns;
				}
			}
			if (ReadMem(machPtr, tmp, &(opPtr->result[0])) == 0) {
				goto stopSimulation;
			}
			break;


		case OP_LH:
		case OP_LHU: {
			int value;
			opPtr = Isu_Issue(machPtr, wordPtr, LOAD_BUF, INT_OP, NON_OP, INT_OP);
			tmp = opPtr->source1[0] + wordPtr->extra;
			if (tmp & 0x1) {
				result = AddressError(machPtr, tmp, 1);
				if (result != TCL_OK) {
					goto stopSimulation;
				} else {
					goto endOfIns;
				}
			}
			if (ReadMem(machPtr, tmp, &value) == 0) {
				goto stopSimulation;
			}
			if (!(tmp & 0x2)) {
				value >>= 16;
			}
			if ((value & 0x8000) && (wordPtr->opCode == OP_LH)) {
				value |= 0xffff0000;
			} else {
				value &= 0xffff;
			}
			opPtr->result[0] = value;
			break;
		}

		case OP_LHI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, IMM_OP, NON_OP, INT_OP);
			opPtr->result[0] = wordPtr->extra << 16;
			break;

		case OP_LW:
			opPtr = Isu_Issue(machPtr, wordPtr, LOAD_BUF, INT_OP, NON_OP, INT_OP);
			tmp = opPtr->source1[0] + wordPtr->extra;
			if (tmp & 0x3) {
				result = AddressError(machPtr, tmp, 1);
				if (result != TCL_OK) {
					goto stopSimulation;
				} else {
					goto endOfIns;
				}
			}
			if (ReadMem(machPtr, tmp, &(opPtr->result[0])) == 0) {
				goto stopSimulation;
			}
			break;

		case OP_MOVD:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, DFP_OP, NON_OP, DFP_OP);
			*((double *)&(opPtr->result[0])) = *((double *)&(opPtr->source1[0]));
			break;

		case OP_MOVF:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, SFP_OP, NON_OP, SFP_OP);
			*((float *)&(opPtr->result[0])) = *((float *)&(opPtr->source1[0]));
			break;

		case OP_MOVFP2I:
		case OP_MOVI2FP:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, NON_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0];
			break;

		case OP_MOVI2S:
			errMsg = "MOVI2S instruction is unimplemented";
			goto error;

		case OP_MOVS2I:
			errMsg = "MOVS2I instruction is unimplemented";
			goto error;


		case OP_OR:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0] | opPtr->source2[0];
			break;

		case OP_ORI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0] | (wordPtr->extra & 0xffff);
			break;

		case OP_RFE:
			errMsg = "RFE instruction is unimplemented";
			goto error;

		case OP_SB:
			opPtr = Isu_Issue(machPtr, wordPtr, STORE_BUF, INT_OP, INT_OP, NON_OP);
			tmp = opPtr->address;
			if (WriteMem(machPtr, tmp, 1, opPtr->source2[0]) == 0)
				goto stopSimulation;

			break;

		case OP_SD:
			opPtr = Isu_Issue(machPtr, wordPtr, STORE_BUF, INT_OP, DFP_OP, NON_OP);
			tmp = opPtr->address;
			if (WriteMem(machPtr, tmp, 4, opPtr->source2[0]) == 0)
				goto stopSimulation;
			if (WriteMem(machPtr, tmp + 4, 4, opPtr->source2[1]) == 0)
				goto stopSimulation;
			break;

		case OP_SEQ:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (opPtr->source1[0] == opPtr->source2[0])
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SEQI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (opPtr->source1[0] == wordPtr->extra)
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SEQU:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) ==
				((unsigned int) opPtr->source2[0]))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SEQUI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) ==
				((unsigned int) wordPtr->extra))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SF:
			opPtr = Isu_Issue(machPtr, wordPtr, STORE_BUF, INT_OP, SFP_OP, NON_OP);
			tmp = opPtr->address;
			if (WriteMem(machPtr, tmp, 4, opPtr->source2[0]) == 0)
				goto stopSimulation;
			break;


		case OP_SGE:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (opPtr->source1[0] >= opPtr->source2[0])
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SGEI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (opPtr->source1[0] >= wordPtr->extra)
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SGEU:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) >=
				((unsigned int) opPtr->source2[0]))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SGEUI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) >=
				((unsigned int) wordPtr->extra))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SGT:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (opPtr->source1[0] > opPtr->source2[0])
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SGTI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (opPtr->source1[0] > wordPtr->extra)
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SGTU:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) >
				((unsigned int) opPtr->source2[0]))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SGTUI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) >
				((unsigned int) wordPtr->extra))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SH:
			opPtr = Isu_Issue(machPtr, wordPtr, STORE_BUF, INT_OP, INT_OP, NON_OP);
			tmp = opPtr->address;
			if (WriteMem(machPtr, tmp, 2, opPtr->source2[0]) == 0)
				goto stopSimulation;
			break;

		case OP_SLE:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (opPtr->source1[0] <= opPtr->source2[0])
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SLEI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (opPtr->source1[0] <= wordPtr->extra)
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SLEU:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) <=
				((unsigned int) opPtr->source2[0]))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SLEUI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) <=
				((unsigned int) wordPtr->extra))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SLL:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			opPtr->result[0] = opPtr->source2[0] << (opPtr->source1[0] & 0x1f);
			break;

		case OP_SLLI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, IMM_OP, INT_OP, INT_OP);
			opPtr->result[0] = opPtr->source2[0] << wordPtr->extra;
			break;

		case OP_SLT:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (opPtr->source1[0] < opPtr->source2[0])
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SLTI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (opPtr->source1[0] < wordPtr->extra)
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SLTU:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) <
				((unsigned int) opPtr->source2[0]))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SLTUI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) <
				((unsigned int) wordPtr->extra))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SNE:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (opPtr->source1[0] != opPtr->source2[0])
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SNEI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (opPtr->source1[0] != wordPtr->extra)
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SNEU:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) !=
				((unsigned int) opPtr->source2[0]))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SNEUI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			if (((unsigned int) opPtr->source1[0]) !=
				((unsigned int) wordPtr->extra))
				opPtr->result[0] = 1;
			else
				opPtr->result[0] = 0;
			break;

		case OP_SRA:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			opPtr->result[0] = opPtr->source2[0] >> (opPtr->source1[0] & 0x1f);
			break;

		case OP_SRAI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, IMM_OP, INT_OP, INT_OP);
			opPtr->result[0] = opPtr->source2[0] >> wordPtr->extra;
			break;

		case OP_SRL:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			tmp = opPtr->source2[0];
			tmp >>= (opPtr->source1[0] & 0x1f);
			opPtr->result[0] = tmp;
			break;

		case OP_SRLI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, IMM_OP, INT_OP, INT_OP);
			tmp = opPtr->source2[0];
			tmp >>= wordPtr->extra;
			opPtr->result[0] = tmp;
			break;

		case OP_SUB: {
			int diff;
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			diff = opPtr->source1[0] - opPtr->source2[0];
			if (((opPtr->source1[0] ^ opPtr->source2[0])
				 & SIGN_BIT) && ((opPtr->source1[0]
								  ^ diff) & SIGN_BIT)) {
				result = Overflow(machPtr);
				if (result != TCL_OK) {
					goto stopSimulation;
				} else {
					goto endOfIns;
				}
			}
			opPtr->result[0] = diff;
			break;
		}

		case OP_SUBI: {
			int diff;
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			diff = opPtr->source1[0] - wordPtr->extra;
			if (((opPtr->source1[0] ^ wordPtr->extra)
				 & SIGN_BIT) && ((opPtr->source1[0]
								  ^ diff) & SIGN_BIT)) {
				result = Overflow(machPtr);
				if (result != TCL_OK) {
					goto stopSimulation;
				} else {
					goto endOfIns;
				}
			}
			opPtr->result[0] = diff;
			break;
		}

		case OP_SUBU:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0] - opPtr->source2[0];
			break;

		case OP_SUBUI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, IMM_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0] - (wordPtr->extra & 0xffff);
			break;

		case OP_SW:
			opPtr = Isu_Issue(machPtr, wordPtr, STORE_BUF, INT_OP, INT_OP, NON_OP);
			tmp = opPtr->address;
			if (WriteMem(machPtr, tmp, 4, opPtr->source2[0]) == 0)
				goto stopSimulation;
			break;

		case OP_TRAP:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, NON_OP, NON_OP, NON_OP);
			switch (Trap_Handle(machPtr, (wordPtr->extra & 0x3ffffff))) {
			case 1 :
				printf("TRAP #%d failed\n", wordPtr->extra & 0x3ffffff);
				trapCaught = 1;
				break;
			case 2 :
				printf("TRAP #%d received\n", wordPtr->extra & 0x3ffffff);
				if (!wordPtr->extra)
					Tcl_VarEval(machPtr->interp, "Trap0", (char *)NULL);
				trapCaught = 1;
				break;
			}
			break;

		case OP_XOR:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, INT_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0] ^ opPtr->source2[0];
			break;

		case OP_XORI:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, NON_OP, INT_OP);
			opPtr->result[0] = opPtr->source1[0] ^ (wordPtr->extra & 0xffff);
			break;

		/* floating point operations */

		case OP_DIV:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_DIV, INT_OP, INT_OP, INT_OP);
			if (opPtr->source2[0]) {
				opPtr->result[0] = opPtr->source1[0] / opPtr->source2[0];
			} else {
				errMsg = "divide by zero";
				goto error;
			}
			break;

		case OP_DIVU:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_DIV, INT_OP, INT_OP, INT_OP);
			if (*((unsigned int *)&(opPtr->source2[0]))) {
				*((unsigned int *)&(opPtr->result[0])) =
					*((unsigned int *)&(opPtr->source1[0])) /
					*((unsigned int *)&(opPtr->source2[0]));
			} else {
				errMsg = "divide by zero";
				goto error;
			}
			break;

		case OP_MULT:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_MUL, INT_OP, INT_OP, INT_OP);
			if (Mult(opPtr->source1[0], opPtr->source2[0], 1, &opPtr->result[0])) {
				errMsg = "signed multiply overflow";
				goto error;
			}
			break;

		case OP_MULTU:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_MUL, INT_OP, INT_OP, INT_OP);
			if (Mult(opPtr->source1[0], opPtr->source2[0], 0, &opPtr->result[0])) {
				errMsg = "signed multiply overflow";
				goto error;
			}
			break;

		case OP_ADDD:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_ADD, DFP_OP, DFP_OP, DFP_OP);
			*((double *)&(opPtr->result[0])) =
				*((double *)&(opPtr->source1[0])) + *((double *)&(opPtr->source2[0]));
			break;

		case OP_ADDF:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_ADD, SFP_OP, SFP_OP, SFP_OP);
			*((float *)&(opPtr->result[0])) =
				*((float *)&(opPtr->source1[0])) + *((float *)&(opPtr->source2[0]));
			break;

		case OP_CVTD2F:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, DFP_OP, NON_OP, SFP_OP);
			*((float *)&(opPtr->result[0])) =
				*((double *)&(opPtr->source1[0]));
			break;

		case OP_CVTD2I:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, DFP_OP, NON_OP, INT_OP);
			*((int *)&(opPtr->result[0])) =
				*((double *)&(opPtr->source1[0]));
			break;

		case OP_CVTF2D:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, SFP_OP, NON_OP, DFP_OP);
			*((double *)&(opPtr->result[0])) =
				*((float *)&(opPtr->source1[0]));
			break;

		case OP_CVTF2I:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, SFP_OP, NON_OP, INT_OP);
			*((int *)&(opPtr->result[0])) =
				*((float *)&(opPtr->source1[0]));
			break;

		case OP_CVTI2D:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, NON_OP, DFP_OP);
			*((double *)&(opPtr->result[0])) =
				*((int *)&(opPtr->source1[0]));
			break;

		case OP_CVTI2F:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, INT_OP, NON_OP, SFP_OP);
			*((float *)&(opPtr->result[0])) =
				*((int *)&(opPtr->source1[0]));
			break;

		case OP_DIVD:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_DIV, DFP_OP, DFP_OP, DFP_OP);
			if (*((double *)&(opPtr->source2[0]))) {
				*((double *)&(opPtr->result[0])) =
					*((double *)&(opPtr->source1[0])) / *((double *)&(opPtr->source2[0]));
			} else {
				errMsg = "divide by zero";
				goto error;
			}
			break;

		case OP_DIVF:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_DIV, SFP_OP, SFP_OP, SFP_OP);
			if (*((float *)&(opPtr->source2[0]))) {
				*((float *)&(opPtr->result[0])) =
					*((float *)&(opPtr->source1[0])) / *((float *)&(opPtr->source2[0]));
			} else {
				errMsg = "divide by zero";
				goto error;
			}
			break;

		case OP_EQD:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, DFP_OP, DFP_OP, INT_OP);
			opPtr->result[0] =  *((double *)&(opPtr->source1[0]))
								== *((double *)&(opPtr->source2[0]));
			break;

		case OP_EQF:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, SFP_OP, SFP_OP, INT_OP);
			opPtr->result[0] =  *((float *)&(opPtr->source1[0]))
								== *((float *)&(opPtr->source2[0]));
			break;

		case OP_GED:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, DFP_OP, DFP_OP, INT_OP);
			opPtr->result[0] =  *((double *)&(opPtr->source1[0]))
								>= *((double *)&(opPtr->source2[0]));
			break;

		case OP_GEF:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, SFP_OP, SFP_OP, INT_OP);
			opPtr->result[0] =  *((float *)&(opPtr->source1[0]))
								>= *((float *)&(opPtr->source2[0]));
			break;

		case OP_GTD:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, DFP_OP, DFP_OP, INT_OP);
			opPtr->result[0] =  *((double *)&(opPtr->source1[0]))
								> *((double *)&(opPtr->source2[0]));
			break;

		case OP_GTF:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, SFP_OP, SFP_OP, INT_OP);
			opPtr->result[0] =  *((float *)&(opPtr->source1[0]))
								> *((float *)&(opPtr->source2[0]));
			break;

		case OP_LED:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, DFP_OP, DFP_OP, INT_OP);
			opPtr->result[0] =  *((double *)&(opPtr->source1[0]))
								<= *((double *)&(opPtr->source2[0]));
			break;

		case OP_LEF:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, SFP_OP, SFP_OP, INT_OP);
			opPtr->result[0] =  *((float *)&(opPtr->source1[0]))
								<= *((float *)&(opPtr->source2[0]));
			break;

		case OP_LTD:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, DFP_OP, DFP_OP, INT_OP);
			opPtr->result[0] =  *((double *)&(opPtr->source1[0]))
								< *((double *)&(opPtr->source2[0]));
			break;

		case OP_LTF:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, SFP_OP, SFP_OP, INT_OP);
			opPtr->result[0] =  *((float *)&(opPtr->source1[0]))
								< *((float *)&(opPtr->source2[0]));
			break;

		case OP_MULTD:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_MUL, DFP_OP, DFP_OP, DFP_OP);
			*((double *)&(opPtr->result[0])) =
				*((double *)&(opPtr->source1[0])) * *((double *)&(opPtr->source2[0]));
			break;

		case OP_MULTF:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_MUL, SFP_OP, SFP_OP, SFP_OP);
			*((float *)&(opPtr->result[0])) =
				*((float *)&(opPtr->source1[0])) * *((float *)&(opPtr->source2[0]));
			break;

		case OP_NED:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, DFP_OP, DFP_OP, INT_OP);
			opPtr->result[0] =  *((double *)&(opPtr->source1[0]))
								!= *((double *)&(opPtr->source2[0]));
			break;

		case OP_NEF:
			opPtr = Isu_Issue(machPtr, wordPtr, INT, SFP_OP, SFP_OP, INT_OP);
			opPtr->result[0] =  *((float *)&(opPtr->source1[0]))
								!= *((float *)&(opPtr->source2[0]));
			break;

		case OP_SUBD:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_ADD, DFP_OP, DFP_OP, DFP_OP);
			*((double *)&(opPtr->result[0])) =
				*((double *)&(opPtr->source1[0])) - *((double *)&(opPtr->source2[0]));
			break;

		case OP_SUBF:
			opPtr = Isu_Issue(machPtr, wordPtr, FP_ADD, SFP_OP, SFP_OP, SFP_OP);
			*((float *)&(opPtr->result[0])) =
				*((float *)&(opPtr->source1[0])) - *((float *)&(opPtr->source2[0]));
			break;

		case OP_NOT_COMPILED:
			Compile(wordPtr);
			goto execute;
			break;

		case OP_RES:
			errMsg = "reserved operation";
			goto error;

		case OP_UNIMP:
			errMsg = "instruction not implemented in simulator";
			goto error;

		default:
			i = wordPtr->opCode;
			sprintf(interp->result,
					"internal error in Simulate():  bad opCode %d, pc = %.100s",
					i, Sym_GetString(machPtr, Sim_GetPC(machPtr)));
			result = TCL_ERROR;
			goto stopSimulation;
		}


		/*
		 * Make sure R0 stays zero.
		 */
		machPtr->regs[0] = 0;


		switch (machPtr->config) {
		case BASICPIPE:
			machPtr->cycleCount++;
			Isu_WriteBack (machPtr);
			break;
		case TOMASULO:
			Isu_WriteBack (machPtr);
			machPtr->cycleCount++;
			break;
		case SCOREBOARD:
			Isu_WriteBack (machPtr);
			machPtr->cycleCount++;
			break;
		}

		/*
		 * Monitor cycleCount and function units
		 */
		/* re-zero the cycle count periodically to avoid wrap around probs */
		if (machPtr->cycleCount >= CYC_CNT_RESET) {
			int i;
			Op *opPtr = machPtr->opsList;
			for (i = 0; i < 64; i++)
				if (machPtr->waiting_regs[i])
					machPtr->waiting_regs[i] -= CYC_CNT_RESET - 5;
			machPtr->cycleCount -= CYC_CNT_RESET - 5;
			while (opPtr != NULL) {
				opPtr->rdReady -= CYC_CNT_RESET - 5;
				opPtr = opPtr->nextPtr;
			}
		}

		/*
		 * Advance program counters.
		 */

		machPtr->regs[PC_REG] = machPtr->regs[NEXT_PC_REG];
		machPtr->regs[NEXT_PC_REG] = pc;


		Tcl_VarEval(machPtr->interp, "", (char *)NULL);

		/*
		 * Check flags for special actions to perform after the instruction.
		 */

		if (trapCaught) {
			result = TCL_OK;
			goto stopSimulation;
		}

		if ((machPtr->insCount += 1) >= checkNum) {
			while (machPtr->callBackList != NULL) {
				register CallBack *cbPtr;

				cbPtr = machPtr->callBackList;
				if (machPtr->insCount < cbPtr->serialNum) {
					break;
				}
				machPtr->callBackList = cbPtr->nextPtr;
				(*cbPtr->proc)(cbPtr->clientData, machPtr);
				free((char *) cbPtr);
			}
			if ((machPtr->badPC != 0)
				&& (machPtr->insCount == machPtr->addrErrNum)) {
				result = AddressError(machPtr, machPtr->badPC, 1);
				if (result != TCL_OK) {
					goto stopSimulation;
				}
			}
			if (singleStep) {
				tmp = Sim_GetPC(machPtr);
				sprintf(interp->result,
						"stopped after single step, pc = %.100s: %.50s",
						Sym_GetString(machPtr, tmp),
						Asm_Disassemble(machPtr,
										machPtr->memPtr[machPtr->regs[PC_REG]].value,
										tmp & ~0x3));
				result = TCL_OK;
				goto stopSimulation;
			}

			if (machPtr->flags & STOP_REQUESTED) {
				errMsg = "execution stopped";
				goto error;
			}
			goto setCheckNum;
		}

		if (machPtr->insCount == machPtr->backInsCount) {
			machPtr->backInsCount = 0x7fffffff;
			result = TCL_OK;
			goto stopSimulation;
		}
endOfIns:
		continue;
	}

error:
	tmp = Sim_GetPC(machPtr);
	Tcl_SetVar(machPtr->interp, "errMsg", errMsg,
			   TCL_GLOBAL_ONLY|TCL_LEAVE_ERR_MSG);
	Tcl_VarEval(machPtr->interp, "Err $errMsg", (char *)NULL);
	sprintf(interp->result, "%s, pc = %.50s: %.60s", errMsg,
			Sym_GetString(machPtr, tmp),
			Asm_Disassemble(machPtr,
							machPtr->memPtr[machPtr->regs[PC_REG]].value, tmp & ~0x3));



	result = TCL_OK;

	/*
	 * Before returning, store the current instruction serial number
	 * in a Tcl variable.
	 */

stopSimulation:
	Io_EndSim(machPtr);
	sprintf(msg, "%d", machPtr->insCount);
	/*   Tcl_SetVar(machPtr->interp, "insCount", msg,
	       TCL_GLOBAL_ONLY|TCL_LEAVE_ERR_MSG);*/
	Tcl_VarEval(machPtr->interp, "set insCount", msg, (char *)NULL);
	Tcl_VarEval(machPtr->interp, "ChangeCycleIcon", (char *)NULL);

	if (machPtr->config != BASICPIPE) {
		while (!machPtr->cycleDisplayMode && machPtr->updateList != NULL &&
			   machPtr->updateList->displayCycle == (machPtr->cycleCount-1)) {
			switch (machPtr->updateList->actionType) {
			case SHOW_CDB_LINE:
				Tcl_VarEval(machPtr->interp, "ShowCdbLine ", machPtr->updateList->msg,
							(char *)NULL);
				break;
			case SHOW_LINE:
				Tcl_VarEval(machPtr->interp, "ShowResLine ", machPtr->updateList->msg,
							(char *)NULL);
				break;
			}
			updatePtr = machPtr->updateList;
			machPtr->updateList = updatePtr->nextPtr;
			Isu_FreeUpdate(machPtr, updatePtr);
		}
	}

	if (machPtr->refTraceFile)
		fflush(machPtr->refTraceFile);


	return result;
}


/*
 *----------------------------------------------------------------------
 *
 * Compile --
 *
 *	Given a memory word, decode it into a set of fields that
 *	permit faster interpretation.
 *
 * Results:
 *	None.
 *
 * Side effects:
 *	The contents of *wordPtr are modified.
 *
 *----------------------------------------------------------------------
 */

static void
Compile(wordPtr)
register MemWord *wordPtr;		/* Memory location to be compiled. */
{
	register OpInfo *opPtr;

	wordPtr->rs1 = (wordPtr->value >> 21) & 0x1f;
	wordPtr->rs2 = (wordPtr->value >> 16) & 0x1f;
	wordPtr->rd = (wordPtr->value >> 11) & 0x1f;
	opPtr = &opTable[(wordPtr->value >> 26) & 0x3f];
	wordPtr->opCode = opPtr->opCode;
	if (opPtr->format == IFMT) {
		wordPtr->rd = wordPtr->rs2;
		wordPtr->extra = wordPtr->value & 0xffff;
		if (wordPtr->extra & 0x8000) {
			wordPtr->extra |= 0xffff0000;
		}
	} else if (opPtr->format == RFMT) {
		wordPtr->extra = (wordPtr->value >> 6) & 0x1f;
	} else {
		wordPtr->extra = wordPtr->value & 0x3ffffff;
		if (wordPtr->extra & 0x2000000) {
			wordPtr->extra |= 0xfc000000;
		}
	}
	switch (wordPtr->opCode) {
	case SPECIAL:
		wordPtr->opCode = specialTable[wordPtr->value & 0x3f];
		switch (wordPtr->opCode) {
		case OP_SLLI:
			wordPtr->opCode = 0;
			break;
		case OP_MOVFP2I:
			wordPtr->rs1 += 32;
			break;
		case OP_MOVI2FP:
			wordPtr->rd += 32;
			break;
		}
		break;
	case FPARITH:
		wordPtr->opCode = FParithTable[wordPtr->value & 0x3f];
		wordPtr->rs1 += 32;
		wordPtr->rs2 += 32;
		switch (wordPtr->opCode) {
		case OP_EQD:
		case OP_EQF:
		case OP_GED:
		case OP_GEF:
		case OP_GTD:
		case OP_GTF:
		case OP_LED:
		case OP_LEF:
		case OP_LTD:
		case OP_LTF:
		case OP_NED:
		case OP_NEF:
			wordPtr->rd = FP_STATUS;
			break;
		default:
			wordPtr->rd += 32;
		}
		break;
	case OP_LF:
	case OP_LD:
		wordPtr->rd += 32;
		break;
	case OP_SF:
	case OP_SD:
		wordPtr->rs2 = wordPtr->rd + 32;
		break;
	case OP_SB:
	case OP_SH:
	case OP_SW:
		wordPtr->rs2 = wordPtr->rd;
		break;
	case OP_BFPF:
	case OP_BFPT:
		wordPtr->rs1 = FP_STATUS;
		break;
	case OP_JAL:
	case OP_JALR:
		wordPtr->rd = 31;
		break;
	}
}


/*
 *----------------------------------------------------------------------
 *
 * BusError --
 *
 *	Handle bus error execptions.
 *
 * Results:
 *	A standard Tcl return value.  If TCL_OK, then there is no return
 *	string and it's safe to keep on simulating.
 *
 * Side effects:
 *	May simulate a trap for the machine.
 *
 *----------------------------------------------------------------------
 */

/* ARGSUSED */
static int
BusError(machPtr, address, iFetch)
DLX *machPtr;		/* Machine description. */
unsigned int address;	/* Location that was referenced but doesn't
				 * exist. */
int iFetch;			/* 1 means error occurred during instruction
				 * fetch.  0 means during a load or store. */
{
	unsigned int pcAddr;

	pcAddr = Sim_GetPC(machPtr);
	if (iFetch) {
		sprintf(machPtr->interp->result,
				"bus error: tried to fetch instruction at 0x%x",
				address);
	} else {
		sprintf(machPtr->interp->result,
				"bus error: referenced 0x%x during load or store, pc = %.100s: %s",
				address, Sym_GetString(machPtr, pcAddr),
				Asm_Disassemble(machPtr,
								machPtr->memPtr[machPtr->regs[PC_REG]].value,
								pcAddr & ~0x3));
	}
	return TCL_ERROR;
}


/*
 *----------------------------------------------------------------------
 *
 * AddressError --
 *
 *	Handle address error execptions.
 *
 * Results:
 *	A standard Tcl return value.  If TCL_OK, then there is no return
 *	string and it's safe to keep on simulating.
 *
 * Side effects:
 *	May simulate a trap for the machine.
 *
 *----------------------------------------------------------------------
 */

/* ARGSUSED */
static int
AddressError(machPtr, address, load)
DLX *machPtr;		/* Machine description. */
unsigned int address;	/* Location that was referenced but doesn't
				 * exist. */
int load;			/* 1 means error occurred during instruction
				 * fetch or load, 0 means during a store. */
{
	sprintf(machPtr->interp->result,
			"address error:  referenced 0x%x, pc = %.100s", address,
			Sym_GetString(machPtr, Sim_GetPC(machPtr)));
	return TCL_ERROR;
}


/*
 *----------------------------------------------------------------------
 *
 * Overflow --
 *
 *	Handle arithmetic overflow execptions.
 *
 * Results:
 *	A standard Tcl return value.  If TCL_OK, then there is no return
 *	string and it's safe to keep on simulating.
 *
 * Side effects:
 *	May simulate a trap for the machine.
 *
 *----------------------------------------------------------------------
 */

/* ARGSUSED */
static int
Overflow(machPtr)
DLX *machPtr;		/* Machine description. */
{
	unsigned int pcAddr;

	pcAddr = Sim_GetPC(machPtr);
	sprintf(machPtr->interp->result, "arithmetic overflow, pc = %.100s: %s",
			Sym_GetString(machPtr, pcAddr),  Asm_Disassemble(machPtr,
					machPtr->memPtr[machPtr->regs[PC_REG]].value,
					pcAddr & ~0x3));
	return TCL_ERROR;
}


/*
 *----------------------------------------------------------------------
 *
 * Mult --
 *
 *	Simulate DLX multiplication.
 *
 * Results:
 *      The word at *resultPtr is overwritten with the result
 *      of the multiplication and 0 is returned if there was no
 *      overflow.  On overflow, a negative integer is returned
 *      and *resultPtr is not changed.
 *
 * Side effects:
 *	None.
 *
 *----------------------------------------------------------------------
 */

static int
Mult(a, b, signedArith, resultPtr)
int a, b;			/* Two operands to multiply. */
int signedArith;		/* Zero means perform unsigned arithmetic,
				 * one means perform signed arithmetic. */
int *resultPtr;            /* Put result here. */
{
	unsigned int aHi, aLo, bHi, bLo, res1, res2, res3, res4, res5;
	unsigned int *resPtr;
	int negative;

	if ((a == 0) || (b == 0)) {
		*resultPtr = 0;
		return 0;
	}

	/*
	 * Compute the sign of the result, then make everything positive
	 * so unsigned computation can be done in the main computation.
	 */

	negative = 0;
	if (signedArith) {
		if (a < 0) {
			negative = !negative;
			a = -a;
		}
		if (b < 0) {
			negative = !negative;
			b = -b;
		}
	}

	/*
	 * Compute the result in unsigned arithmetic (check a's bits one at
	 * a time, and add in a shifted value of b).
	 */

	aLo = (unsigned int) (a & 0xffff);
	aHi = ((unsigned int) a) >> 16;
	bLo = (unsigned int) (b & 0xffff);
	bHi = ((unsigned int) b) >> 16;

	/* printf ("\naHi %d aLo %d bHi %d bLo %d\n", aHi, aLo, bHi, bLo); */

	if (aHi && bHi) return -1;

	res1 = aLo * bLo;
	res2 = aHi * bLo;
	res3 = aLo * bHi;
	if ((res2 | res3) & 0xffff0000) return -2;

	res2 <<= 16;
	res3 <<= 16;
	if ((res2 & res3) & 0x80000000) return -3;

	res4 = res2 + res3;
	if (((res2 | res3) & 0x80000000) && (!(res4 & 0x80000000))) return -4;

	if ((res1 & res4) & 0x80000000) return -5;
	res5 = res1 + res4;
	if (((res1 | res4) & 0x80000000) && (!(res5 & 0x80000000))) return -6;

	/*
	 * If the result is supposed to be negative, compute the two's
	 * complement of the double-word result.
	 */

	if (negative) {
		if ((res5 & 0x80000000) && (res5 & 0x7fffffff)) return -7;
		else res5 = (~res5) + 1;
	}

	resPtr = (unsigned int *) resultPtr;
	*resPtr = res5;
	return 0;
}


static int
NextCycle (machPtr, interp)
DLX *machPtr;                       /* Machine of interest. */
Tcl_Interp *interp;                 /* Tcl interpreter, for results and
                                         * break commands. */
{
	Update *updatePtr;
	char counter[10];
	char part[10];
	char linelist[200], linecolorlist[200];
	char textlist[100], textcolorlist[200];
	char rectlist[200], rectcolorlist[200];

	sprintf(counter, "%d", machPtr->cycleDisplayCount);
	switch (machPtr->config) {
	case BASICPIPE:
		Tcl_VarEval(machPtr->interp, ".bas.datapath.counter configure -text ",
					counter, (char *)NULL);
		Tcl_VarEval(machPtr->interp, ".bas.block.counter configure -text ",
					counter, (char *)NULL);
		sprintf(part, "%d ", machPtr->cycleDisplayCount -  machPtr->cycleHead);
		Tcl_VarEval(machPtr->interp, "BasChangeStrip ", part, (char *)NULL);
		if (machPtr->cycleDisplayCount == machPtr->nextIF)
			Simulate(machPtr, interp, 1);
	case TOMASULO:
		Tcl_VarEval(machPtr->interp, ".tom.c.counter configure -text ", counter,
					(char *)NULL);
		Tcl_VarEval(machPtr->interp, ".tom.c delete thickLine", (char *)NULL);
		if (machPtr->cycleDisplayCount == machPtr->cycleCount)
			Simulate(machPtr, interp, 1);
		break;
	case SCOREBOARD:
		Tcl_VarEval(machPtr->interp, ".sco.c.counter configure -text ", counter,
					(char *)NULL);
		Tcl_VarEval(machPtr->interp, ".sco.c delete thickLine", (char *)NULL);
		if (machPtr->cycleDisplayCount == machPtr->cycleCount)
			Simulate(machPtr, interp, 1);
		break;
	}



	Tcl_VarEval(machPtr->interp, "UnHighLight ", (char *)NULL);

	switch (machPtr->config) {
	case BASICPIPE:
		strcpy(linelist, "{");
		strcpy(linecolorlist, "{");
		strcpy(textlist, "{");
		strcpy(textcolorlist, "{");
		strcpy(rectlist, "{");
		strcpy(rectcolorlist, "{");
		break;
	case TOMASULO:
		Isu_CleanFinished (machPtr, ".tom", machPtr->cycleDisplayCount);
		break;
	case SCOREBOARD:
		Isu_CleanFinished (machPtr, ".sco", machPtr->cycleDisplayCount);
		break;
	}

	while (machPtr->updateList != NULL &&
		   machPtr->updateList->displayCycle == machPtr->cycleDisplayCount) {
		updatePtr = machPtr->updateList;
		machPtr->updateList = updatePtr->nextPtr;
		Tcl_SetVar(machPtr->interp, "msg", updatePtr->msg, TCL_GLOBAL_ONLY);
		switch (updatePtr->actionType) {
		case INSERT_HIGHLIGHT:
			Tcl_VarEval(machPtr->interp, "InsertAndHighLight ", updatePtr->w, "$msg",
						(char *)NULL);
			break;
		case HIGHLIGHT_CODE:
			Tcl_VarEval(machPtr->interp, "HighLightCodeLine ", "$msg",
						(char *)NULL);
			break;
		case DELETE_INSERT:
			Tcl_VarEval(machPtr->interp, "DeleteAndInsert ", updatePtr->w, "$msg",
						(char *)NULL);
			break;
		case ADD_END:
			Tcl_VarEval(machPtr->interp, "AddToEnd ", updatePtr->w, "$msg",
						(char *)NULL);
			break;
		case HIGHLIGHT_WRITE:
			Tcl_VarEval(machPtr->interp, "HighLightWrite ", updatePtr->w, (char *)NULL);
			break;
		case SHOW_CDB_LINE:
			Tcl_VarEval(machPtr->interp, "ShowCdbLine ", "$msg", (char *)NULL);
			break;
		case SHOW_LINE:
			Tcl_VarEval(machPtr->interp, "ShowResLine ", "$msg", (char *)NULL);
			break;
		case BAS_HIGHLIGHT_LINE:
			strcat(linelist, updatePtr->w);
			strcat(linecolorlist, updatePtr->msg);
			break;
		case BAS_HIGHLIGHT_TEXT:
			strcat(textlist, updatePtr->w);
			strcat(textcolorlist, "{");
			strcat(textcolorlist, updatePtr->msg);
			strcat(textcolorlist, "} ");
			break;
		case BAS_HIGHLIGHT_RECT:
			strcat(rectlist, updatePtr->w);
			strcat(rectcolorlist, updatePtr->msg);
			break;
		}

		Isu_FreeUpdate(machPtr, updatePtr);
	}

	if (machPtr->config == BASICPIPE) {
		strcat(linelist, "} ");
		strcat(linecolorlist, "} ");
		strcat(textlist, "} ");
		strcat(textcolorlist, "} ");
		strcat(rectlist, "} ");
		strcat(rectcolorlist, "} ");
		Tcl_VarEval(machPtr->interp, "BasVisual ", linelist, linecolorlist,
					textlist, textcolorlist, rectlist, rectcolorlist, (char *)NULL);
	}

	machPtr->cycleDisplayCount++;
	Tcl_VarEval(machPtr->interp, "grab release .code.t", (char *)NULL);
	return (TCL_OK);


}


