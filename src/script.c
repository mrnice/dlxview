/*
 * script.c --
 *
 *  This file contains TCL/TK commands reading for the animation of DLX
 *  processors.
 *
 *  This file is part of DISC.  It was written by Yinong Zhang
 *  (yinong@ecn.purdue.edu)
 *
 */


#include <tcl.h>
#include "script.h"
void
Script_Read (interp)
Tcl_Interp *interp;
{

	Tcl_Eval(interp, mkConfigure_tcl);
	Tcl_Eval(interp, ShowMemSize_tcl);
	Tcl_Eval(interp, SubConfigure_tcl);
	Tcl_Eval(interp, BasConfigure_tcl);
	Tcl_Eval(interp, TomConfigure_tcl);
	Tcl_Eval(interp, ScoConfigure_tcl);
	Tcl_Eval(interp, ConfigureOk_tcl);
	Tcl_Eval(interp, SubConfigureOk_tcl);
	Tcl_Eval(interp, ScaleValue_tcl);
	Tcl_Eval(interp, BasRadioCommand_tcl);
	Tcl_Eval(interp, DISC_tcl);
	Tcl_Eval(interp, mkBasicPipe_tcl);
	Tcl_Eval(interp, create_dot_tcl);
	Tcl_Eval(interp, BasVisual_tcl);
	Tcl_Eval(interp, BasGenTable_tcl);
	Tcl_Eval(interp, BasClean_tcl);
	Tcl_Eval(interp, BasChangeStrip_tcl);
	Tcl_Eval(interp, mkDisc_tcl);
	Tcl_Eval(interp, mkStepBack_tcl);
	Tcl_Eval(interp, mkPreviousCycle_tcl);
	Tcl_Eval(interp, DeleteAndInsert_tcl);
	Tcl_Eval(interp, InsertAndHighLight_tcl);
	Tcl_Eval(interp, UnHighLight_tcl);
	Tcl_Eval(interp, AddToEnd_tcl);
	Tcl_Eval(interp, HighLightCodeLine_tcl);
	Tcl_Eval(interp, Trap0_tcl);
	Tcl_Eval(interp, Err_tcl);
	Tcl_Eval(interp, ChangeCycleTitle_tcl);
	Tcl_Eval(interp, ChangeCycleIcon_tcl);
	Tcl_Eval(interp, Start_tcl);
	Tcl_Eval(interp, DisableTextWidget_tcl);
	Tcl_Eval(interp, mkGoStep_tcl);
	Tcl_Eval(interp, Go_tcl);
	Tcl_Eval(interp, Step_tcl);
	Tcl_Eval(interp, Cycle_tcl);
	Tcl_Eval(interp, demo_tcl);
	Tcl_Eval(interp, demoactbody_tcl);
	Tcl_Eval(interp, mkLoad_tcl);
	Tcl_Eval(interp, LoadOpen_tcl);
	Tcl_Eval(interp, LoadRefresh_tcl);
	Tcl_Eval(interp, LoadFile_tcl);
	Tcl_Eval(interp, Load_tcl);
	Tcl_Eval(interp, LoadDone_tcl);
	Tcl_Eval(interp, LoadCancel_tcl);
	Tcl_Eval(interp, FileEntryReturn_tcl);
	Tcl_Eval(interp, mkReset_tcl);
	Tcl_Eval(interp, ResetMode_tcl);
	Tcl_Eval(interp, mkScoreboard_tcl);
	Tcl_Eval(interp, mkStop_tcl);
	Tcl_Eval(interp, mkTomasulo_tcl);
	Tcl_Eval(interp, ShowCdbLineBAK_tcl);
	Tcl_Eval(interp, ShowCdbLine_tcl);
	Tcl_Eval(interp, HighLightWrite_tcl);
	Tcl_Eval(interp, ShowInsLine_tcl);
	Tcl_Eval(interp, ShowResLine_tcl);
	Tcl_Eval(interp, mkEquation_tcl);
	Tcl_Eval(interp, InsertWithTags_tcl);
	Tcl_Eval(interp, DISC_tcl);

}

