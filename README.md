About
=====

This is a fork of the DLX view 0.9 from University of Purdue. The purpose of this fork is to make it available again to the public. I couldn't find the source code any more on the internet and it won't compile on newer machines. The main goal of this fork is to be able to compile it again.

DLX View
========
DLX View is a nice DLX simulator based on dlx sim. It is enhanced in that way that it shows pipelining with tomasulo and scoreboard algorithms.

Build
=====

Requirements
------------
* TCL >= 8.6
* TK >= 8.6
* X11

Build steps
-----------
    meson builddir
    cd buliddir
    ninja